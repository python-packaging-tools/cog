--
--  Copyright (C) 2008, AdaCore
--
with AUnit.Simple_Test_Cases; use AUnit.Simple_Test_Cases;
with Cog_Cli.Test;

package body Cog_Suite is

   function Suite return Access_Test_Suite is
      Ret : constant Access_Test_Suite := new Test_Suite;
   begin
      Ret.Add_Test (Test_Case_Access'(new Cog_Cli.Test.Test));
      return Ret;
   end Suite;

end Cog_Suite;
