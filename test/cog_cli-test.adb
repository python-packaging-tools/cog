--  with Ada.Command_Line;
with AUnit.Assertions; use AUnit.Assertions;

package body Cog_Cli.Test is

   --  package Acl renames Ada.Command_Line;

   function Name (T : Test) return AUnit.Message_String is
      pragma Unreferenced (T);
   begin
      return AUnit.Format ("Test Cog_Cli package");
   end Name;

   procedure Run_Test (T : in out Test) is
      pragma Unreferenced (T);
   begin
      --  TODO: Mkae the CLI optionally take string instead of always
      --  reading the Acl.Command_Line
      --
      --  Acl.Command_Line := (1 => "--help");
      --  Parse;
      Assert (True, "Dummy test");
   end Run_Test;

end Cog_Cli.Test;
