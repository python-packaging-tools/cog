#!sh

set -ex

pacman --noconfirm -Sy unzip git
curl -Lo alr.zip \
     https://github.com/alire-project/alire/releases/download/v1.2.2/alr-1.2.2-bin-x86_64-linux.zip
unzip alr.zip
./bin/alr toolchain -i gnat_native=13.2.1 -i gprbuild=22.0.1
./bin/alr toolchain --select gnat_native=13.2.1 gprbuild=22.0.1
git clone --recursive https://gitlab.com/stcarrez/resource-embedder.git
cd resource-embedder
../bin/alr build --release
cd -

# Generate code for embedding resources
cd cog
../resource-embedder/bin/are --lang=Ada --rule=package.xml -o src --name-access .

# Build our program
../bin/alr build --release

# Test our program
cd -
./bin/alr build --release
./bin/test_cog
