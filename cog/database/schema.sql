create table platforms(
  platform text not null primary key
);

-- There is no documentation on how MacOS platform is specified.
-- Comment below is my guess.
-- Similarly, there's no documentation on what Windows platform
-- strings are possible.
insert into platforms(platform)
values ('any'), ('linux_x86_64'), ('linux_i386'), ('manylinux1'),
('manylinux2010'), ('manylinux2014'),
('manylinux_%d_%d'),            -- libc major / minor
('macosx_%d_%d_%s'),            -- OSX major / minor / CPU arch
('win32'), ('win_amd64');

create table pythons(
  python text not null primary key
);

insert into pythons(python) values ('py'), ('cp'), ('ip'), ('pp'), ('jy');

create table version_formats(
  pep integer not null primary key,
  format text not null
);

insert into version_formats(pep, format)
values (491, '{distribution}-{version}(-{build})?-{python}-{abi}-{platform}.whl');

create table abis(
  abi text not null primary key
);

-- There is no documentation on what ABI tags are possible
insert into abis(abi) values ('none'), ('cp%d%s%s'), ('abi3');

create table install_paths(
  label text not null primary key
);

-- I think these are the locations possible under .dist-info/data
insert into install_paths(label) values
('stdlib'),
('platstdlib'),
('purelib'),
('platlib'),
('include'),
('platinclude'),
('scripts'),
('data');

create table files(
  fpath text not null primary key,
  install_path text not null,
  foreign key(install_path) references install_paths(label)
);

create table description_content_types(
  content_type text not null
    check(content_type in ('text/plain', 'text/x-rst', 'text/markdown'))
    default 'text/plain',
  charset text default 'utf-8',
  variant text check(variant in ('GFM', 'CommonMark'))
);

create table wheel_metadata_v1_0(
  metadata_version text check(metadata_version in ('1.0', '1.1', '1.2', '2.1', '2.2', '2.3')),
  -- need two virtual fields: name and version that are taken from wheels table
  summary text,
  -- this can be written after all the fields if version is v2.1 or newer.  Padded with
  -- seven spaces and pipe otherwise
  description text,
  -- url
  home_page text,
  license text
);

create table wheel_metadata_v1_1(
  -- where this wheel can be downloaded from
  download_url text,
  older integer not null,
  foreign key(older) references wheel_metadata_v1_0(rowid)
);

create table wheel_metadata_v1_2(
  requires_python text,
  older integer not null,
  foreign key(older) references wheel_metadata_v1_1(rowid)
);

create table wheel_metadata_v2_1(
  description_content_type text,
  older integer not null,
  foreign key(description_content_type) references description_content_types(rowid)
  foreign key(older) references wheel_metadata_v1_2(rowid)
);

create table wheel_metadata_v2_2(
  older integer not null,
  foreign key(older) references wheel_metadata_v2_1(rowid)
);

create table wheel_metadata_v2_3(
  older integer not null,
  foreign key(older) references wheel_metadata_v2_2(rowid)
);

create table meta_provides_dists(
  provides_dist text not null,
  metadata integer not null,
  foreign key(metadata) references wheel_metadata_v1_2(rowid)
);

-- no idea what this is, the description is idiotic: https://peps.python.org/pep-0643/
create table meta_dynamics(
  "dynamic" text not null,
  metadata integer not null,
  foreign key(metadata) references wheel_metadata_v2_2(rowid)
);

create table meta_obsoletes_dists(
  obsoletes_dist text not null,
  metadata integer not null,
  foreign key(metadata) references wheel_metadata_v1_2(rowid)
);

-- list found here: https://pypi.org/classifiers/ (also PEP-301)
create table meta_classifiers(
  classifier text,
  metadata integer not null,
  foreign key(metadata) references wheel_metadata_v1_1(rowid)
);

-- Not surprisingly, this is not the same as platfom in wheel
create table meta_platforms(
  platform text,
  metadata integer not null,
  foreign key(metadata) references wheel_metadata_v1_0(rowid)
);

create table meta_authors(
  author text,
  metadata integer not null,
  foreign key(metadata) references wheel_metadata_v1_0(rowid)
);

create table meta_maintainers(
  maintainer text,
  metadata integer not null,
  foreign key(metadata) references wheel_metadata_v1_2(rowid)
);

-- Nobody knows how this should be specified
create table meta_supported_platforms(
  supported_platform text,
  metadata integer not null,
  foreign key(metadata) references wheel_metadata_v1_1(rowid)
);

create table meta_keywords(
  keywords text,
  metadata integer not null,
  foreign key(metadata) references wheel_metadata_v1_0(rowid)
);

create table meta_author_emails(
  author_email text,
  metadata integer not null,
  foreign key(metadata) references wheel_metadata_v1_0(rowid)
);

create table meta_maintainer_emails(
  maintainer_email text,
  metadata integer not null,
  foreign key(metadata) references wheel_metadata_v1_2(rowid)
);

-- details are in PEP-508
create table meta_requires_dists(
  requires_dist text,
  metadata integer not null,
  foreign key(metadata) references wheel_metadata_v1_2(rowid)
);

create table meta_requires_externals(
  requires_external text,
  metadata integer not null,
  foreign key(metadata) references wheel_metadata_v1_2(rowid)
);

create table meta_provides_extras(
  provides_extra text check(provides_extra regexp '^([a-z0-9]|[a-z0-9]([a-z0-9-](?!--))*[a-z0-9])$'),
  metadata integer not null,
  foreign key(metadata) references wheel_metadata_v2_1(rowid)
);

create table meta_project_urls(
  project_url text,
  metadata integer not null,
  foreign key(metadata) references wheel_metadata_v1_2(rowid)
);

-- This is what goest into WHEEL file
create table wheel_wheels(
  wheel_version text check(wheel_version = '1.9'),
  generator text check(generator = 'cog 1.0')
  -- couple of virtual fields: root_is_purelib (from wheel)
  -- and build (from wheel).  Tag field is also generated based on
  -- various info from wheel, a concatenation of {python}-{abi}-{platform}
  -- in principle, there can be multiple tags, but our definition of
  -- wheel currently doesn't support that.
);

-- TODO: figure out what to do about RECORD.jws and RECORD.p7s (both
-- are used for digital signatures).
create table wheels(
-- TODO: version and platform cannot be artibrary strings, we need
-- some check constraints here.
-- Due to https://packaging.python.org/en/latest/specifications/name-normalization/#name-normalization
-- it's OK to restrict names here to only normalized names.
  distribution text not null check(distribution regexp '^([a-z0-9]|[a-z0-9]+(_?[a-z0-9]+)+)$'),
  -- how version is specified:
  -- https://packaging.python.org/en/latest/specifications/version-specifiers/#version-specifiers
  -- [N!]N(.N)*[{a|b|rc}N][.postN][.devN] this is the general semver
  -- format in bizarro notation that probably applies to anything that
  -- has "dist" in it.
  version text not null default '$version',
  pep integer not null default 491,
  build text check(build glob '[0-9$]*'),
  root text,
  -- this controls where the wheel is unpacked.
  root_is_purelib integer not null check(root_is_purelib in (0, 1)) default 0,
  metadata integer,
  foreign key(pep) references version_formats(pep),
  foreign key(metadata) references wheel_metadata_v1_0(rowid),
  --  TODO: probably metadata needs to be included here as well, but I
  --  still cannot decide whether recipes should be able to generate
  --  it, or should it be configured statically per wheel
  unique(distribution, version, build, root, root_is_purelib)
);

-- In principle, it's possible to have multiple tags in the same wheel
create table wheel_tags(
  platform text not null,
  python text not null default 'cp',
  -- this will add "d" to ABI tag
  debugging integer not null check(debugging in (0, 1)) default 0,
  -- this will add "m" to ABI tag
  pymalloc integer not null check(pymalloc in (0, 1)) default 0,
  -- this will add "u" to ABI tag, seems to be irrelevant since Python
  -- 3.3 and pep-0393
  wide_unicode integer not null check(wide_unicode in (0, 1)) default 0,
  abi text not null default '$abi',
  wheel integer not null,
  foreign key(wheel) references wheels(rowid),
  foreign key(platform) references platforms(platform),
  foreign key(python) references pythons(python),
  foreign key(abi) references abis(abi)
);

create table variables(
  vname text not null primary key,
  ivalue integer,
  tvalue text
);

insert into variables(vname, tvalue)
values ('version', '1.0.0'), ('platform', 'any'), ('abi', 'none');

create table condition_ops(
  op text not null primary key
);

insert into condition_ops(op) values ('='), ('<'), ('>'), ('!='), ('>='), ('<=');

create table commands(
  command text primary key,
  collect integer not null check(collect in (0, 1)) default 1
);

create table destinations(
  destination text not null primary key
);

create table sources(
  destination text,
  file text,
  foreign key(destination) references destinations(destination),
  foreign key(file) references files(fpath),
  check(file is null + destination is null = 1)
);

create table conditions(
  condition text not null primary key,
  condition_variable text,
  condition_op text,
  condition_value text,
  foreign key(condition_variable) references variables(vname),
  foreign key(condition_op) references condition_ops(op),
  unique(condition, condition_variable, condition_op, condition_value)
);

-- Recipes tie wheels to destinations (the files that are produced
-- from sources using commands).  Condition variables control whether
-- recipe is used when creating a wheel.  For example, one may want
-- to define a wheel with a variable in its name.  When the wheel
-- is created, the variable's value is substituted for its name
-- then, when looking up recipes to use to populate the wheel, the
-- variable can be again checked to see if the recipe needs to be
-- executed.
create table recipes(
  label text not null primary key,
  command text,
  foreign key(command) references commands(command),
  unique(command, label)
);

create table wheel_recipes(
  recipe text not null,
  wheel integer not null,
  foreign key(recipe) references recipes(label),
  foreign key(wheel) references wheels(rowid),
  unique(recipe, wheel)
);

create table condition_recipes(
  recipe text not null,
  condition text not null,
  foreign key(recipe) references recipes(label),
  foreign key(condition) references conditions(condition),
  unique(recipe, condition)
);

create table destination_recipes(
  recipe text not null,
  destination text not null,
  foreign key(recipe) references recipes(label),
  foreign key(destination) references destinations(destination),
  unique(recipe, destination)
);

create table source_recipes(
  recipe text not null,
  src integer not null,
  foreign key(recipe) references recipes(label),
  foreign key(src) references sources(rowid),
  unique(recipe, src)
);
