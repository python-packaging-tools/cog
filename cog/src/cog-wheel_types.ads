with Ada.Containers;
with Ada.Containers.Indefinite_Vectors;

package Cog.Wheel_Types is

   package Ac renames Ada.Containers;

   use Ac;

   Multiple_Wheel_Match : exception;
   No_Matching_Wheel : exception;

   type Wheel_Tag
     (Platform_Length : Natural;
      Python_Length   : Natural;
      Abi_Length      : Natural) is record
         Platform     : aliased String (1 .. Platform_Length);
         Python       : aliased String (1 .. Python_Length );
         Abi          : aliased String (1 .. Abi_Length);
         Debugging    : Boolean := False;
         Pymalloc     : Boolean := False;
         Wide_Unicode : Boolean := False;
      end record;

   type Wheel_Tag_Access is access all Wheel_Tag;

   package Tags_Vectors is new Indefinite_Vectors
     (Index_Type   => Positive,
      Element_Type => Wheel_Tag);

   type Wheel
     (Distribution_Length : Natural;
      Version_Length      : Natural;
      Build_Length        : Natural) is record
         Distribution    : aliased String (1 .. Distribution_Length);
         Version         : aliased String (1 .. Version_Length);
         Build           : aliased String (1 .. Build_Length);
         Root_Is_Purelib : Boolean := False;
         Id              : Integer := -1;
         Tags            : Tags_Vectors.Vector;
      end record;

   function Tag_Abi (Tag : Wheel_Tag) return String;

   function Tags_To_String (Tags : Tags_Vectors.Vector) return String;

   function Wheel_File_Name (Arch : Wheel) return String;

end Cog.Wheel_Types;
