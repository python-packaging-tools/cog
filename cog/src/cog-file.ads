with GNATCOLL.SQL.Sqlite.Gnade;

package Cog.File is

   package Gnade renames GNATCOLL.SQL.Sqlite.Gnade;

   procedure Add_File (Db_File : String; File : String);
   procedure Add_File (Db : Gnade.Database; File : String);

   procedure Add_Derived_File (Db_File : String; File : String; Recipe : Integer);
   procedure Add_Derived_File (Db : Gnade.Database; File : String; Recipe : Integer);

end Cog.File;
