with Ada.Containers;
with Ada.Containers.Indefinite_Vectors;
with Ada.Containers.Indefinite_Holders;
with GNATCOLL.SQL.Sqlite.Gnade;
with Cog.Wheel_Types;
with GNATCOLL.Strings;

package Cog.Wheel_Metadata is

   package Ac renames Ada.Containers;
   package Gnade renames GNATCOLL.SQL.Sqlite.Gnade;
   package Gs renames GNATCOLL.Strings;
   package Cwt renames Cog.Wheel_Types;

   use Ac;

   type Metadata_Version is (Mv_1_0, Mv_1_1, Mv_1_2, Mv_2_1, Mv_2_2, Mv_2_3);

   function Mv_To_String (Mv : Metadata_Version) return String;

   function Mv_From_String (Mv : String) return Metadata_Version;

   type Meta_Content_Type is (Plain, Rst, Markdown);

   type Markdown_Variant is (Gfm, Common_Makr);

   type Charset is (Utf8);

   type Description_Content_Type is record
      Content_Type : Meta_Content_Type := Plain;
      Cs           : Charset           := Utf8;
      Variant      : Markdown_Variant  := Common_Makr;
   end record;

   type Semver_Release is array (Positive range <>) of Natural;

   type Prerelease_Kind is (A, B, Rc, None);

   --  TODO: Apparently, there are "local" versions that look like:
   --  semver+([a-zA-Z9-0]|[a-zA-Z9-0][a-zA-Z9-0.]*[a-zA-Z9-0]).  I
   --  don't know if this is really ever used, and I'm not sure if to
   --  make this part of the semver, or supply it additionally when an
   --  entity can have such a version (eg. requires_python cannot have
   --  that).

   --  This absurd notation once again:
   --  [N!]N(.N)*[{a|b|rc}N][.postN][.devN]
   type Semver (N_Releases : Positive) is record
      Epoch          : Natural;
      Release        : Semver_Release (1 .. N_Releases);
      Prerelease_Tag : Prerelease_Kind := None;
      Prerelease_N   : Natural := 0;
      Post_Release   : Natural := 0;
      Dev_Release    : Natural := 0;
   end record;

   function Make_Semver
     (Epoch          : Natural;
      Release        : Semver_Release;
      Prerelease_Tag : Prerelease_Kind := None;
      Prerelease_N   : Natural := 0;
      Post_Release   : Natural := 0;
      Dev_Release    : Natural := 0) return Semver;

   type Version_Op is
     (Vop_Gt, Vop_Lt, Vop_Eq, Vop_Neq, Vop_Gtq, Vop_Ltq, Vop_Caretq, Vop_Tildeq);

   type Version_Requirement (N_Releases : Positive) is record
      Version : Semver (N_Releases);
      Op : Version_Op := Vop_Eq;
   end record;

   No_Version_Requirement : Version_Requirement :=
     (N_Releases => 1, Version => (N_Releases => 1, others => <>), others => <>);

   package Version_Requirement_Vectors is new Indefinite_Vectors
     (Index_Type   => Positive,
      Element_Type => Version_Requirement);

   type Dist (D_Length : Natural; N_Releases : Natural) is record
      Name : String (1 .. D_Length);
      Op: Version_Op := Vop_Eq;
      Version : Semver (N_Releases);
      --  TODO: this can also have a part separated by semicolon,
      --  eg. "extra == 'dev'.  Need to find the exact format spec.
   end record;

   package Dist_Vectors is new Indefinite_Vectors
     (Index_Type   => Positive,
      Element_Type => Dist);

   type Platform (P_Length : Natural) is record
      P : String (1 .. P_Length);
   end record;

   type Author (A_Length : Natural) is record
      A : String (1 .. A_Length);
   end record;

   type Author_Email (A_Length : Natural) is record
      A : String (1 .. A_Length);
   end record;

   type Keyword (K_Length : Natural) is record
      K : String (1 .. K_Length);
   end record;

   type Url (U_Length : Natural) is record
      U : String (1 .. U_Length);
   end record;

   No_Url : Url := (U_Length => 0, others => <>);

   function Make_Url (U : String) return Url;

   type Dynamic (D_Length : Natural) is record
      D : String (1 .. D_Length);
   end record;

   type Provides_External (P_Length : Natural) is record
      P : String (1 .. P_Length);
   end record;

   type Extra (E_Length : Natural) is record
      E : String (1 .. E_Length);
   end record;

   --  TODO: This is a sequence of strings
   type Classifier (C_Length : Natural) is record
      E : String (1 .. C_Length);
   end record;

   package Platform_Vectors is new Indefinite_Vectors
     (Index_Type   => Positive,
      Element_Type => Platform);

   package Author_Vectors is new Indefinite_Vectors
     (Index_Type   => Positive,
      Element_Type => Author);

   package Author_Email_Vectors is new Indefinite_Vectors
     (Index_Type   => Positive,
      Element_Type => Author_Email);

   package Keyword_Vectors is new Indefinite_Vectors
     (Index_Type   => Positive,
      Element_Type => Keyword);

   package Dynamic_Vectors is new Indefinite_Vectors
     (Index_Type   => Positive,
      Element_Type => Dynamic);

   package Url_Vectors is new Indefinite_Vectors
     (Index_Type   => Positive,
      Element_Type => Url);

   package Provides_External_Vectors is new Indefinite_Vectors
     (Index_Type   => Positive,
      Element_Type => Provides_External);

   package Extra_Vectors is new Indefinite_Vectors
     (Index_Type   => Positive,
      Element_Type => Extra);

   package Classifier_Vectors is new Indefinite_Vectors
     (Index_Type   => Positive,
      Element_Type => Classifier);

   type Wheel_Metadata
     (Summary_Len      : Natural;
      Description_Len  : Natural;
      Homepage_Len     : Natural;
      License_Len      : Natural;
      Download_Url_Len : Natural)
      is record
         Mv                 : Metadata_Version := Mv_2_3;
         Summary            : String (1 .. Summary_Len);
         Description        : String (1 .. Description_Len);
         Homepage           : Url (Homepage_Len);
         Download_Url       : Url (Download_Url_Len);
         License            : String (1 .. License_Len);
         Requires_Python    : Version_Requirement_Vectors.Vector;
         Dct                : Description_Content_Type;
         Provides_Dists     : Dist_Vectors.Vector;
         Obsoletes_Dists    : Dist_Vectors.Vector;
         Requires_Dists     : Dist_Vectors.Vector;
         Dynamic            : Dynamic_Vectors.Vector;
         Platforms          : Platform_Vectors.Vector;
         Authors            : Author_Vectors.Vector;
         Maintainers        : Author_Vectors.Vector;
         Author_Emails      : Author_Email_Vectors.Vector;
         Maintainer_Emails  : Author_Email_Vectors.Vector;
         Keywords           : Keyword_Vectors.Vector;
         Requires_Externals : Provides_External_Vectors.Vector;
         Provides_Extras    : Extra_Vectors.Vector;
         Project_Urls       : Url_Vectors.Vector;
         Classifiers        : Classifier_Vectors.Vector;
      end record;

   function Make_Wheel_Metadata
     (Summary          : String := "";
      Description      : String := "";
      Homepage         : Url := No_Url;
      License          : String := "";
      Download_Url     : Url := No_Url;
      Dct              : Description_Content_Type := (others => <>);
      Mv               : Metadata_Version := Mv_2_3) return Wheel_Metadata;

   function Fetch_Wheel_Metadata (Db: Gnade.Database; Arch : Cwt.Wheel)
                                 return Wheel_Metadata;

   type Column_Reader is abstract tagged null record;

   procedure Call
     (Cr     : in out Column_Reader;
      Db     : Gnade.Database;
      Column : String;
      Md     : Wheel_Metadata) is abstract;

   package Cr_Holder is new Ada.Containers.Indefinite_Holders
     (Column_Reader'Class);

   type Column_Fetcher (N : Natural) is record
      Reader : Cr_Holder.Holder;
      Column : String (1 .. N);
   end record;

   function Make_Column_Fetcher (Column : String; Reader : Column_Reader'Class)
                                return Column_Fetcher;

   package Column_Fetcher_Vectors is new Indefinite_Vectors
     (Index_Type   => Positive,
      Element_Type => Column_Fetcher);

   type Dist_Reader is new Column_Reader with null record;
   type Dynamic_Reader is new Column_Reader with null record;
   type Platform_Reader is new Column_Reader with null record;
   type Author_Reader is new Column_Reader with null record;
   type Author_Email_Reader is new Column_Reader with null record;
   type Keyword_Reader is new Column_Reader with null record;
   type Provides_External_Reader is new Column_Reader with null record;
   type Extra_Reader is new Column_Reader with null record;
   type Url_Reader is new Column_Reader with null record;
   type classifier_Reader is new Column_Reader with null record;

   overriding procedure Call
     (Cr     : in out Dist_Reader;
      Db     : Gnade.Database;
      Column : String;
      Md     : Wheel_Metadata);

   overriding procedure Call
     (Cr     : in out Dynamic_Reader;
      Db     : Gnade.Database;
      Column : String;
      Md     : Wheel_Metadata);

   overriding procedure Call
     (Cr     : in out Platform_Reader;
      Db     : Gnade.Database;
      Column : String;
      Md     : Wheel_Metadata);

   overriding procedure Call
     (Cr     : in out Author_Reader;
      Db     : Gnade.Database;
      Column : String;
      Md     : Wheel_Metadata);

   overriding procedure Call
     (Cr     : in out Author_Email_Reader;
      Db     : Gnade.Database;
      Column : String;
      Md     : Wheel_Metadata);

   overriding procedure Call
     (Cr     : in out Keyword_Reader;
      Db     : Gnade.Database;
      Column : String;
      Md     : Wheel_Metadata);

   overriding procedure Call
     (Cr     : in out Provides_External_Reader;
      Db     : Gnade.Database;
      Column : String;
      Md     : Wheel_Metadata);

   overriding procedure Call
     (Cr     : in out Extra_Reader;
      Db     : Gnade.Database;
      Column : String;
      Md     : Wheel_Metadata);

   overriding procedure Call
     (Cr     : in out Url_Reader;
      Db     : Gnade.Database;
      Column : String;
      Md     : Wheel_Metadata);

   overriding procedure Call
     (Cr     : in out Classifier_Reader;
      Db     : Gnade.Database;
      Column : String;
      Md     : Wheel_Metadata);

   procedure Set (Db_File : String; Chosen : Integer; Key : String; Value : String);
   procedure Set (Db : Gnade.Database; Chosen : Integer; Key : String; Value : String);

   procedure Show (Db_File : String; Chosen : Integer);
   procedure Show (Db : Gnade.Database; Chosen : Integer);

   procedure Append (Db_File : String; Chosen : Integer; Key : String; Value : String);
   procedure Append (Db : Gnade.Database; Chosen : Integer; Key : String; Value : String);

end Cog.Wheel_Metadata;
