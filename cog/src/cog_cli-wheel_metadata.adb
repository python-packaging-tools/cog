with Ada.Text_IO;
with Ada.Strings.Unbounded;
with GNATCOLL.Traces;
with Cog_Cli.Options;
with Cog_Cli.Xml;
with Cog.Wheel;
with Cog.Wheel_Types;
with Cog.Wheel_Metadata;

package body Cog_Cli.Wheel_Metadata is

   package Ati renames Ada.Text_IO;
   package Asu renames Ada.Strings.Unbounded;
   package Gt renames GNATCOLL.Traces;
   package Cco renames Cog_Cli.Options;
   package Ccx renames Cog_Cli.Xml;
   package Cw renames Cog.Wheel;
   package Cwt renames Cog.Wheel_Types;
   package Cwm renames Cog.Wheel_Metadata;

   procedure Parse_Set (C : in out Cco.Context) is
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : aliased Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-wheel-metadata-set"));
      Chosen   : Integer;
   begin
      Cco.Parse_Common_Select_Wheel
        (C, Cl, Help, "wheel metadata set", 2, 2, Chosen, Continue);

      if Continue then
         Cwm.Set
           (Asu.To_String (Cco.Db_File),
            Chosen,
            Cco.Argument (Cl),
            Cco.Argument (Cl, Cl.Index + 1));
      end if;

   end Parse_Set;

   procedure Parse_List (C : in out Cco.Context) is
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-wheel-metadata-list"));
   begin
      Cco.Parse_Common (C, Cl, Help, "wheel list", 0, 1, Continue);
      if Continue then
         if Cco.Has_More (Cl, 1) then
            Cw.List (Asu.To_String (Cco.Db_File), Cco.Argument (Cl));
         else
            Cw.List (Asu.To_String (Cco.Db_File));
         end if;
      end if;
   end Parse_List;

   procedure Parse_Remove (C : in out Cco.Context) is
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-wheel-metadata-remove"));
   begin
      Cco.Parse_Common (C, Cl, Help, "wheel metadata remove", 2, Continue);
   end Parse_Remove;

   procedure Parse_Show (C : in out Cco.Context) is
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-wheel-metadata-show"));
      Chosen   : Integer;
   begin
      Cco.Parse_Common_Select_Wheel
        (C, Cl, Help, "wheel metadata show", 0, 0, Chosen, Continue);
      if Continue then
         Cwm.Show (Asu.To_String (Cco.Db_File), Chosen);
      end if;
   end Parse_Show;

   procedure Parse_Append  (C : in out Cco.Context) is
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-wheel-metadata-append"));
      Chosen   : Integer;
   begin
      Cco.Parse_Common_Select_Wheel
        (C, Cl, Help, "wheel metadata append", 2, 2, Chosen, Continue);

      if Continue then
         Cwm.Append
           (Asu.To_String (Cco.Db_File),
            Chosen,
            Cco.Argument (Cl),
            Cco.Argument (Cl, Cl.Index + 1));
      end if;

   end Parse_Append;

   procedure Parse (C : in out Cco.Context) is
      Lc   : Cco.Context;
      Help : aliased Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-wheel-metadata"));
   begin
      Cco.Include (Lc, "--help", "h", Help'Unchecked_Access);
      Lc.Index := C.Index;
      Cco.Parse (Lc);

      if Cco.Has_More (Lc) then
         declare
            Subcommand : constant String := Cco.Argument (Lc);
         begin
            Gt.Trace (Log, "Parsing subcommand: " & Subcommand);
            Lc.Index := Lc.Index + 1;
            Gt.Trace (Log, "Set index to: " & Integer'Image (Lc.Index));
            if Subcommand = "set" then
               Gt.Trace (Log, "Set subcommand");
               Parse_Set (Lc);
            elsif Subcommand = "show" then
               Gt.Trace (Log, "Show subcommand");
               Parse_Show (Lc);
            elsif Subcommand = "remove" then
               Gt.Trace (Log, "Remove subcommand");
               Parse_Remove (Lc);
            elsif Subcommand = "append" then
               Gt.Trace (Log, "Append subcommand");
               Parse_Append (Lc);
            else
               raise Cco.Unknown_Subcommand
                 with Subcommand & " is not a known subcommand";
            end if;
         end;
      end if;

   end Parse;

end Cog_Cli.Wheel_Metadata;
