with Ada.Strings.Unbounded;

package Cog is
   pragma Elaborate_Body;

   package Asu renames Ada.Strings.Unbounded;

   Selected_Wheel_Pattern : Asu.Unbounded_String := Asu.Null_Unbounded_String;
   Selected_Wheel_Rowid : Integer := -1;

   Selected_Recipe_Label : Asu.Unbounded_String := Asu.Null_Unbounded_String;
   Selected_Recipe_Rowid : Integer := -1;

end Cog;
