with Ada.Strings.Fixed;
with Ada.Directories.Hierarchical_File_Names;
with Ada.Directories;
with Ada.Strings.Unbounded;

package body Cog.Fs is

   package Asu renames Ada.Strings.Unbounded;
   package Asf renames Ada.Strings.Fixed;
   package Ad renames Ada.Directories;
   package Adh renames Ada.Directories.Hierarchical_File_Names;

   function Relpath (Of_File : String; Relative_To : String) return String is
   begin
      if Asf.Index (Relative_To, "..") > 0 then
         raise Cannot_Compute_Relpath
           with "Cannot compute relative path when " &
             Relative_To & " uses parent (..) directory";
      elsif Adh.Is_Relative_Name (Of_File) then
         declare
            Abs_Of_File : constant String :=
              Adh.Compose (Ad.Current_Directory, Of_File);
         begin
            if Adh.Is_Relative_Name (Relative_To) then
               declare
                  Abs_Relative_To : constant String :=
                    Adh.Compose (Ad.Current_Directory, Relative_To);
               begin
                  return Relpath (Abs_Of_File, Abs_Relative_To);
               end;
            else
               return Relpath (Abs_Of_File, Relative_To);
            end if;
         end;
      elsif Adh.Is_Relative_Name (Relative_To) then
         declare
            Abs_Relative_To : constant String :=
              Adh.Compose (Ad.Current_Directory, Relative_To);
         begin
            return Relpath (Of_File, Abs_Relative_To);
         end;
      elsif Adh.Initial_Directory (Of_File) /=
        Adh.Initial_Directory (Relative_To) then
         raise Cannot_Compute_Relpath 
           with Of_File & " and " & Relative_To
             & "aren't in the same filesystem";
      else
         declare
            Prefix : Asu.Unbounded_String :=
              Asu.To_Unbounded_String (Relative_To);
            Parents : Asu.Unbounded_String := Asu.Null_Unbounded_String;
            --  This is necessary for comparison between unbounded strings.
            use Asu;
         begin
            loop
               if Asu.Length (Prefix) <= Of_File'Length
                 and then Asu.To_String (Prefix) = Of_File
                 (Of_File'First .. Of_File'First + Asu.Length (Prefix) - 1) then
                  if Parents = Asu.Null_Unbounded_String then
                     return Of_File (Of_File'First + Asu.Length (Prefix) + 1
                                       .. Of_File'Last);
                  else
                     return Adh.Compose 
                       (Asu.To_String (Parents),
                        Of_File (Of_File'First + Asu.Length (Prefix) + 1
                                   .. Of_File'Last));
                  end if;
               end if;
               Prefix := Asu.To_Unbounded_String
                 (Ad.Containing_Directory (Asu.To_String (Prefix)));
               if Asu.Length(Parents) = 0 then
                  Parents := Asu.To_Unbounded_String ("..");
               else
                  Parents := Asu.To_Unbounded_String
                    (Ad.Compose (Asu.To_String (Parents), ".."));
               end if;
            end loop;
         end;
      end if;
   end Relpath;

end Cog.Fs;
