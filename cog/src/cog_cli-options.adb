with Ada.Text_IO;
with Ada.Command_Line;
with Ada.Directories;
with Ada.Directories.Hierarchical_File_Names;
with GNATCOLL.Traces;
with Cog_Cli.Xml;
with Cog.Wheel;

package body Cog_Cli.Options is

   package Acl renames Ada.Command_Line;
   package Ati renames Ada.Text_IO;
   package Ad renames Ada.Directories;
   package Adh renames Ada.Directories.Hierarchical_File_Names;
   package Gt renames GNATCOLL.Traces;
   package Ccx renames Cog_Cli.Xml;

   use Option_Handler_Maps;

   type Verb is (Short, Long, Subcommand);

   function Next_Verb (Current : String) return Verb is
   begin
      if Current'Length = 0 then
         return Subcommand;
      elsif Current'Length = 1 then
         if Current (Current'First) = '-' then
            return Short;
         else
            return Subcommand;
         end if;
      elsif Current (Current'First) = '-' then
         if Current (Current'First + 1) = '-' then
            return Long;
         else
            return Short;
         end if;
      else
         return Subcommand;
      end if;
   end Next_Verb;

   function Nargs (H : Option_Handler) return Integer is
   begin
      --  TODO: find a way to make this abstract.
      return 0;
   end;

   overriding function Nargs (H : Help_Handler) return Integer is
   begin
      return 1;
   end Nargs;

   overriding function Nargs (H : Value_Handler) return Integer is
   begin
      return 2;
   end Nargs;

   overriding function Nargs (H : Boolean_Handler) return Integer is
   begin
      return 1;
   end Nargs;

   function Call (H : in out Option_Handler; Args : Option_Arguments) return Boolean is
   begin
      --  TODO: find a way to make this abstract.
      return True;
   end Call;

   overriding function Call (H : in out Help_Handler; Args : Option_Arguments) return Boolean is
   begin
      Ati.Put_Line (Ccx.Cli_Help (Asu.To_String (H.Help_Key)));
      return True;
   end Call;

   overriding function Call (H : in out Value_Handler; Args : Option_Arguments) return Boolean is
   begin
      H.Option_Value := Args (2);
      H.Set := True;
      return False;
   end Call;

   overriding function Call (H : in out Boolean_Handler; Args : Option_Arguments) return Boolean is
   begin
      H.Set := True;
      return False;
   end Call;

   procedure Include
     (C : in out Context; Key : String; Short : String; Handler : Oh_Access) is
   begin
      C.Known_Options.Include (Key, Handler);
      C.Known_Options.Include (Short, Handler);
   end Include;

   function Has_More (C : in out Context) return Boolean is
   begin
      return Has_More (C, 1);
   end Has_More;

   function Has_More (C : in out Context; N : Integer) return Boolean is
   begin
      return C.Index + N <= Acl.Argument_Count + 1 and then not C.Completed;
   end Has_More;

   function Argument (C : in out Context) return String is
   begin
      return Acl.Argument (C.Index);
   end;

   function Argument (C : in out Context; I : Integer) return String is
   begin
      return Acl.Argument (I);
   end;

   function Parse_Short_Options (Raw : String; C : in out Context) return Boolean is
      use Asu;
      Opt_String : String (1 .. 1);
      Upt_String : Asu.Unbounded_String;
      Handler    : Oh_Access := null;
      Result     : Boolean;
   begin
      --  TODO: extend to any number of arguments
      for I in Raw'Range loop
         if not C.Is_Argument then
            Opt_String := (1 => Raw (I));
            Upt_String := Asu.To_Unbounded_String (Opt_String);
            if C.Known_Options.Contains (Opt_String) then
               Handler := C.Known_Options (Opt_String);
               if Nargs (Handler.all) = 1 then
                  Result := Call (Handler.all, (1 => Asu.To_Unbounded_String (Raw)));
                  if Result then
                     return Result;
                  end if;
               else
                  C.Is_Argument := True;
               end if;
            else
               raise Unsupported_Option with Opt_String & " is not supported";
            end if;
         else
            -- Parsing argument to options with arguments.  Assuming
            -- the rest of the string is the argument's value.
            declare
               Arg_Value : constant String := Raw (I .. Raw'Last);
               Urg_Value : constant Asu.Unbounded_String
                 := Asu.To_Unbounded_String (Arg_Value);
            begin
               if Arg_Value'Length > 0 and then Arg_Value (Arg_Value'First) = '=' then
                  Result := Call
                    (Handler.all,
                     (1 => Upt_String,
                      2 => Asu.To_Unbounded_String
                        (Arg_Value (Arg_Value'First + 1 .. Arg_Value'Last))));
               else
                  Result := Call (Handler.all, (1 => Upt_String, 2 => Urg_Value));
               end if;
               C.Is_Argument := False;
               if Result then
                  return Result;
               end if;
            end;
         end if;
      end loop;
      if C.Is_Argument then
         C.Argument_Is_Short := True;
      end if;
      return False;
   end Parse_Short_Options;

   function Long_Option_Name (Arg : String) return String is
   begin
      for I in Arg'Range loop
         if Arg (I) = '=' then
            return Arg (Arg'First .. I - 1);
         end if;
      end loop;
      return Arg;
   end Long_Option_Name;

   function Parse_Long_Option
     (C : in out Context; Arg : String; I : Integer) return Boolean is
      Opt_Name : constant String := Long_Option_Name (Arg);
      Upt_Name : constant Asu.Unbounded_String :=
        Asu.To_Unbounded_String (Opt_Name);
      Urg : constant Asu.Unbounded_String := Asu.To_Unbounded_String (Arg);
      Handler : Oh_Access;
   begin
      if C.Known_Options.Contains (Opt_Name) then
         Handler := C.Known_Options (Opt_Name);
         Ati.Put_Line ("nargs for: " & Arg & " is: " & Integer'Image (Nargs (Handler.all)));
         if Opt_Name /= Arg and Nargs (Handler.all) = 1 then
            raise Option_Takes_No_Arguments
              with Opt_Name & " takes no arguments";
         end if;
         if Nargs (Handler.all) = 1 then
            C.Index := C.Index + 1;
            return Call (Handler.all,  (1 => Urg));
         end if;
         if I = Acl.Argument_Count then
            raise Option_Missing_Argument with Opt_Name & " requires argument";
         end if;
         if Opt_Name /= Arg then
            return Call
              (Handler.all,
               (1 => Upt_Name,
                2 => Asu.To_Unbounded_String
                  (Arg (Opt_Name'Last + 2 .. Arg'Last))));
         end if;
         C.Is_Argument := True;
      else
         raise Unsupported_Option with Opt_Name & " is not supported";
      end if;
      return False;
   end Parse_Long_Option;

   procedure Parse_Options (C : in out Context) is
      Handler : Oh_Access;
   begin
      for I in C.Index .. Acl.Argument_Count loop
         declare
            Arg : constant String := Acl.Argument (I);
         begin
            if not C.Is_Argument then
               case Next_Verb (Arg) is
                  when Long =>
                     if Parse_Long_Option (C, Arg, I) then
                        C.Completed := True;
                        C.Index := I + 1;
                        return;
                     end if;
                  when Short =>
                     if Parse_Short_Options (Arg (2 .. Arg'Length), C) then
                        C.Completed := True;
                        C.Index := I + 1;
                        return;
                     end if;
                  when Subcommand =>
                     C.Index := I;
                     return;
               end case;
            elsif C.Argument_Is_Short then
               declare
                  Previous : constant String := Acl.Argument (I - 1);
                  Previous_Short : constant String := 
                    Previous (Previous'Last .. Previous'Last);
               begin
                  Handler := C.Known_Options (Previous_Short);
                  C.Is_Argument := False;
                  C.Argument_Is_Short := False;
                  if Call (Handler.all,
                           (1 => Asu.To_Unbounded_String (Previous_Short),
                            2 => Asu.To_Unbounded_String (Arg))) then
                     C.Completed := True;
                     C.Index := I + 1;
                     return;
                  end if;
               end;
            else
               Handler := C.Known_Options (Acl.Argument (I - 1));
               C.Is_Argument := False;
               if Call (Handler.all,
                        (1 => Asu.To_Unbounded_String (Acl.Argument (I - 1)),
                         2 => Asu.To_Unbounded_String (Arg))) then
                  C.Completed := True;
                  C.Index := I + 1;
                  return;
               end if;
            end if;
         end;
      end loop;
      --  This means we parsed everything
      C.Index := Acl.Argument_Count + 1;
   end Parse_Options;

   procedure Parse (C : in out Context) is
   begin
      Parse_Options (C);
   end Parse;

   procedure Find_Database is
   begin
      if Asu.To_String (Db_File) /= "" then
         return;
      end if;
      declare
         D : Asu.Unbounded_String :=
           Asu.To_Unbounded_String (Ad.Current_Directory);
      begin
         loop
            if Adh.Is_Root_Directory_Name (Asu.To_String (D)) then
               raise Cannot_Find_Database
                 with Ad.Current_Directory &
                   " and ancestor directories don't contain wheels.db." &
                   "  Create it using `cog init'";
            end if;
            declare
               Candidate : constant String :=
                 Adh.Compose (Asu.To_String (D), "wheels", ".db");
            begin
               if Ad.Exists (Candidate) then
                  Db_File := Asu.To_Unbounded_String (Candidate);
                  exit;
               else
                  D := Asu.To_Unbounded_String
                    (Ad.Containing_Directory (Asu.To_String (D)));
               end if;
            end;
         end loop;
      end;
   end Find_Database;

   procedure Parse_Common
     (C         : in out Context;
      Cl        : in out Context;
      Help      : in out Help_Handler;
      Cmd       : String;
      Nargs_Min : Integer;
      Nargs_Max : Integer;
      Continue  : out Boolean) is
      Database_Opt : aliased Value_Handler;
      Help_Var : aliased Help_Handler := Help;
   begin
      Include (Cl, "--help", "h", Help_Var'Unchecked_Access);
      Include (Cl, "--database", "d", Database_Opt'Unchecked_Access);
      Cl.Index := C.Index;
      Parse (Cl);

      if Database_Opt.Set then
         declare
            File : constant String := Asu.To_String (Database_Opt.Option_Value);
         begin
         Gt.Trace (Log, "Setting database to: " & File);
         if not Ad.Exists (File) then
            raise Cannot_Find_Database
              with File & " doesn't exist.  Create one with `cog init`";
            end if;
         end;
         Db_File := Database_Opt.Option_Value;
      end if;

      if Cl.Completed then
         Continue := False;
         return;
      end if;
      if Has_More (Cl, Nargs_Max + 1) then
         raise Command_Arguments_Count_Mismatch
           with "`" & Cmd & "` requires at most" &
             Integer'Image (Nargs_Max) & " arguments";
      end if;
      if not Has_More (Cl, Nargs_Min) then
         raise Command_Arguments_Count_Mismatch
           with "`" & Cmd & "` requires at least" &
             Integer'Image (Nargs_Min) & " arguments";
      end if;
      Find_Database;
      Continue := True;
   end Parse_Common;
   
   procedure Parse_Common
     (C         : in out Context;
      Cl        : in out Context;
      Help      : in out Help_Handler;
      Cmd       : String;
      Nargs     : Integer;
      Continue  : out Boolean) is
      Help_Var : aliased Help_Handler := Help;
   begin
      Parse_Common (C, Cl, Help_Var, Cmd, Nargs, Nargs, Continue);
   end Parse_Common;

   procedure Parse_Common_Select_Wheel
     (C         : in out Context;
      Cl        : in out Context;
      Help      : in out Help_Handler;
      Cmd       : String;
      Nargs_Min : Integer;
      Nargs_Max : Integer;
      Chosen    : out Integer;
      Continue  : out Boolean) is
      Wheel     : aliased Value_Handler;
      Wheel_Id  : aliased Value_Handler;
   begin
      Include (Cl, "--wheel", "w", Wheel'Unchecked_Access);
      Include (Cl, "--wheel-id", "i", Wheel_Id'Unchecked_Access);
      Parse_Common (C, Cl, Help, Cmd, Nargs_Min, Nargs_Max, Continue);

      if Wheel.Set and Wheel_Id.Set then
         raise Options_Mutually_Exclusive
           with "--wheel and --wheel-id are mutually exclusive";
      end if;

      if Continue then
         if Wheel.Set then
            Cog.Wheel.Choose
              (Asu.To_String (Db_File), Asu.To_String (Wheel.Option_Value));
            Chosen := Cog.Selected_Wheel_Rowid;
         elsif Wheel_Id.Set then
            Chosen := Integer'Value (Asu.To_String (Wheel_Id.Option_Value));
         else
            Chosen := Cog.Wheel.Chosen (Asu.To_String (Db_File));
         end if;
      end if;
   end Parse_Common_Select_Wheel;

end Cog_Cli.Options;
