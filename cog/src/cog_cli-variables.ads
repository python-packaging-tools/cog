with Cog_Cli.Options;

package Cog_Cli.Variables is

   Unsupported_Variable_Type : exception;

   procedure Parse (C : in out Cog_Cli.Options.Context);

end Cog_Cli.Variables;
