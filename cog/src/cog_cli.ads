with GNATCOLL.Traces;

package Cog_Cli is

   package Gt renames GNATCOLL.Traces;

   procedure Parse;
   
private

   Log : constant Gt.Trace_Handle := Gt.Create ("cog_cli", Stream => "&2");

end Cog_Cli;
