with Ada.Containers;
with Ada.Containers.Indefinite_Vectors;
with GNATCOLL.SQL.Sqlite.Gnade;
with Cog.Wheel;

package Cog.Recipes is

   package Ac renames Ada.Containers;
   package Gnade renames GNATCOLL.SQL.Sqlite.Gnade;
   package Cw renames Cog.Wheel;

   Unreqcognized_Condition : exception;

   Unreqcognized_Recipe : exception;

   use Ac;

   type Destination (D_Length : Natural) is record
      D : aliased String (1 .. D_Length);
   end record;

   type Destination_Access is access all Destination;

   function Make_Destination (D : String) return Destination;

   package Destination_Vectors is new Indefinite_Vectors
     (Index_Type   => Positive,
      Element_Type => Destination);

   type Source (S_Length : Natural) is record
      S : aliased String (1 .. S_Length);
      Derived : Boolean := False;
   end record;

   type Source_Access is access all Source;

   function Make_Source (S : String; Derived : Boolean := False)
                        return Source;

   package Source_Vectors is new Indefinite_Vectors
     (Index_Type   => Positive,
      Element_Type => Source);

   type Command (C_Length : Natural) is record
      C : aliased String (1 .. C_Length);
   end record;

   function Make_Command (C : String) return Command;

   type Condition_Op is (Eq, Gt, Lt, Neq, Geq, Leq);

   function Op_From_String (Raw : String) return Condition_Op;

   type Condition (L_Length : Natural; V_Length : Natural; C_Length : Natural)
     is record
      Label    : aliased String (1 .. L_Length);
      Variable : aliased String (1 .. V_Length);
      Op       : Condition_Op;
      Value    : aliased String (1 .. C_Length);
   end record;

   type Condition_Access is access all Condition;

   package Condition_Vectors is new Indefinite_Vectors
     (Index_Type   => Positive,
      Element_Type => Condition);

   function Make_Condition
     (Label : String; Variable : String; Op : Condition_Op; Value : String)
     return Condition;

   type Recipe (Command_Length : Natural; Label_Length : Natural) is record
      Destinations : Destination_Vectors.Vector;
      Sources      : Source_Vectors.Vector;
      Conditions   : Condition_Vectors.Vector;
      Cmd          : Command (Command_Length);
      Label        : aliased String (1 .. Label_Length);
   end record;

   function Make_Recipe
     (Command      : String;
      Label        : String;
      Destinations : Destination_Vectors.Vector := Destination_Vectors.Empty_Vector;
      Sources      : Source_Vectors.Vector := Source_Vectors.Empty_Vector;
      Conditions   : Condition_Vectors.Vector := Condition_Vectors.Empty_Vector)
     return Recipe;

   function Condition_To_String (C : Condition) return String;

   function Command_To_String (C : Command) return String;

   procedure Add (Db_File : String; Dependency : in out Recipe; To : Integer);
   procedure Add (Db : Gnade.Database; Dependency : in out Recipe; To : Integer);

   procedure Add_Condition
     (Db_File    : String;
      Dependency : in out Recipe;
      Cond       : aliased in out Condition);
   procedure Add_Condition
     (Db          : Gnade.Database;
      Dependency  : in out Recipe;
      Cond        : aliased in out Condition);
   procedure Add_Condition
     (Db          : Gnade.Database;
      Dependency  : in out Recipe;
      Cond        : in out Condition_Access;
      Stmt_Insert : Gnade.Statement;
      Stmt_Rel    : Gnade.Statement);

   procedure Add_Source
     (Db_File    : String;
      Dependency : in out Recipe;
      Src        : aliased in out Source);
   procedure Add_Source
     (Db          : Gnade.Database;
      Dependency  : in out Recipe;
      Src         : aliased in out Source);
   procedure Add_Source
     (Db          : Gnade.Database;
      Dependency  : in out Recipe;
      Src         : in out Source_Access;
      Stmt_Insert : Gnade.Statement;
      Stmt_Rel    : Gnade.Statement);

   procedure Add_Destination
     (Db_File    : String;
      Dependency : in out Recipe;
      Dst        : aliased in out Destination);
   procedure Add_Destination
     (Db          : Gnade.Database;
      Dependency  : in out Recipe;
      Dst         : aliased in out Destination);
   procedure Add_Destination
     (Db          : Gnade.Database;
      Dependency  : in out Recipe;
      Dst         : in out Destination_Access;
      Stmt_Insert : Gnade.Statement;
      Stmt_Rel    : Gnade.Statement);

   procedure Show (Db_File : String; Dependency : String);
   procedure Show (Db : Gnade.Database; Dependency : String);
   procedure Show (Db : Gnade.Database; Dependency : Recipe);

end Cog.Recipes;
