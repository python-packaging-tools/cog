with Ada.Text_IO;
with Ada.Exceptions;
with Ada.Directories;
with Ada.Strings.Unbounded;
with GNATCOLL.Traces;
with Cog_Cli.Options;
with Cog_Cli.Xml;
with Cog.Variables;

package body Cog_Cli.Variables is

   package Ati renames Ada.Text_IO;
   package Asu renames Ada.Strings.Unbounded;
   package Gt renames GNATCOLL.Traces;
   package Ad renames Ada.Directories;
   package Ae renames Ada.Exceptions;
   package Cco renames Cog_Cli.Options;
   package Ccx renames Cog_Cli.Xml;
   package Cv renames Cog.Variables;

   Value_Type : Cv.Vt := Cv.Str;

   procedure Parse_Set (C : in out Cco.Context) is
      use Cv;
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : aliased Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-variable-set"));
      Vtype : aliased Cco.Value_Handler;
   begin
      Cco.Include (Cl, "--type", "t", Vtype'Unchecked_Access);
      Cco.Parse_Common (C, Cl, Help, "variable set", 2, Continue);

      if Vtype.Set then
         declare
            Vtype_Raw : constant String := Asu.To_String (Vtype.Option_Value);
         begin
            if Vtype_Raw = "int" then
               Value_Type := Cv.Int;
            elsif Vtype_Raw = "str" then
               Value_Type := Cv.Str;
            else
               raise Unsupported_Variable_Type
                 with Vtype_Raw & " is not a supported type" &
                   "supported types are `int` and `str`";
            end if;
         end;
      end if;

      if Continue and Value_Type = Cv.Str then
         declare
            Var_Name : constant String := Cco.Argument (Cl);
            Str_Val : constant String := Cco.Argument (Cl, Cl.Index + 1);
            Val : constant Cv.Variable_Type :=
              (Rt => Cv.Str,
               T => Cv.Str,
               Str_Val => Asu.To_Unbounded_String (Str_Val));
         begin
            Cv.Upsert (Asu.To_String (Cco.Db_File), Var_Name, Val);
         end;
      elsif Continue and Value_Type = Cv.Int then
         declare
            Var_Name : constant String := Cco.Argument (Cl);
            Int_Val : constant Integer :=
              Integer'Value (Cco.Argument (Cl, Cl.Index + 1));
            Val : constant Cv.Variable_Type :=
              (Rt => Cv.Int, T => Cv.Int, Int_Val => Int_Val);
         begin
            Cv.Upsert (Asu.To_String (Cco.Db_File), Var_Name, Val);
         end;
      end if;

   end Parse_Set;

   procedure Parse_List (C : in out Cco.Context) is
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-variable-list"));
   begin
      Cco.Parse_Common (C, Cl, Help, "variable list", 0, 1, Continue);
      if Continue then
         if Cco.Has_More (Cl, 1) then
            Cv.List (Asu.To_String (Cco.Db_File), Cco.Argument (Cl));
         else
            Cv.List (Asu.To_String (Cco.Db_File));
         end if;
      end if;
   end Parse_List;

   procedure Parse_Unset (C : in out Cco.Context) is
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-variable-unset"));
   begin
      Cco.Parse_Common (C, Cl, Help, "variable unset", 1, Continue);
      if Continue then
         declare
            Var_Name : constant String := Cco.Argument (Cl);
         begin
            Cv.Unset (Asu.To_String (Cco.Db_File), Var_Name);
         end;
      end if;
   end Parse_Unset;

   procedure Parse_Get (C : in out Cco.Context) is
      use Cv;
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-variable-get"));
   begin
      Cco.Parse_Common (C, Cl, Help, "variable get", 1, Continue);
      if Continue then
         declare
            Var : constant Cv.Variable_Type :=
              Cv.Get (Asu.To_String (Cco.Db_File), Cco.Argument (Cl));
         begin
            if Var.T = Cv.Str then
               --  TODO: Unsure about quote escaping, same in list
               Ati.Put_Line (Asu.To_String (Var.Str_Val));
            else
               Ati.Put_Line (Integer'Image (Var.Int_Val));
            end if;
         end;
      end if;
   end Parse_Get;

   procedure Parse (C : in out Cco.Context) is
      Var_Context : Cco.Context;
      Help        : aliased Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-variable"));
   begin
      Cco.Include (Var_Context, "--help", "h", Help'Unchecked_Access);
      Var_Context.Index := C.Index;
      Cco.Parse (Var_Context);

      if Cco.Has_More (Var_Context) then
         declare
            Subcommand : constant String := Cco.Argument (Var_Context);
         begin
            Gt.Trace (Log, "Parsing subcommand: " & Subcommand);
            Var_Context.Index := Var_Context.Index + 1;
            if Subcommand = "set" then
               Gt.Trace (Log, "Set subcommand");
               Parse_Set (Var_Context);
            elsif Subcommand = "list" then
               Gt.Trace (Log, "List subcommand");
               Parse_List (Var_Context);
            elsif Subcommand = "unset" then
               Gt.Trace (Log, "unset subcommand");
               Parse_Unset (Var_Context);
            elsif Subcommand = "get" then
               Gt.Trace (Log, "get subcommand");
               Parse_Get (Var_Context);
            else
               raise Cco.Unknown_Subcommand
                 with Subcommand & " is not a known subcommand";
            end if;
         end;
      end if;

   end Parse;

end Cog_Cli.Variables;
