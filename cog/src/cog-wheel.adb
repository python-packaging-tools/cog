with Ada.Text_IO;
with Ada.Characters.Handling;
with Ada.Strings.Unbounded;
with Ada.Exceptions;
with Ada.Directories;
with System;
with Interfaces.C;
with GNAT.Regpat;
with GNATCOLL.SQL;
with GNATCOLL.Sql_Impl;
with GNATCOLL.Traces;
with GNATCOLL.Strings;
with Cog.Fs;
with Cog.Database;
with Cog.Variables;

package body Cog.Wheel is

   package Ati renames Ada.Text_IO;
   package Ae renames Ada.Exceptions;
   package Ad renames Ada.Directories;
   package Ach renames Ada.Characters.Handling;
   package Gp renames GNAT.Regpat;
   package Gq renames GNATCOLL.SQL;
   package Gsi renames GNATCOLL.Sql_Impl;
   package Gt renames GNATCOLL.Traces;
   package Gs renames GNATCOLL.Strings;
   package Cd renames Cog.Database;
   package Cv renames Cog.Variables;

   Malformed_Wheel_Name : exception;

   Log : constant Gt.Trace_Handle := Gt.Create ("cog.wheel", Stream => "&2");

   function Make_Wheel_Tag
     (Platform     : String  := "any";
      Python       : String  := "cp";
      Abi          : String  := "none";
      Debugging    : Boolean := False;
      Pymalloc     : Boolean := False;
      Wide_Unicode : Boolean := False) return Cwt.Wheel_Tag is
   begin
      return
        (Platform_Length => Platform'Length,
         Platform        => Platform,
         Python_Length   => Python'Length,
         Python          => Python,
         Abi_Length      => Abi'Length,
         Abi             => Abi,
         Debugging       => Debugging,
         Pymalloc        => Pymalloc,
         Wide_Unicode    => Wide_Unicode);
   end;
   
   function Make_Wheel
     (Distribution    : String;
      Version         : String  := "1.0.0";
      Build           : String  := "";
      Root_Is_Purelib : Boolean := False;
      Id              : Integer := -1;
      Tags            : Cwt.Tags_Vectors.Vector := Cwt.Tags_Vectors.Empty_Vector)
     return Cwt.Wheel is
   begin
      return 
        (Distribution_Length => Distribution'Length,
         Distribution        => Distribution,
         Version_Length      => Version'Length,
         Version             => Version,
         Build_Length        => Build'Length,
         Build               => Build,
         Root_Is_Purelib     => Root_Is_Purelib,
         Id                  => Id,
         Tags                => Tags);
   end;

   --  TODO: this needs to be able to handle partial spec that
   --  includes build number.
   function Normalize_Wheel_Name (Arch_Pattern : String) return String is
      use Asu;
      Tmp : Asu.Unbounded_String := Asu.To_Unbounded_String (Arch_Pattern);
      Dummy : constant String := "-$version-$python-$abi-$platform";
      Nshys : Integer := 0;
   begin
      for C of Arch_Pattern loop
         if C = '-' then
            Nshys := Nshys + 1;
         end if;
      end loop;
      case Nshys is
         when 0 => Tmp := Tmp & Asu.To_Unbounded_String (Dummy);
         when 1 => Tmp := Tmp & Asu.To_Unbounded_String (Dummy (9 .. Dummy'Last));
         when 2 => Tmp := Tmp & Asu.To_Unbounded_String (Dummy (17 .. Dummy'Last));
         when 3 => Tmp := Tmp & Asu.To_Unbounded_String (Dummy (22 .. Dummy'Last));
         when 4 => null;
         when 5 => null;
         when others =>
            raise Malformed_Wheel_Name with Arch_Pattern & " has too many hyphens";
      end case;

      return Asu.To_String (Tmp);
   end Normalize_Wheel_Name;

   function Parse_Wheel (Arch_Pattern : String) return Cwt.Wheel is
      use Gp;
      Expression : constant String :=
           "([a-z0-9_]+)-([$]?[a-z0-9.]+)(-[$][a-z0-9]+|[0-9][a-z0-9]*)?-" &
           "([$]?[^-]+)-([$]?[^-]+)-([$]?[^-]+)([.]whl)?";
      Re      : constant Gp.Pattern_Matcher := Gp.Compile (Expression);
      Matches : Gp.Match_Array (0 .. 7);
      Arch_Normalized : constant String := Normalize_Wheel_Name (Arch_Pattern);
      Tags : Cwt.Tags_Vectors.Vector;
   begin
      Gp.Match (Re, Arch_Normalized, Matches);
      if Matches (0) = Gp.No_Match then
         raise Malformed_Wheel_Name with "Cannot parse: " & Arch_Pattern &
           " (" & Arch_Normalized & ")";
      end if;
      Gt.Trace (Log, "Parse_Wheel: pattern: " & Arch_Pattern);
      Gt.Trace (Log, "Parse_Wheel: normalized: " & Arch_Normalized);
      --  TODO: This needs to actually do what it claims to do
      Tags.Append (Make_Wheel_Tag);
      return Make_Wheel
        (Arch_Normalized (Matches (1).First .. Matches (1).Last),
        Tags => Tags);
   end Parse_Wheel;

   function Make_Wheel_Fetcher
     (N_Parameters : Natural;
      Db           : Gnade.Database;
      Condition    : Asu.Unbounded_String;
      Variables    : Gse.Sql_Parameters) return Wheel_Fetcher is
   begin
      return
        (N_Parameters => 1,
         Db => Db,
         Condition => Condition,
         Variables => Variables);
   end Make_Wheel_Fetcher;

   procedure Bind_Wheel (Stmt : Gnade.Statement; Arch : aliased in out Cwt.Wheel) is
   begin
      Gnade.Bind_Text (Stmt, 1, Arch.Distribution'Address, Arch.Distribution'Length);
      Gt.Trace (Log, "Binding distribution: " & Arch.Distribution);
      Gnade.Bind_Text (Stmt, 2, Arch.Version'Address, Arch.Version'Length);
      Gt.Trace (Log, "Binding version: " & Arch.Version);
      Gnade.Bind_Text (Stmt, 3, Arch.Build'Address, Arch.Build'Length);
      Gt.Trace (Log, "Binding build: " & Arch.build);
   end Bind_Wheel;

   procedure Add_Tag (Db : Gnade.Database; Tag : Cwt.Wheel_Tag; Selected_Wheel : Long_Long_Integer) is
      Raw       : constant String         := "insert into wheel_tags" &
        "(platform, python, abi, debugging, pymalloc, wheel)" &
        "values (?, ?, ?, ?, ?, ?)";
      Stmt      : Gnade.Statement;
      Platform  : aliased constant String := Tag.Platform;
      Python    : aliased constant String := Tag.Python;
      Abi       : aliased constant String := Tag.Abi;
      Debugging : constant Integer        := (if Tag.Debugging then 1 else 0);
      Pymalloc  : constant Integer        := (if Tag.Pymalloc then 1 else 0);
   begin
      Cd.Prepare (Db, Raw, Stmt);
      Gnade.Bind_Text (Stmt, 1, Platform'Address, Platform'Length);
      Gnade.Bind_Text (Stmt, 2, Python'Address, Python'Length);
      Gnade.Bind_Text (Stmt, 3, Abi'Address, Abi'Length);
      Gnade.Bind_Int (Stmt, 4, Interfaces.C.int (Debugging));
      Gnade.Bind_Int (Stmt, 5, Interfaces.C.int (Pymalloc));
      Gnade.Bind_Int (Stmt, 6, Interfaces.C.int (Selected_Wheel));
      Cd.Step (Db, Stmt);
   end Add_Tag;

   function Where_Tags (Arch : Cwt.Wheel) return String is
      use Gs;
      Template : Gs.XString;
      Question : Gs.XString;
      Result : Gs.XString;
      Questions : Gs.XString;
   begin
      Question.Set ("?, ");
      Template.Append ("(");
      Questions := Integer (Arch.Tags.Length) * Question;
      Template.Append (Questions);
      Ati.Put_Line ("template_lenght: " & Integer'Image (Template.Length));
      Ati.Put_Line ("tags_lenght: " & Integer'Image (Integer (Arch.Tags.Length)));
      Template (Template.Length - 1) := ')';
      Result.Append ("wheel_tags.platform in ");
      Result.Append (Template);
      Result.Append ("and wheel_tags.python in ");
      Result.Append (Template);
      Result.Append ("and wheel_tags.abi in ");
      Result.Append (Template);
      Result.Append ("and wheel_tags.debugging in ");
      Result.Append (Template);
      Result.Append ("and wheel_tags.pymalloc in ");
      Result.Append (Template);
      return Gs.To_String (Result);
   end Where_Tags;

   procedure Bind_One_Tag
     (Stmt : Gnade.Statement; Tag : in out Cwt.Wheel_Tag_Access; Offset : Integer) is
   begin
      Gt.Trace (Log, "Binding platform: " & Tag.Platform &
                  " at " & Integer'Image (1 + Offset));
      Gnade.Bind_Text (Stmt, 1 + Offset, Tag.Platform'Address, Tag.Platform'Length);
      Gt.Trace (Log, "Binding python: " & Tag.Python &
                  " at " & Integer'Image (2 + Offset));
      Gnade.Bind_Text (Stmt, 2 + Offset, Tag.Python'Address, Tag.Python'Length);
      Gt.Trace (Log, "Binding abi: " & Tag.Abi &
                  " at " & Integer'Image (3 + Offset));
      Gnade.Bind_Text (Stmt, 3 + Offset, Tag.Abi'Address, Tag.Abi'Length);
      Gt.Trace (Log, "Binding debugging: " & Boolean'Image (Tag.Debugging) &
                  " at " & Integer'Image (4 + Offset));
      Gnade.Bind_Int (Stmt, 4 + Offset, Interfaces.C.int (if Tag.Debugging then 1 else 0));
      Gt.Trace (Log, "Binding pymalloc: " & Boolean'Image (Tag.Pymalloc) &
                  " at " & Integer'Image (5 + Offset));
      Gnade.Bind_Int (Stmt, 5 + Offset, Interfaces.C.int (if Tag.Pymalloc then 1 else 0));
   end Bind_One_Tag;

   procedure Bind_Tags (Stmt : Gnade.Statement; Arch : aliased in out Cwt.Wheel; Bound : Integer) is
      Tag_Spacing : Integer := Integer (Arch.Tags.Length);
      Tag_Index : Integer := 0;
      C : Cwt.Tags_Vectors.Cursor := Arch.Tags.First;
      Tag_Var : Cwt.Wheel_Tag_Access;
   begin
      while Cwt.Tags_Vectors.Has_Element (C) loop
         Tag_Var := Arch.Tags.Reference (C).Element;
         Bind_One_Tag (Stmt, Tag_Var, Tag_Index * Tag_Spacing + Bound);
         Cwt.Tags_Vectors.Next (C);
         Tag_Index := Tag_Index + 1;
      end loop;
   end Bind_Tags;

   function Count_Tags (Raw : Asu.Unbounded_String) return Positive is
      use Gp;
      Raw_Str : constant String := Asu.To_String (Raw);
      Result : Positive := 1;
      Component : Positive := 1;
      Expression : constant String := "([^-]+)-([^-]+)-([^-]+)";
      Re      : constant Gp.Pattern_Matcher := Gp.Compile (Expression);
      Matches : Gp.Match_Array (0 .. 3);
   begin
      Gp.Match (Re, Raw_Str, Matches);
      if Matches (0) = Gp.No_Match then
         raise Malformed_Wheel_Name
           with "Cannot parse tag: " & Raw_Str;
      end if;
      for Match of Matches loop
         Component := 1;
         for C of Raw_Str (Match.First .. Match.Last) loop
            if C = '.' then
               Component := Component + 1;
            end if;
         end loop;
         if Result = 1 then
            Result := Component;
         else
            Result := Component * Result;
         end if;
      end loop;
      return Result;
   end Count_Tags;

   procedure Populate_Tags
     (W         : in out Cwt.Wheel;
      Abi       : Asu.Unbounded_String;
      Debugging : Boolean;
      Pymalloc  : Boolean;
      Platform  : Asu.Unbounded_String;
      Python    : Asu.Unbounded_String) is
      use Asu;
      Abi_Str      : constant String := Asu.To_String (Abi);
      Platform_Str : constant String := Asu.To_String (Platform);
      Python_Str   : constant String := Asu.To_String (Python);
      Abi_X        : Gs.XString;
      Platform_X   : Gs.XString;
      Python_X     : Gs.XString;
   begin
      --  TODO: consider using xstring in other places to avoid using
      --  unbounded string too much.
      Abi_X.Set (Abi_Str);
      Platform_X.Set (Platform_Str);
      Python_X.Set (Python_Str);

      for A of Abi_X.Split ('.') loop
         for Pm of Platform_X.Split ('.') loop
            for Pn of Python_X.Split ('.') loop
               W.Tags.Append
                 (Make_Wheel_Tag
                    (Abi => Gs.To_String (A),
                     Platform => Gs.To_String (Pm),
                     Python => Gs.To_String (Pn),
                     Debugging => Debugging,
                     Pymalloc => Pymalloc));
            end loop;
         end loop;
      end loop;
   end Populate_Tags;

   procedure Add (Db_File : String; Arch : Cwt.Wheel; Wheel_Path : String) is
      use Ae;
      Exception_Caught : Ae.Exception_Occurrence;
      Db               : Gnade.Database;
   begin
      declare
         use Gnade;
         Root         : aliased constant String :=
           Cog.Fs.Relpath (Wheel_Path, Ad.Containing_Directory (Db_File));
         Raw          : constant String         :=
           "insert into wheels" &
           "(distribution, version, build, root)" &
           " values (?, ?, ?, ?)";
         Stmt         : Gnade.Statement;
         Inserted : Long_Long_Integer := 0;
         Arch_Var : aliased Cwt.Wheel := Arch;
      begin
         Cd.Open (Db, Db_File);
         Cd.Prepare (Db, Raw, Stmt);
         Bind_Wheel (Stmt, Arch_Var);
         Gnade.Bind_Text (Stmt, 4, Root'Address, Root'Length);
         Cd.Step (Db, Stmt);
         Inserted := Gnade.Last_Insert_Rowid (Db);
         --  TODO: this shouldn't create more variables downstream.
         for Tag of Arch.Tags loop
            Add_Tag (Db, Tag, Inserted);
         end loop;
         Choose (Db, Arch);
      exception
         when E : others =>
            Ae.Save_Occurrence (Exception_Caught, E);
      end;
      Gnade.Close (Db);
      Ae.Reraise_Occurrence (Exception_Caught);
   end Add;

   procedure Choose (Db_File : String; Arch_Pattern : String) is
      use Gnade;
      Arch             : Cwt.Wheel := Parse_Wheel (Arch_Pattern);
      Db               : Gnade.Database;
      Exception_Caught : Ae.Exception_Occurrence;
   begin
      Cd.Open (Db, Db_File);
      declare
      begin
         Choose (Db, Arch);
      exception
         when E : others =>
            Ae.Save_Occurrence (Exception_Caught, E);
      end;
      Gnade.Close (Db);
      Ae.Reraise_Occurrence (Exception_Caught);
   end Choose;

   procedure Choose (Db : Gnade.Database; Arch : Cwt.Wheel) is
      use Gnade;
      Raw  : constant String := "select distinct wheels.rowid from wheels " &
        "join wheel_tags on wheels.rowid = wheel_tags.wheel " &
        "where " &
        "wheels.distribution = ? and " &
        "wheels.version = ? and " &
        "wheels.build = ? and " & Where_Tags (Arch);
      Stmt : Gnade.Statement;
      Rowid : Integer := -1;
      Arch_Var : aliased Cwt.Wheel := Arch;
   begin
      Cd.Prepare (Db, Raw, Stmt);
      Bind_Wheel (Stmt, Arch_Var);
      Bind_Tags (Stmt, Arch_Var, 3);

      while Cd.Fetch (Db, Stmt) loop
         if Rowid /= -1 then
            raise Cwt.Multiple_Wheel_Match
              with "Multiple wheels match " & Cwt.Wheel_File_Name (Arch);
         end if;
         Rowid := Gnade.Column_Int (Stmt, 0);
      end loop;

      Gnade.Finalize (Stmt);

      if Rowid = -1 then
         raise Cwt.No_Matching_Wheel
           with "No wheel matches " & Cwt.Wheel_File_Name (Arch);
      end if;

      declare
         Rowid_Val : Cv.Variable_Type :=
           (Rt => Cv.Int, T => Cv.Int, Int_Val => Rowid);
      begin
         Cv.Upsert (Db, "selected_wheel", Rowid_Val);
         Selected_Wheel_Pattern :=
           Asu.To_Unbounded_String (Cwt.Wheel_File_Name (Arch));
         Selected_Wheel_Rowid := Rowid;
      end;
   end Choose;

   function Chosen (Db_File : String) return Integer is
      use Ae;
      Exception_Caught : Ae.Exception_Occurrence;
      Db               : Gnade.Database;
      Result : Integer;
   begin
      declare
      begin
         Cd.Open (Db, Db_File);
         Result := Chosen (Db);
      exception
         when E : others =>
            Ae.Save_Occurrence (Exception_Caught, E);
      end;
      Gnade.Close (Db);
      Ae.Reraise_Occurrence (Exception_Caught);
      return Result;
   end Chosen;

   function Chosen (Db : Gnade.Database) return Integer is
      Selected : Cv.Variable_Type := Cv.Get (Db, "selected_wheel");
      use Cv;
   begin
      if Selected.Rt = Cv.Str then
         raise Cd.Database_Corrupted with "selected_wheel must be an integer";
      end if;
      return Selected.Int_Val;
   end Chosen;

   procedure List (Db_File : String; Regexp : String := ".") is
      use Ae;
      Exception_Caught : Ae.Exception_Occurrence;
      Db               : Gnade.Database;
   begin
      declare
      begin
         Cd.Open (Db, Db_File);
         List (Db, Regexp);
      exception
         when E : others =>
            Ae.Save_Occurrence (Exception_Caught, E);
      end;
      Gnade.Close (Db);
      Ae.Reraise_Occurrence (Exception_Caught);
   end List;

   function Has_Element (Pos : Wheel_Fetcher_Cursor) return Boolean is
   begin
      return Pos.Has_More;
   end Has_Element;

   function First (Self : Wheel_Fetch_Iterator) return Wheel_Fetcher_Cursor is
      Has_More : Boolean := Cd.Fetch (Self.Db, Self.Stmt);
   begin
      return C : Wheel_Fetcher_Cursor do
         C.Has_More := Has_More;
         C.Db := Self.Db;
         C.Stmt := Self.Stmt;
      end return;
   end First;

   function Next (Self : Wheel_Fetch_Iterator; Pos : Wheel_Fetcher_Cursor)
                 return Wheel_Fetcher_Cursor is
      pragma Unreferenced (Self);
      Has_More : Boolean := Cd.Fetch (Pos.Db, Pos.Stmt);
   begin
      return C : Wheel_Fetcher_Cursor do
         C.Has_More := Has_More;
         C.Db := Pos.Db;
         C.Stmt := Pos.Stmt;
      end return;
   end Next;

   function Fetch_Wheel (Fetcher : Wheel_Fetcher)
                        return Wheel_Fetch_Iterators.Forward_Iterator'Class is
      use Gnade;
      Raw  : constant String :=
        "select wheels.rowid, wheels.distribution, wheels.version, wheels.build, " &
        "wheel_tags.platform, wheel_tags.python, wheel_tags.abi," &
        "wheel_tags.pymalloc, wheel_tags.debugging, wheel_tags.wide_unicode " &
        "from wheels " &
        "join wheel_tags on wheels.rowid = wheel_tags.wheel " &
        Asu.To_String (Fetcher.Condition);
      Stmt : Gnade.Statement;
   begin
      --  TODO: This needs to be aggregated by wheels.rowid and
      --  multiple tags jointed together, but it's ok as it is for
      --  now.
      Cd.Prepare (Fetcher.Db, Raw, Stmt);
      for P in Fetcher.Variables'Range loop
         if Fetcher.Variables (P).Get in Gsi.SQL_Parameter_Text'Class then
            declare
               P2 : constant access Gsi.SQL_Parameter_Text :=
                 Gsi.SQL_Parameter_Text (Fetcher.Variables (P).Get.Element.all)'Access;
               Str_Adr : System.Address;
               Str_Len : Natural;
            begin
               -- I think this is a support for unbounded strings...
               -- if P2.Str_Ptr = null then
               --    Aux.Get_String (P2.Str_Val, Str_Ptr, Str_Len);
               -- else
               --    Str_Adr := P2.Str_Ptr.all'Address;
               --    Str_Len := P2.Str_Ptr'Length;
               -- end if;
               Str_Adr := P2.Str_Ptr.all'Address;
               Str_Len := P2.Str_Ptr'Length;

               if P2.Make_Copy then
                  Gnade.Bind_Text (Stmt, P, Str_Adr, Str_Len, Gnade.Transient);
               else
                  Gnade.Bind_Text (Stmt, P, Str_Adr, Str_Len);
               end if;
            end;
         elsif Fetcher.Variables (P).Get in Gq.SQL_Parameter_Integer'Class then
            declare
               P2 : constant access Gq.SQL_Parameter_Integer :=
                 Gq.SQL_Parameter_Integer (Fetcher.Variables (P).Get.Element.all)'Access;
            begin
               Gnade.Bind_Int (Stmt, P, Interfaces.C.int (P2.Val));
            end;
         end if;
      end loop;
      return I : Wheel_Fetch_Iterator do
         I.Db := Fetcher.Db;
         I.Stmt := Stmt;
      end return;
   end Fetch_Wheel;

   function Element (Pos : Wheel_Fetcher_Cursor) return Cwt.Wheel is
      use Cwt.Tags_Vectors;
      Rowid        : constant Integer        := Gnade.Column_Int (Pos.Stmt, 0);
      Distribution : constant String         := Gnade.Column_Text (Pos.Stmt, 1);
      Version      : constant String         := Gnade.Column_Text (Pos.Stmt, 2);
      Build        : constant String         := Gnade.Column_Text (Pos.Stmt, 3);
      Platform     : constant String         := Gnade.Column_Text (Pos.Stmt, 4);
      Python       : constant String         := Gnade.Column_Text (Pos.Stmt, 5);
      Abi          : constant String         := Gnade.Column_Text (Pos.Stmt, 6);
      Pymalloc     : constant Boolean        :=
        (if Gnade.Column_int (Pos.Stmt, 7) = 0 then False else True);
      Debugging    : constant Boolean        :=
        (if Gnade.Column_int (Pos.Stmt, 8) = 0 then False else True);
      Wide_Unicode : constant Boolean        :=
        (if Gnade.Column_int (Pos.Stmt, 9) = 0 then False else True);
      Tags         : Cwt.Tags_Vectors.Vector := Cwt.Tags_Vectors.Empty_Vector &
        Make_Wheel_Tag (Platform, Python, Abi, Debugging, Pymalloc, Wide_Unicode);
   begin
      return Make_Wheel (Distribution, Version, Build, Id => Rowid, Tags => Tags);
   end Element;

   procedure List (Db : Gnade.Database; Regexp : String := ".") is
      use Gnade;
      use Gse;
      Regex_Var : aliased constant String := Regexp;
      Fetcher : Wheel_Fetcher :=
        (N_Parameters => 1,
         Db => Db,
         Condition => Asu.To_Unbounded_String ("where wheels.distribution regexp ?"),
         Variables => (1 => +(Regex_Var'Access)));
   begin
      for C in Fetch_Wheel (Fetcher) loop
         Ati.Put_Line (Cwt.Wheel_File_Name (Element (C)));
      end loop;
   end List;

   procedure Show (Db_File : String; Rowid : Integer) is
      use Ae;
      Exception_Caught : Ae.Exception_Occurrence;
      Db               : Gnade.Database;
   begin
      declare
      begin
         Cd.Open (Db, Db_File);
         Show (Db, Rowid);
      exception
         when E : others =>
            Ae.Save_Occurrence (Exception_Caught, E);
      end;
      Gnade.Close (Db);
      Ae.Reraise_Occurrence (Exception_Caught);
   end Show;

   procedure Show (Db : Gnade.Database; Rowid : Integer) is
   begin
      Show (Db, Wheel_By_Id (Db, Rowid));
   end Show;

   procedure Show (Db_File : String; Arch : Cwt.Wheel) is
      use Ae;
      Exception_Caught : Ae.Exception_Occurrence;
      Db               : Gnade.Database;
   begin
      declare
      begin
         Cd.Open (Db, Db_File);
         Show (Db, Arch);
      exception
         when E : others =>
            Ae.Save_Occurrence (Exception_Caught, E);
      end;
      Gnade.Close (Db);
      Ae.Reraise_Occurrence (Exception_Caught);
   end Show;

   procedure Show (Db : Gnade.Database; Arch : Cwt.Wheel) is
   begin
      null;
   end Show;

   function Wheel_By_Id (Db : Gnade.Database; Id : Integer) return Cwt.Wheel is
      use Gse;
      Fetcher : Wheel_Fetcher := Make_Wheel_Fetcher
        (N_Parameters => 1,
         Db => Db,
         Condition => Asu.To_Unbounded_String ("where wheels.rowid = ?"),
         Variables => (1 => +(Id)));
   begin
      for C in Fetch_Wheel (Fetcher) loop
         return Element (C);
      end loop;
      raise Cwt.No_Matching_Wheel
        with "Cannot find wheel with id: " & Integer'Image (Id);
   end Wheel_By_Id;

   procedure Build
     (Db_File : String; Arch : Integer; Output : String; Vars : Cwb.Vars_Maps.Map) is
      use Ae;
      Exception_Caught : Ae.Exception_Occurrence;
      Db               : Gnade.Database;
   begin
      declare
      begin
         Cd.Open (Db, Db_File);
         Cog.Wheel_Build.Build (Db, Wheel_By_Id (Db, Arch), Output, Vars);
      exception
         when E : others =>
            Ae.Save_Occurrence (Exception_Caught, E);
      end;
      Gnade.Close (Db);
      Ae.Reraise_Occurrence (Exception_Caught);
   end Build;

end Cog.Wheel;
