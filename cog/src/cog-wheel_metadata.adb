with Ada.Exceptions;
with Ada.Text_IO;
with Ada.Containers.Indefinite_Hashed_Maps;
with Ada.Strings.Hash;
with Interfaces.C;
with GNATCOLL.Traces;
with Cog.Database;

package body Cog.Wheel_Metadata is

   package Ati renames Ada.Text_IO;
   package Ae renames Ada.Exceptions;
   package Gt renames GNATCOLL.Traces;
   package Cd renames Cog.Database;

   use Gs;

   Log : constant Gt.Trace_Handle :=
     Gt.Create ("cog.wheel_metadata", Stream => "&2");

   Missing_Metadata : exception;

   Unrecognized_Metadata_Version : exception;

   Unrecognized_Metadata_Key : exception;

   Obsolete_Metadata_Key : exception;

   Incompatible_Metadata_Version : exception;

   function Make_Semver
     (Epoch          : Natural;
      Release        : Semver_Release;
      Prerelease_Tag : Prerelease_Kind := None;
      Prerelease_N   : Natural := 0;
      Post_Release   : Natural := 0;
      Dev_Release    : Natural := 0) return Semver is
   begin
      return
        (Epoch => Epoch,
         N_Releases => Release'Length,
         Release => Release,
         Prerelease_Tag => Prerelease_Tag,
         Prerelease_N => Prerelease_N,
         Post_Release => Post_Release,
         Dev_Release => dev_release);
   end Make_Semver;

   function Make_Wheel_Metadata
     (Summary          : String := "";
      Description      : String := "";
      Homepage         : Url := No_Url;
      License          : String := "";
      Download_Url     : Url := No_Url;
      Dct              : Description_Content_Type := (others => <>);
      Mv               : Metadata_Version := Mv_2_3) return Wheel_Metadata is
   begin
      return
        (Summary => Summary,
         Summary_Len => Summary'Length,
         Description => Description,
         Description_Len => Description'Length,
         Homepage_Len => Homepage.U'Length,
         Homepage => Homepage,
         License_Len => License'Length,
         License => License,
         Download_Url_Len => Download_Url.U'Length,
         Download_Url => Download_Url,
         Dct => Dct,
         Mv => Mv,
         others => <>);
   end;

   function Next_Mv (Mv : Metadata_Version) return Metadata_Version is
   begin
      case Mv is
         when Mv_1_0 => return Mv_1_1;
         when Mv_1_1 => return Mv_1_2;
         when Mv_1_2 => return Mv_2_1;
         when Mv_2_1 => return Mv_2_2;
         when Mv_2_2 => return Mv_2_3;
         when Mv_2_3 => raise Program_Error with "2.3 is the newest version";
      end case;
   end Next_Mv;

   function Previous_Mv (Mv : Metadata_Version) return Metadata_Version is
   begin
      case Mv is
         when Mv_1_0 => raise Program_Error with "1.0 is the oldest version";
         when Mv_1_1 => return Mv_1_0;
         when Mv_1_2 => return Mv_1_1;
         when Mv_2_1 => return Mv_1_2;
         when Mv_2_2 => return Mv_2_1;
         when Mv_2_3 => return Mv_2_2;
      end case;
   end Previous_Mv;

   function Mv_To_String (Mv : Metadata_Version) return String is
   begin
      case Mv is
         when Mv_1_0 => return "1.0";
         when Mv_1_1 => return "1.1";
         when Mv_1_2 => return "1.2";
         when Mv_2_1 => return "2.1";
         when Mv_2_2 => return "2.2";
         when Mv_2_3 => return "2.3";
      end case;
   end Mv_To_String;

   function Mv_To_Table (Mv : Metadata_Version) return String is
   begin
      case Mv is
         when Mv_1_0 => return "wheel_metadata_v1_0";
         when Mv_1_1 => return "wheel_metadata_v1_1";
         when Mv_1_2 => return "wheel_metadata_v1_2";
         when Mv_2_1 => return "wheel_metadata_v2_1";
         when Mv_2_2 => return "wheel_metadata_v2_2";
         when Mv_2_3 => return "wheel_metadata_v2_3";
      end case;
   end Mv_To_Table;

   function Key_To_Field (Key : String) return String is
      Result : Gs.XString;
   begin
      Result.Set (Key);
      Result.To_Lower;
      Result := Gs.Join ('_', Result.Split ('-'));
      return Result.To_String;
   end Key_To_Field;

   package Key_Vectors is new Indefinite_Vectors
     (Index_Type   => Positive,
      Element_Type => Gs.XString);

   function Single_Keys return Key_Vectors.Vector is
      use Key_Vectors;
      Result : Key_Vectors.Vector;
      Elt : Gs.XString;
   begin
      Elt.Set ("Metadata-Version");
      Result.Append (Elt);
      Elt.Set ("Summary");
      Result.Append (Elt);
      Elt.Set ("Description");
      Result.Append (Elt);
      Elt.Set ("License");
      Result.Append (Elt);
      Elt.Set ("Download-URL");
      Result.Append (Elt);
      Elt.Set ("Requires-Python");
      Result.Append (Elt);
      Elt.Set ("Description-Content-Type");
      Result.Append (Elt);
      return Result;
   end Single_Keys;

   function Multiple_Keys return Key_Vectors.Vector is
      use Key_Vectors;
      Result : Key_Vectors.Vector;
      Elt : Gs.XString;
   begin
      Elt.Set ("Provides-Dist");
      Result.Append (Elt);
      Elt.Set ("Obsoletes-Dist");
      Result.Append (Elt);
      Elt.Set ("Classifier");
      Result.Append (Elt);
      Elt.Set ("Platform");
      Result.Append (Elt);
      Elt.Set ("Author");
      Result.Append (Elt);
      Elt.Set ("Maintainer");
      Result.Append (Elt);
      Elt.Set ("Supported-Platform");
      Result.Append (Elt);
      Elt.Set ("Keywords");
      Result.Append (Elt);
      Elt.Set ("Author-Email");
      Result.Append (Elt);
      Elt.Set ("Maintainer-Email");
      Result.Append (Elt);
      Elt.Set ("Requires-Dist");
      Result.Append (Elt);
      Elt.Set ("Requires-External");
      Result.Append (Elt);
      Elt.Set ("Provides-Extra");
      Result.Append (Elt);
      Elt.Set ("Project-URL");
      Result.Append (Elt);
      Elt.Set ("Dynamic");
      Result.Append (Elt);
      return Result;
   end Multiple_Keys;

   function Key_Is_Multiple (Key : String) return Boolean is
   begin
      for Mkey of Multiple_Keys loop
         if Mkey.To_String = Key then
            return True;
         end if;
      end loop;
      return False;
   end Key_Is_Multiple;

   function Key_To_Table (Key : String) return String is
      Result : Gs.XString;
   begin
      Result.Set ("meta_");
      Result.Append (Key_To_Field (Key));
      if Key (Key'Last) /= 's' then
         Result.Append ('s');
      end if;
      return Result.To_String;
   end Key_To_Table;

   function Effective_Key_Version (Key : String; Mv : Metadata_Version)
                                  return Metadata_Version is
      Min_Mv : Metadata_Version;
   begin
      --  Metadata version 2.3 didn't add any new fields, it modified how
      --  old field are formatted.
      if Key = "Requires" or Key = "Provides" or Key = "Obsoletes" then
         raise Obsolete_Metadata_Key with Key & " is not supported.  " &
           "It was obsoleted some time ago.  Eventually, I'll add support.";
      elsif Key = "Metadata-Version" or Key = "Summary" or Key = "Description" or
        Key = "Home-Page" or Key = "Author" or Key = "Author-Email" or
        Key = "License" or Key = "Platform" or Key = "Keywords" then
         Min_Mv := Mv_1_0;
      elsif Key = "Supported-Platform" or Key = "Download-URL" or
        Key = "Classifier" then
         Min_Mv := Mv_1_1;
      elsif Key = "Maintainer" or Key = "Maintainer-Email" or
        Key = "Requires-Dist" or Key = "Requires-Python" or
        Key = "Requires-External" or Key = "Project-URL" or
        Key = "Provides-Dist" or Key = "Obsoletes-Dist" then
         Min_Mv := Mv_1_2;
      elsif Key = "Description-Content-Type" or Key = "Provides-Extra" then
         Min_Mv := Mv_2_1;
      elsif Key = "Dynamic" then
         Min_Mv := Mv_2_2;
      else
         raise Unrecognized_Metadata_Key
           with Key & " is not a valid metadata field";
      end if;
      if Min_Mv > Mv then
         raise Incompatible_Metadata_Version
           with Key & " requires at least " & Mv_To_String (Min_Mv) &
           " but the version is set to " & Mv_To_String (Mv);
      end if;
      return Min_Mv;
   end Effective_Key_Version;

   package Table_Aliases_Maps is new
     Ada.Containers.Indefinite_Hashed_Maps
     (Key_Type        => String,
      Element_Type    => String,
      Hash            => Ada.Strings.Hash,
      Equivalent_Keys => "=");

   function Keys_To_Field_List (Aliases : Table_Aliases_Maps.Map)
                               return String is
      Result : Gs.XString;
   begin
      for Key of Single_Keys loop
         declare
            Skey : constant String := Key.To_String;
            Kv : Metadata_Version := Effective_Key_Version (Skey, Mv_2_3);
            Ftn : constant String := Mv_To_Table (Kv);
            Tn : constant String :=
              (if Aliases.Contains (Ftn) then Aliases (Ftn) else Ftn);
         begin
            Result.Append (", ");
            Result.Append (Tn);
            Result.Append (".");
            Result.Append (Key_To_Field (Skey));
         end;
      end loop;
      return Result.Tail (Result.Length - 2).To_String;
   end;

   function Mv_From_String (Mv : String) return Metadata_Version is
   begin
      if Mv = "1.0" then
         return Mv_1_0;
      elsif Mv = "1.1" then
         return Mv_1_1;
      elsif Mv = "1.2" then
         return Mv_1_2;
      elsif Mv = "2.1" then
         return Mv_2_1;
      elsif Mv = "2.2" then
         return Mv_2_2;
      elsif Mv = "2.3" then
         return Mv_2_3;
      end if;
      raise Unrecognized_Metadata_Version
        with Mv & " isn't a recognized metadata version";
   end Mv_From_String;

   function Make_Url (U : String) return Url is
   begin
      return (U_Length => U'Length, U => U);
   end Make_Url;

   function Fetch_Wheel_Metadata (Db: Gnade.Database; Arch : Cwt.Wheel)
                                 return Wheel_Metadata is
      Wheel_Rowid_Raw : constant String :=
        "select rowid, metadata from wheels where " &
        "distribution = ? and " &
        "version = ? and " &
        "build = ? and " &
        "root_is_purelib = ?";
      Meta_Rowid_Raw : constant String := "select metadata from wheels where " &
        "rowid = ?";
      Meta_Raw : constant String :=
        "select metadata_version, summary, description, home_page, license " &
        "from wheel_metadata_v1_0 where rowid = ?";
      Stmt : Gnade.Statement;
      Wheel_Rowid : Integer := Arch.Id;
      Meta_Id : Integer := -1;
   begin
      if Wheel_Rowid < 0 then
         Cd.Prepare (Db, Wheel_Rowid_Raw, Stmt);
         Gnade.Bind_Text (Stmt, 1, Arch.Distribution'Address, Arch.Distribution'Length);
         Gnade.Bind_Text (Stmt, 2, Arch.Version'Address, Arch.Version'Length);
         Gnade.Bind_Text (Stmt, 3, Arch.Build'Address, Arch.Build'Length);
         Gnade.Bind_int (Stmt, 4, Interfaces.C.int (if Arch.Root_Is_Purelib then 1 else 0));

         while Cd.Fetch (Db, Stmt) loop
            Wheel_Rowid := Gnade.Column_Int (Stmt, 0);
            Meta_Id := Gnade.Column_Int (Stmt, 1);
         end loop;
      else
         Cd.Prepare (Db, Meta_Rowid_Raw, Stmt);
         Gnade.Bind_int (Stmt, 1, Interfaces.C.int (Arch.Id));

         while Cd.Fetch (Db, Stmt) loop
            Meta_Id := Gnade.Column_Int (Stmt, 0);
         end loop;
      end if;

      Gnade.Finalize (Stmt);

      if Wheel_Rowid < 0 then
         raise Cwt.No_Matching_Wheel
           with "No wheel matches " & Cwt.Wheel_File_Name (Arch);
      end if;
      --  This will generate default minimum metadata
      if Meta_Id < 0 then
         return Make_Wheel_Metadata;
      end if;
      Cd.Prepare (Db, Wheel_Rowid_Raw, Stmt);
      Gnade.Bind_int (Stmt, 1, Interfaces.C.int (Meta_Id));
      while Cd.Fetch (Db, Stmt) loop
         --  TODO:  There's more to fetch.
         declare
            Mv : Metadata_Version := Mv_From_String (Gnade.Column_Text (Stmt, 0));
         begin
            return Make_Wheel_Metadata
              (Summary     => Gnade.Column_Text (Stmt, 1),
               Description => Gnade.Column_Text (Stmt, 2),
               Homepage    => Make_Url (Gnade.Column_Text (Stmt, 3)),
               License     => Gnade.Column_Text (Stmt, 4),
               Mv => mv);
         end;
      end loop;
      raise Missing_Metadata
        with "database consistency error.  " &
        "Expected to find metadata with id: " & Integer'Image (Meta_Id);
   end Fetch_Wheel_Metadata;

   procedure Set (Db_File : String; Chosen : Integer; Key : String; Value : String) is
      use Ae;
      Exception_Caught : Ae.Exception_Occurrence;
      Db               : Gnade.Database;
   begin
      declare
      begin
         Cd.Open (Db, Db_File);
         Set (Db, Chosen, Key, Value);
      exception
         when E : others =>
            Ae.Save_Occurrence (Exception_Caught, E);
            Cd.Rollback_Transaction (Db);
      end;
      Gnade.Close (Db);
      Ae.Reraise_Occurrence (Exception_Caught);
   end Set;

   function Effective_Metadata_Key
     (Db : Gnade.Database; Chosen : Integer; Emv : Metadata_Version)
     return Integer is
      use Gnade;
      Fetch_Main_Raw : constant String := "select wheels.metadata " &
        "from wheels where rowid = ?";
      Add_Main_Raw   : constant String := "insert into wheel_metadata_v1_0 " &
        "(metadata_version) values (?)";
      Rel_Main_Raw   : constant String := "update wheels set metadata = ? " &
        "where rowid = ?";
      Stmt           : Gnade.Statement;
      Found          : Boolean;
      Cmv            : Metadata_Version := Mv_1_0;
      Main_Rowid     : Integer;
      Next_Rowid     : Integer;
   begin
      Cd.Prepare (Db, Fetch_Main_Raw, Stmt);
      Gnade.Bind_Int (Stmt, 1, Interfaces.C.int (Chosen));
      Found := Cd.Fetch (Db, Stmt);

      if not Found or else Gnade.Column_Type (Stmt, 0) = Gnade.Sqlite_Null then
         Gnade.Finalize (Stmt);
         --  We need to create as many as we need
         declare
            Mvs : aliased constant String := Mv_To_String (Emv);
         begin
            Cd.Prepare (Db, Add_Main_Raw, Stmt);
            Gnade.Bind_Text (Stmt, 1, Mvs'Address, Mvs'Length);
            Cd.Step (Db, Stmt);
            Main_Rowid := Integer (Gnade.Last_Insert_Rowid (Db));

            Cd.Prepare (Db, Rel_Main_Raw, Stmt);
            Gnade.Bind_Int (Stmt, 1, Interfaces.C.int (Main_Rowid));
            Gnade.Bind_Int (Stmt, 2, Interfaces.C.int (Chosen));
            Cd.Step (Db, Stmt);
         end;
      else
         Main_Rowid := Gnade.Column_Int (Stmt, 0);
         Gnade.Finalize (Stmt);
      end if;

      Gt.Trace (Log, "main rowid: " & Integer'Image (Main_Rowid));
      Next_Rowid := Main_Rowid;

      while Cmv /= Emv loop
         declare
            Nmv : Metadata_Version := Next_Mv (Cmv);
            Raw_Find : constant String := "select older from " & Mv_To_Table (Nmv)
              & " where rowid = ?";
            Raw_Add : constant String := "insert into " & Mv_To_Table (Nmv) &
              "(older) values (?)";
         begin
            Cd.Prepare (Db, Raw_Find, Stmt);
            Gnade.Bind_Int (Stmt, 1, Interfaces.C.int (Next_Rowid));
            if Cd.Fetch (Db, Stmt) then
               Next_Rowid := Gnade.Column_Int (Stmt, 0);
               Gnade.Finalize (Stmt);
            else
               Gnade.Finalize (Stmt);
               Cd.Prepare (Db, Raw_Add, Stmt);
               Gnade.Bind_Int (Stmt, 1, Interfaces.C.int (Next_Rowid));
               Cd.Step (Db, Stmt);
               Next_Rowid := Integer (Gnade.Last_Insert_Rowid (Db));
            end if;
            Cmv := Nmv;
         end;
      end loop;

      return Next_Rowid;
   end Effective_Metadata_Key;

   procedure Set (Db : Gnade.Database; Chosen : Integer; Key : String; Value : String) is
      Fetch_Version_Raw : constant String := "select md.metadata_version " &
        "from wheels join wheel_metadata_v1_0 as md " &
        "on wheels.metadata = md.rowid " &
        "where wheels.rowid = ?";
      Stmt              : Gnade.Statement;
      Found             : Boolean;
      Mv                : Metadata_Version;
      Emv               : Metadata_Version;
      Emk               : Integer;
   begin
      Cd.Prepare (Db, Fetch_Version_Raw, Stmt);
      Gnade.Bind_Int (Stmt, 1, Interfaces.C.int (Chosen));
      Found := Cd.Fetch (Db, Stmt);
      if Found then
         --  We should have a version
         Mv := Mv_From_String (Gnade.Column_Text (Stmt, 0));
         Gnade.Finalize (Stmt);
      else
         --  Assume the latest version
         Mv := Mv_2_3;
      end if;
      Emv := Effective_Key_Version (Key, Mv);

      Cd.Begin_Transaction (Db);

      Emk := Effective_Metadata_Key (Db, Chosen, Emv);
      Gt.Trace (Log, "metadata key: " & Integer'Image (Emk));
      if Key_Is_Multiple (Key) then
         --  TODO: this seems incorrect.  Need to decide what exactly
         --  set for multi-keys should do.
         declare
            Upsert_Kv_Raw : constant String := "insert or replace into " &
              key_To_Table (key) & "(metadata, " & Key_To_Field (Key) &
              ") values (?, ?)";
            Value_Var : aliased constant String := Value;
         begin
            Cd.Prepare (Db, Upsert_Kv_Raw, Stmt);
            Gnade.Bind_Int (Stmt, 1, Interfaces.C.int (Emk));
            Gnade.Bind_Text (Stmt, 2, Value_Var'Address, Value_Var'Length);
            Cd.Step (Db, Stmt);
         end;
      else
         declare
            Update_Raw : constant String := "update " & Mv_To_Table (Emv) &
              " set " & Key_To_Field (Key) & " = ? where older = ?";
            Value_Var : aliased constant String := Value;
         begin
            Cd.Prepare (Db, Update_Raw, Stmt);
            Gnade.Bind_Text (Stmt, 1, Value_Var'Address, Value_Var'Length);
            Gnade.Bind_Int (Stmt, 2, Interfaces.C.int (Emk));
            Cd.Step (Db, Stmt);
         end;
      end if;

      Cd.Commit_Transaction (Db);
   end Set;

   procedure Show (Db_File : String; Chosen : Integer) is
      use Ae;
      Exception_Caught : Ae.Exception_Occurrence;
      Db               : Gnade.Database;
   begin
      declare
      begin
         Cd.Open (Db, Db_File);
         Show (Db, Chosen);
      exception
         when E : others =>
            Ae.Save_Occurrence (Exception_Caught, E);
      end;
      Gnade.Close (Db);
      Ae.Reraise_Occurrence (Exception_Caught);
   end Show;

   function Int_Or_Default
     (Stmt : Gnade.Statement; Pos : Integer; Default : Integer)
     return Integer is
      use Gnade;
   begin
      if Gnade.Column_Type (Stmt, Pos) /= Gnade.Sqlite_Null then
         return Gnade.Column_Int (Stmt, Pos);
      end if;
      return Default;
   end Int_Or_Default;

   procedure Call
     (Cr     : in out Dist_Reader;
      Db     : Gnade.Database;
      Column : String;
      Md     : Wheel_Metadata) is
   begin
      Ati.Put_Line ("Calling dist reader");
   end Call;

   procedure Call
     (Cr     : in out Dynamic_Reader;
      Db     : Gnade.Database;
      Column : String;
      Md     : Wheel_Metadata) is
   begin
      Ati.Put_Line ("Calling dynamic reader");
   end Call;

   procedure Call
     (Cr     : in out Platform_Reader;
      Db     : Gnade.Database;
      Column : String;
      Md     : Wheel_Metadata) is
   begin
      Ati.Put_Line ("Calling platform reader");
   end Call;

   procedure Call
     (Cr     : in out Author_Reader;
      Db     : Gnade.Database;
      Column : String;
      Md     : Wheel_Metadata) is
   begin
      Ati.Put_Line ("Calling author reader");
   end Call;

   procedure Call
     (Cr     : in out Author_Email_Reader;
      Db     : Gnade.Database;
      Column : String;
      Md     : Wheel_Metadata) is
   begin
      Ati.Put_Line ("Calling author email reader");
   end Call;

   procedure Call
     (Cr     : in out Keyword_Reader;
      Db     : Gnade.Database;
      Column : String;
      Md     : Wheel_Metadata) is
   begin
      Ati.Put_Line ("Calling keyword reader");
   end Call;

   procedure Call
     (Cr     : in out Provides_External_Reader;
      Db     : Gnade.Database;
      Column : String;
      Md     : Wheel_Metadata) is
   begin
      Ati.Put_Line ("Calling provides external reader");
   end Call;

   procedure Call
     (Cr     : in out Extra_Reader;
      Db     : Gnade.Database;
      Column : String;
      Md     : Wheel_Metadata) is
   begin
      Ati.Put_Line ("Calling extra reader");
   end Call;

   procedure Call
     (Cr     : in out Url_Reader;
      Db     : Gnade.Database;
      Column : String;
      Md     : Wheel_Metadata) is
   begin
      Ati.Put_Line ("Calling url reader");
   end Call;

   procedure Call
     (Cr     : in out Classifier_Reader;
      Db     : Gnade.Database;
      Column : String;
      Md     : Wheel_Metadata) is
   begin
      Ati.Put_Line ("Calling classifier reader");
   end Call;

   function Make_Column_Fetcher (Column : String; Reader : Column_Reader'Class)
                                return Column_Fetcher is
   begin
      return
        (Reader => Cr_Holder.To_Holder (Reader),
         N => Column'Length,
         Column => Column);
   end Make_Column_Fetcher;

   function Tables_Of_Version (Mv : Metadata_Version)
                              return Column_Fetcher_Vectors.Vector is
      use Column_Fetcher_Vectors;
      Result : Column_Fetcher_Vectors.Vector;
   begin
      case Mv is
         when Mv_1_0 =>
            declare
               Pr : Platform_Reader;
               Ar : Author_Reader;
               Kr : Keyword_Reader;
               Aer : Author_Email_Reader;
            begin
               Result.Append (Make_Column_Fetcher ("platform", Pr));
               Result.Append (Make_Column_Fetcher ("author", Ar));
               Result.Append (Make_Column_Fetcher ("keywords", Kr));
               Result.Append (Make_Column_Fetcher ("author_keywords", Aer));
            end;
         when Mv_1_1 =>
            declare
               Cr : Classifier_Reader;
               --  TODO: This is probably a different kind of platform
               Pr : Platform_Reader;
            begin
               Result.Append (Make_Column_Fetcher ("classifier", Cr));
               Result.Append (Make_Column_Fetcher ("supported_platform", Pr));
            end;
         when Mv_1_2 =>
            declare
               Pdr : Dist_Reader;
               Odr : Dist_Reader;
               Rdr : Dist_Reader;
               Mr : Author_Reader;
               Mer : Author_Email_Reader;
               --  TODO: Something's fishy with this name.  Looks like a typo
               Per : Provides_External_Reader;
               Ur : Url_Reader;
            begin
               Result.Append (Make_Column_Fetcher ("provides_dist", Pdr));
               Result.Append (Make_Column_Fetcher ("obsoletes_dist", Odr));
               Result.Append (Make_Column_Fetcher ("maintainer", Mr));
               Result.Append (Make_Column_Fetcher ("maintainer_email", Mer));
               Result.Append (Make_Column_Fetcher ("requires_dist", Rdr));
               Result.Append (Make_Column_Fetcher ("requires_external", Per));
               Result.Append (Make_Column_Fetcher ("project_url", Ur));
            end;
         when Mv_2_1 =>
            declare
               Er : Extra_Reader;
            begin
               Result.Append (Make_Column_Fetcher ("provides_extra", Er));
            end;
         when Mv_2_2 =>
            declare
               Dr : Dynamic_Reader;
            begin
               Result.Append (Make_Column_Fetcher ("dynamic", Dr));
            end;
         when Mv_2_3 => null;
      end case;
      return Result;
   end Tables_Of_Version;

   --  TODO: rename table_aliases_maps and key_vectors into something
   --  more generic
   procedure Fetch_Multi
     (Db         : Gnade.Database;
      M          : Wheel_Metadata;
      Mv         : Metadata_Version;
      Base_Rowid : Integer) is
   begin
      for Fetcher of Tables_Of_Version (Mv) loop
         declare
            Reader : Column_Reader'Class := Fetcher.Reader.Element;
         begin
            Reader.Call (Db, Fetcher.Column, M);
         end;
      end loop;
   end Fetch_Multi;

   procedure Show (Db : Gnade.Database; Chosen : Integer) is
      --  TODO: Restructure this so that it fetches and then prints
      --  the metadata instead of printing it right away.
      Aliases : Table_Aliases_Maps.Map;
      Stmt    : Gnade.Statement;
      --  TODO: this should be declared later, after we've fetched all
      --  the necessary single elements...
      M       : Wheel_Metadata := Make_Wheel_Metadata;
   begin
      Aliases.Include ("wheel_metadata_v1_0", "v1_0");
      Aliases.Include ("wheel_metadata_v1_1", "v1_1");
      Aliases.Include ("wheel_metadata_v1_2", "v1_2");
      Aliases.Include ("wheel_metadata_v2_1", "v2_1");
      Aliases.Include ("wheel_metadata_v2_2", "v2_2");
      Aliases.Include ("wheel_metadata_v2_3", "v2_3");
      declare
         use Gnade;
         Raw : constant String := "select " &
           Keys_To_Field_List (Aliases) &
           ", v1_0.rowid, v1_1.rowid, v1_2.rowid, v2_1.rowid, v2_2.rowid, v2_3.rowid" &
           " from wheels join " &
           "wheel_metadata_v1_0 as v1_0 on wheels.metadata = v1_0.rowid " &
           "left join wheel_metadata_v1_1 as v1_1 on v1_1.older = v1_0.rowid " &
           "left join wheel_metadata_v1_2 as v1_2 on v1_2.older = v1_1.rowid " &
           "left join wheel_metadata_v2_1 as v2_1 on v2_1.older = v1_2.rowid " &
           "left join wheel_metadata_v2_2 as v2_2 on v2_2.older = v2_1.rowid " &
           "left join wheel_metadata_v2_3 as v2_3 on v2_3.older = v2_2.rowid " &
           "where wheels.rowid = ?";
         Col : Integer := 0;
      begin
         Cd.Prepare (Db, Raw, Stmt);
         Gnade.Bind_Int (Stmt, 1, Interfaces.C.int (Chosen));
         if Cd.Fetch (Db, Stmt) then
            for Key of Single_Keys loop
               if Gnade.Column_Type (Stmt, Col) /= Gnade.Sqlite_Null then
                  Ati.Put_Line
                    (Key.To_String & ": " & Gnade.Column_Text (Stmt, Col));
               end if;
               Col := Col + 1;
            end loop;
            declare
               use Key_Vectors;
               type Rowids is array (Integer range 1 .. 6) of Integer;
               type Mkeys_Part is array (Integer range 1 .. 6) of Key_Vectors.Vector;
               type Mversions is array (Integer range 1 .. 6) of Metadata_Version;

               Rids : Rowids :=
                 (1 => Int_Or_Default (Stmt, Col + 1, -1),
                  2 => Int_Or_Default (Stmt, Col + 2, -1),
                  3 => Int_Or_Default (Stmt, Col + 3, -1),
                  4 => Int_Or_Default (Stmt, Col + 4, -1),
                  5 => Int_Or_Default (Stmt, Col + 5, -1),
                  6 => Int_Or_Default (Stmt, Col + 6, -1));
               Partitions : Mkeys_Part := (others => <>);
               Mvs : Mversions :=
                 (1 => Mv_1_0,
                  2 => Mv_1_1,
                  3 => Mv_1_2,
                  4 => Mv_2_1,
                  5 => Mv_2_2,
                  6 => Mv_2_3);
               I : Integer := 1;
            begin
               for Mkey of Multiple_Keys loop
                  declare
                     Xmkey : constant String := Mkey.To_String;
                     Ev : Metadata_Version := Effective_Key_Version (Xmkey, Mv_2_3);
                     Kv : Key_Vectors.Vector;
                  begin
                     case Ev is
                        --  TODO: figure out why it's not possible to append in place
                        when Mv_1_0 => Partitions (1) := Partitions (1) & Mkey;
                        when Mv_1_1 => Partitions (2) := Partitions (2) & Mkey;
                        when Mv_1_2 => Partitions (3) := Partitions (3) & Mkey;
                        when Mv_2_1 => Partitions (4) := Partitions (4) & Mkey;
                        when Mv_2_2 => Partitions (5) := Partitions (5) & Mkey;
                        when Mv_2_3 => Partitions (6) := Partitions (6) & Mkey;
                     end case;
                  end;
               end loop;
               for V of Partitions loop
                  exit when Rids (I) < 0;
                  Ati.Put_Line ("================");
                  Ati.Put_Line ("rowid: " & Integer'Image (Rids (I)));
                  Ati.Put_Line ("mversion: " & Mv_To_String (Mvs (I)));
                  I := I + 1;
                  for Key of V loop
                     Ati.Put_Line (Key.To_String & ": " & Key_To_Table (Key.To_String));
                  end loop;
               end loop;
               --  TODO: Figure out metadata version first.
               for I in 1 .. 6 loop
                  Fetch_Multi (Db, M, Mvs (I), Rids (1));
               end loop;
            end;
         else
            Ati.Put_Line ("Found no metadata");
         end if;
      end;
   end Show;

   type Rowid_X_Mv is array (Metadata_Version) of Integer;

   function Rowids_For_Chosen (Db : Gnade.Database; Chosen : Integer) return Rowid_X_Mv is
      use Gnade;
      Result : aliased Rowid_X_Mv      := (others => -1);
      Raw    : constant String := "select metadata from wheels where rowid = ?";
      Stmt   : Gnade.Statement;
   begin
      Cd.Prepare (Db, Raw, Stmt);
      Gnade.Bind_Int (Stmt, 1, Interfaces.C.int (Chosen));
      if Cd.Fetch (Db, Stmt) then
         Ati.Put_Line ("fetched");
         if Gnade.Column_Type (Stmt, 0) /= Gnade.Sqlite_Null then
            Ati.Put_Line ("found base id " & Integer'Image (Gnade.Column_Int (Stmt, 0)));
            Ati.Put_Line ("result " & Result'Image);
            Ati.Put_Line ("result v1.0: " & Result (Mv_1_0)'Image);
            Result (Mv_1_0) := Gnade.Column_Int (Stmt, 0);
            Gnade.Finalize (Stmt);
            for Mv in Mv_1_1 .. Mv_2_3 loop
               declare
                  Raw_Link : constant String := "select rowid from " & Mv_To_Table (Mv) &
                    " where older = ?";
                  Older : Integer := Result (Previous_Mv (Mv));
               begin
                  Ati.Put_Line ("binding int: " & Older'Image);
                  Cd.Prepare (Db, Raw_Link, Stmt);
                  Gnade.Bind_Int (Stmt, 1, Interfaces.C.int (Older));
                  if Cd.Fetch (Db, Stmt) then
                     Ati.Put_Line ("found older: " & Older'Image);
                     Result (Mv) := Gnade.Column_Int (Stmt, 0);
                     Gnade.Finalize (Stmt);
                  else
                     Gnade.Finalize (Stmt);
                     exit;
                  end if;
               end;
            end loop;
         end if;
      end if;
      return Result;
   end Rowids_For_Chosen;
   
   procedure Create_Linked_Metadata
     (Db : Gnade.Database; Rowids : Rowid_X_Mv; Kv : Metadata_Version) is
      Start_Mv : Metadata_Version := Mv_2_3;
   begin
      for Mv in Rowids'Range loop
         if Rowids (Mv) = -1 then
            Start_Mv := Mv;
            exit;
         end if;
      end loop;
      Ati.Put_Line ("Start creating md entries: " & Mv_To_String (Start_Mv));
   end Create_Linked_Metadata;

   procedure Append (Db_File : String; Chosen : Integer; Key : String; Value : String) is
      use Ae;
      Exception_Caught : Ae.Exception_Occurrence;
      Db               : Gnade.Database;
   begin
      declare
      begin
         Cd.Open (Db, Db_File);
         Append (Db, Chosen, Key, Value);
      exception
         when E : others =>
            Ae.Save_Occurrence (Exception_Caught, E);
            if not Cd.Get_Autocommit (Db) then
               Cd.Rollback_Transaction (Db);
            end if;
      end;
      Gnade.Close (Db);
      Ae.Reraise_Occurrence (Exception_Caught);
   end Append;

   procedure Append (Db : Gnade.Database; Chosen : Integer; Key : String; Value : String) is
      Raw       : constant String         := "";
      Stmt      : Gnade.Statement;
      Rowids    : Rowid_X_Mv              := Rowids_For_Chosen (Db, Chosen);
      Kv        : Metadata_Version        := Effective_Key_Version (Key, Mv_2_3);
      Value_Var : aliased constant String := Value;
   begin
      if not Key_Is_Multiple (Key) then
         raise Unrecognized_Metadata_Key with Key & " must allow multiple values";
      end if;

      Cd.Begin_Transaction (Db);

      declare
         Raw : constant String := "insert into " & Key_To_Table (Key) &
           " (" & Key_To_Field (Key) & ", metadata) values (?, ?)";
      begin
         if Rowids (Kv) = -1 then
            Create_Linked_Metadata (Db, Rowids, Kv);
         end if;
         Cd.Prepare (Db, Raw, Stmt);
         Gnade.Bind_Text (Stmt, 1, Value_Var'Address, Value_Var'Length);
         Gnade.Bind_Int (Stmt, 2, Interfaces.C.int (Rowids (kv)));
         Cd.Step (Db, Stmt);
      end;

      Cd.Commit_Transaction (Db);
   end Append;

end Cog.Wheel_Metadata;
