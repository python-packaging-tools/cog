with Ada.Text_IO;
with Ada.Streams.Stream_IO; use Ada.Streams.Stream_IO;
with Ada.Unchecked_Conversion;
with GNAT.OS_Lib;

package body Cog is
   package Os renames GNAT.OS_Lib;

   procedure Call_Process (Command : String) is
      Args        : Os.Argument_List_Access;
      Exit_Status : Integer;
      Pid         : Os.Process_Id;
      Success     : Boolean;
   begin
      Args := Os.Argument_String_To_List (Command);

      Pid := Os.Non_Blocking_Spawn
         (Program_Name => Args (Args'First).all,
          Args         => Args (Args'First + 1 .. Args'Last));
      Os.Wait_Process (Pid, Success);
      Os.Free (Args);

   end Call_Process;

end Cog;
