with Cog_Cli.Options;

package Cog_Cli.Init is
   
   procedure Parse (C : in out Cog_Cli.Options.Context);
   
end Cog_Cli.Init;
