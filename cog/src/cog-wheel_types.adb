with Ada.Strings.Unbounded;
with Ada.Containers.Generic_Array_Sort;
with GNATCOLL.Traces;

package body Cog.Wheel_Types is

   package Asu renames Ada.Strings.Unbounded;
   package Gt renames GNATCOLL.Traces;

   Log : constant Gt.Trace_Handle :=
     Gt.Create ("cog.wheel_types", Stream => "&2");

   function Tag_Abi (Tag : Wheel_Tag) return String is
      Debugging : constant String := (if Tag.Debugging then "d" else "");
      Pymalloc : constant String := (if Tag.Pymalloc then "m" else "");
      Wide_Unicode : constant String := (if Tag.Wide_Unicode then "w" else "");
   begin
      return Debugging & Pymalloc & Wide_Unicode & Tag.Abi;
   end Tag_Abi;

   function Tags_To_String (Tags : Tags_Vectors.Vector) return String is
      use Asu;
      type Us_Array is array (Positive range <>) of Asu.Unbounded_String;
      procedure Sort_Tag is new Ada.Containers.Generic_Array_Sort
        (Index_Type   => Positive,
         Element_Type => Asu.Unbounded_String,
         Array_Type   => Us_Array);

      Abis : Us_Array (1 .. Integer (Tags.Length));
      Pythons : Us_Array (1 .. Integer (Tags.Length));
      Platforms : Us_Array (1 .. Integer (Tags.Length));
      Tmp : Asu.Unbounded_String;
      Dot : Asu.Unbounded_String := Asu.To_Unbounded_String (".");
      Shy : Asu.Unbounded_String := Asu.To_Unbounded_String ("-");
   begin
      --  TODO: This needs a facelift using XString
   -- for x in pytag.split('.'):
   --     for y in abitag.split('.'):
   --         for z in archtag.split('.'):
   --             yield '-'.join((x, y, z))
      for I in 1 .. Integer (Tags.Length) loop
         Abis (I) := Asu.To_Unbounded_String (Tag_Abi (Tags (I)));
         Pythons (I) := Asu.To_Unbounded_String (Tags (I).Python);
         Platforms (I) := Asu.To_Unbounded_String (Tags (I).Platform);
      end loop;

      Sort_Tag (Abis);
      Sort_Tag (Pythons);
      Sort_Tag (Platforms);

      declare
         Slow : Natural := 1;
      begin
         Tmp := Pythons (Slow);
         for I in 2 .. Integer (Tags.Length) loop
            if Pythons (Slow) /= Pythons (I) then
               Slow := I;
               Tmp := Tmp & Dot & Pythons (I);
            end if;
         end loop;

         Slow := 1;
         Tmp := Tmp & Shy & Abis (Slow);

         for I in 2 .. Integer (Tags.Length) loop
            if Abis (Slow) /= Abis (I) then
               Slow := I;
               Tmp := Tmp & Dot & Abis (I);
            end if;
         end loop;

         Slow := 1;
         Tmp := Tmp & Shy & Platforms (Slow);

         for I in 2 .. Integer (Tags.Length) loop
            if Platforms (Slow) /= Platforms (I) then
               Slow := I;
               Tmp := Tmp & Dot & Platforms (I);
            end if;
         end loop;
      end;

      return Asu.To_String (Tmp);
   end Tags_To_String;

   function Wheel_File_Name (Arch : Wheel) return String is
      --  distribution-version(-build)?-python-abi-platform.whl
      Result : constant String := Arch.Distribution & "-" &
        Arch.Version & "-" &
        (if Arch.Build /= "" then Arch.Build & "-" else "") &
        Tags_To_String (Arch.Tags) & ".whl";
   begin
      Gt.Trace (Log, "Wheel_File_Name: " & Arch.Distribution);
      return Result;
   end Wheel_File_Name;

end Cog.Wheel_Types;
