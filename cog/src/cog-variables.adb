with Ada.Text_IO;
with Ada.Exceptions;
with Interfaces.C;
with GNATCOLL.SQL.Sqlite.Gnade;
with GNATCOLL.Traces;
with Cog.Database;

package body Cog.Variables is
   
   package Ati renames Ada.Text_IO;
   package Ae renames Ada.Exceptions;
   package Cd renames Cog.Database;
   package Gt renames GNATCOLL.Traces;

   Log : constant Gt.Trace_Handle := Gt.Create ("cog.variables", Stream => "&2");

   procedure Upsert (Db_File : String; Variable : String; Val : Variable_Type) is
      use Ae;
      Exception_Caught : Ae.Exception_Occurrence;
      Db               : Gnade.Database;
   begin
      declare
      begin
         Cd.Open (Db, Db_File);
         Upsert (Db, Variable, Val);
      exception
         when E : others =>
            Ae.Save_Occurrence (Exception_Caught, E);
      end;
      Gnade.Close (Db);
      Ae.Reraise_Occurrence (Exception_Caught);
   end Upsert;

   procedure Upsert (Db : Gnade.Database; Variable : String; Val : Variable_Type) is
      Stmt : Gnade.Statement;
   begin
      if Val.T = Int then
         declare
            Raw  : constant String := "insert or replace into " &
              "variables(vname, ivalue) values (?, ?)";
            Ival : constant Integer := Val.Int_Val;
         begin
            Cd.Prepare (Db, Raw, Stmt);
            Gnade.Bind_Text (Stmt, 1, Variable'Address, Variable'Length);
            Gnade.Bind_Int (Stmt, 2, Interfaces.C.Int (Ival));
            Gt.Trace (Log, "Setting: " & Variable & " to " & Integer'Image (Ival));
         end;
      else
         declare
            Raw  : constant String := "insert or replace into " &
              "variables(vname, tvalue) values (?, ?)";
            Sval : constant String := Asu.To_String (Val.Str_Val);
         begin
            Cd.Prepare (Db, Raw, Stmt);
            Gnade.Bind_Text (Stmt, 1, Variable'Address, Variable'Length);
            Gnade.Bind_Text (Stmt, 2, Sval'Address, Sval'Length);
            Gt.Trace (Log, "Setting: " & Variable & " to " & Sval);
         end;
      end if;
      Cd.Step (Db, Stmt);
   end Upsert;

   procedure List (Db_File : String; Regexp : String := ".") is
      use Ae;
      Exception_Caught : Ae.Exception_Occurrence;
      Db               : Gnade.Database;
   begin
      declare
      begin
         Cd.Open (Db, Db_File);
         List (Db, Regexp);
      exception
         when E : others =>
            Ae.Save_Occurrence (Exception_Caught, E);
      end;
      Gnade.Close (Db);
      Ae.Reraise_Occurrence (Exception_Caught);
   end List;

   procedure List (Db : Gnade.Database; Regexp : String := ".") is
      use Gnade;
      Raw  : constant String := "select vname, ivalue, tvalue from variables where vname regexp ?";
      Stmt : Gnade.Statement;
      Exp : aliased constant String := Regexp;
   begin
      Cd.Prepare (Db, Raw, Stmt);
      Gnade.Bind_Text (Stmt, 1, Exp'Address, Exp'Length);
      while Cd.Fetch (Db, Stmt) loop
         if Gnade.Column_Type (Stmt, 1) = Gnade.Sqlite_Null then
            declare
               Vname : constant String := Gnade.Column_Text (Stmt, 0);
               Svalue : constant String := Gnade.Column_Text (Stmt, 2);
            begin
               Ati.Put_Line (Vname & " = '" & Svalue & "'");
            end;
         else
            declare
               Vname : constant String := Gnade.Column_Text (Stmt, 0);
               Ivalue : constant Integer := Gnade.Column_Int (Stmt, 1);
            begin
               Ati.Put_Line (Vname & " = " & Integer'Image (Ivalue));
            end;
         end if;
      end loop;
   end List;

   function Get (Db_File : String; Variable : String) return Variable_Type is
      use Ae;
      Exception_Caught : Ae.Exception_Occurrence;
      Db               : Gnade.Database;
      Result_T         : Vt;
      Result_Str       : Asu.Unbounded_String;
      Result_Int       : Integer;
   begin
      declare
      begin
         Cd.Open (Db, Db_File);
         declare
            Result : Variable_Type := Get (Db, Variable);
         begin
            Result_T := Result.T;
            if Result_T = Str then
               Result_Str := Result.Str_Val;
            else
               Result_Int := Result.Int_Val;
            end if;
         end;
      exception
         when E : others =>
            Ae.Save_Occurrence (Exception_Caught, E);
      end;
      Gnade.Close (Db);
      Ae.Reraise_Occurrence (Exception_Caught);
      return (if Result_T = Str then
                (Rt => Str, T => Str, Str_Val => Result_Str) else
                  (Rt => Int, T => Int, Int_Val => Result_Int));
   end Get;

   function Get (Db : Gnade.Database; Variable : String) return Variable_Type is
      use Gnade;
      Raw  : constant String := "select ivalue, tvalue from variables " &
        "where vname = ?";
      Stmt : Gnade.Statement;
      Var : aliased constant String := Variable;
   begin
      Gt.Trace (Log, "Retrieving: " & Variable);
      Cd.Prepare (Db, Raw, Stmt);
      Gnade.Bind_Text (Stmt, 1, Var'Address, Var'Length);
      while Cd.Fetch (Db, Stmt) loop
         if Gnade.Column_Type (Stmt, 0) = Gnade.Sqlite_Null then
            declare
               Svalue : constant String := Gnade.Column_Text (Stmt, 1);
               Result : Variable_Type :=
                 (Rt => Str,
                  T => Str,
                  Str_Val => Asu.To_Unbounded_String (Svalue));
            begin
               return Result;
            end;
         else
            declare
               Ivalue : constant Integer := Gnade.Column_Int (Stmt, 0);
               Result : Variable_Type := (Rt => Int, T => Int, Int_Val => Ivalue);
            begin
               return Result;
            end;
         end if;
      end loop;
      raise Missing_Variable with Variable & " not found";
   end Get;

   procedure Unset (Db_File : String; Variable : String) is
      use Ae;
      Exception_Caught : Ae.Exception_Occurrence;
      Db               : Gnade.Database;
   begin
      declare
      begin
         Cd.Open (Db, Db_File);
         Unset (Db, Variable);
      exception
         when E : others =>
            Ae.Save_Occurrence (Exception_Caught, E);
      end;
      Gnade.Close (Db);
      Ae.Reraise_Occurrence (Exception_Caught);
   end Unset;

   procedure Unset (Db : Gnade.Database; Variable : String) is
      use Gnade;
      Raw  : constant String := "delete from variables where vname = ?";
      Stmt : Gnade.Statement;
      Var : aliased constant String := Variable;
   begin
      Gt.Trace (Log, "Deleting: " & Variable);
      Cd.Prepare (Db, Raw, Stmt);
      Gnade.Bind_Text (Stmt, 1, Var'Address, Var'Length);
      Cd.Step (Db, Stmt);
   end Unset;

end Cog.Variables;
