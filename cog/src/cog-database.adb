with Ada.Exceptions;
with GNAT.Regpat;
with GNATCOLL.Traces;
with System;
with Interfaces.C;
with Interfaces.C.Strings;
with Cog_Database;

package body Cog.Database is

   package Ae renames Ada.Exceptions;
   package Gt renames GNATCOLL.Traces;
   package Cd renames Cog_Database;
   package Gp renames GNAT.Regpat;

   use Gnade;

   Log : constant Gt.Trace_Handle := Gt.Create ("cog.database", Stream => "&2");

-- int sqlite3_create_function_v2(
--   sqlite3 *db,
--   const char *zFunctionName,
--   int nArg,
--   int eTextRep,
--   void *pApp,
--   void (*xFunc)(sqlite3_context*,int,sqlite3_value**),
--   void (*xStep)(sqlite3_context*,int,sqlite3_value**),
--   void (*xFinal)(sqlite3_context*),
--   void(*xDestroy)(void*)
-- );
   
-- #define SQLITE_UTF8           1    /* IMP: R-37514-35566 */
-- #define SQLITE_UTF16LE        2    /* IMP: R-03371-37637 */
-- #define SQLITE_UTF16BE        3    /* IMP: R-51971-34154 */
-- #define SQLITE_UTF16          4    /* Use native byte order */
-- #define SQLITE_ANY            5    /* Deprecated */
-- #define SQLITE_UTF16_ALIGNED  8    /* sqlite3_create_collation only */

-- #define SQLITE_DETERMINISTIC    0x000000800
-- #define SQLITE_DIRECTONLY       0x000080000
-- #define SQLITE_SUBTYPE          0x000100000
-- #define SQLITE_INNOCUOUS        0x000200000
-- #define SQLITE_RESULT_SUBTYPE   0x001000000

   type Create_Function_Flags is mod (2 ** 32);
   Sqlite_Utf8          : constant Create_Function_Flags := 1;
   Sqlite_Utf16le       : constant Create_Function_Flags := 2;
   Sqlite_Utf16be       : constant Create_Function_Flags := 3;
   Sqlite_Utf16         : constant Create_Function_Flags := 4;
   Sqlite_Any           : constant Create_Function_Flags := 5;
   Sqlite_Utf16_Aligned : constant Create_Function_Flags := 8;

   Sqlite_Deterministic  : constant Create_Function_Flags := 2048;
   Sqlite_Directonly     : constant Create_Function_Flags := 524288;
   Sqlite_Subtype        : constant Create_Function_Flags := 1048576;
   Sqlite_Innocuous      : constant Create_Function_Flags := 2097152;
   Sqlite_Result_Subtype : constant Create_Function_Flags := 16777216;

   type Sqlite_Value is new System.Address;
   pragma Convention (C, Sqlite_Value);

   type Sqlite_Values_Unconstrained is array (Integer range <>) of Sqlite_Value;
   pragma Convention (C, Sqlite_Values_Unconstrained);

   subtype Sqlite_Values_Array is Sqlite_Values_Unconstrained
     (Natural'First .. Natural'Last);

   type Sqlite3_User_Function is access procedure
     (Context : System.Address;
      N_Args : Integer;
      Values : Sqlite_Values_Array);
   pragma Convention (C, Sqlite3_User_Function);

   procedure Create_Function
     (Db : Gnade.Database;
      Function_Name : Interfaces.C.Strings.chars_ptr;
      N_Args : Integer;
      String_Encoding : Create_Function_Flags;
      Data : System.Address;
      X_Func : Sqlite3_User_Function;
      X_Step : System.Address := System.Null_Address;
      X_Final : System.Address := System.Null_Address;
      Destroy : System.Address := System.Null_Address);
   pragma Import (C, Create_Function, "sqlite3_create_function_v2");
   
   procedure Sqlite_Regexp
     (Context : System.Address;
      N_Args : Integer;
      Values : Sqlite_Values_Array);
   pragma Convention (C, Sqlite_Regexp);

   procedure Sqlite_Regexp
     (Context : System.Address;
      N_Args : Integer;
      Values : Sqlite_Values_Array) is
      use Gp;
      function Value_Str (V  : Sqlite_Value)
                          return Interfaces.C.Strings.chars_ptr;
      pragma Import (C, Value_Str, "sqlite3_value_text");
      procedure Result_Int (Ctx : System.Address; Value : Integer);
      pragma Import (C, Result_Int, "sqlite3_result_int");
      Expression : constant String :=
        Interfaces.C.Strings.Value (Value_Str (Values (Values'First)));
      Input : constant String :=
        Interfaces.C.Strings.Value (Value_Str (Values (Values'First + 1)));
      Re      : constant Gp.Pattern_Matcher := Gp.Compile (Expression);
      Matches : Gp.Match_Array (0 .. 1);
   begin
      Gp.Match (Re, Input, Matches);
      if Matches (0) = Gp.No_Match then
         Result_Int (Context, 0);
         Gt.Trace (Log, "sqlite-regexp no match: " & Input & " to: " & Expression);
      else
         Result_Int (Context, 1);
         Gt.Trace (Log, "sqlite-regexp matching: " & Input & " to: " & Expression);
      end if;
   end Sqlite_Regexp;

   procedure Db_Logger
     (Data : System.Address;
      Error_Code : Result_Codes;
      Message : Interfaces.C.Strings.chars_ptr);
   pragma Convention (C, Db_Logger);

   procedure Db_Logger
     (Data : System.Address;
      Error_Code : Result_Codes;
      Message : Interfaces.C.Strings.chars_ptr) is
      pragma Unreferenced (Data);
   begin
      Gt.Trace (Log, "sqlite: " & Interfaces.C.Strings.Value (Message));
   end;

   function Print_Statement (Stmt : Gnade.Statement) return String is
      function Expand_Sql (Stmt  : Gnade.Statement)
                          return Interfaces.C.Strings.chars_ptr;
      pragma Import (C, Expand_Sql, "sqlite3_expanded_sql");
      procedure Sqlite_Free (Ptr  : Interfaces.C.Strings.chars_ptr);
      pragma Import (C, Sqlite_Free, "sqlite3_free");
      C_Sql : Interfaces.C.Strings.chars_ptr := Expand_Sql (Stmt);
      Sql : String := Interfaces.C.Strings.Value (C_Sql);
   begin
      Sqlite_Free (C_Sql);
      return Sql;
   end Print_Statement;

   type Sqlite3_Trace_Option is
     (Sqlite_Trace_Stmt,
      Sqlite_Trace_Profile,
      Sqlite_Trace_Row,
      Sqlite_Trace_close);

   for Sqlite3_Trace_Option use
     (Sqlite_Trace_Stmt    => 1,
      Sqlite_Trace_Profile => 2,
      Sqlite_Trace_Row     => 4,
      Sqlite_Trace_Close   => 8);

   type Tracer is access procedure
     (Option  : Sqlite3_Trace_Option;
      Context : System.Address;
      Stmt    : Gnade.Statement;
      Unknown : System.Address);
   pragma Convention (C, Tracer);

   procedure Sqlite3_Trace
     (Db : Gnade.Database;
      Option : Sqlite3_Trace_Option;
      Func : Tracer;
      Context : System.Address);
   pragma Import (C, Sqlite3_Trace, "sqlite3_trace_v2");

   procedure Db_Tracer
     (Option  : Sqlite3_Trace_Option;
      Context : System.Address;
      Stmt    : Gnade.Statement;
      Unknown : System.Address);
   pragma Convention (C, Db_Tracer);

   procedure Db_Tracer
     (Option  : Sqlite3_Trace_Option;
      Context : System.Address;
      Stmt    : Gnade.Statement;
      Unknown : System.Address) is
      Sql : constant String := Print_Statement (Stmt);
   begin
      Gt.Trace (Log, "sqlite: " & Sql);
   end Db_Tracer;

   function Get_Autocommit (Db: Gnade.Database) return Boolean is
      function Sqlite_Autocommit (Db : Gnade.Database) return Interfaces.C.int;
      pragma Import (C, Sqlite_Autocommit, "sqlite3_get_autocommit");
      Result : Integer := Integer (Sqlite_Autocommit (Db));
   begin
      if Result = 0 then
         return False;
      end if;
      return True;
   end Get_Autocommit;

   procedure Open (Db : in out Gnade.Database; Db_File : String) is
      Rc : Gnade.Result_Codes;
   begin
      --  TODO: Only set up logging and tracing when logger is active.
      Gnade.Set_Config_Log (Db_Logger'Access);
      Gt.Trace (Log, "Sqlite logs configured");
      Gnade.Open (Db, Db_File, Status => Rc);
      Sqlite3_Trace (Db, Sqlite_Trace_Stmt, Db_Tracer'Access, System.Null_Address);
      Create_Function
        (Db,
         Interfaces.C.Strings.New_String ("regexp"),
         2,
         Sqlite_Deterministic or Sqlite_Utf8,
         System.Null_Address,
         Sqlite_Regexp'Access);
      if Rc /= Gnade.Sqlite_Ok then
         raise Cannot_Open_Database
           with "Failed to open database " & Rc'Image;
      end if;
   end Open;

   procedure Prepare
     (Db : Gnade.Database; Raw : String; Stmt: in out Gnade.Statement) is
      Rc : Gnade.Result_Codes;
   begin
      Gnade.Prepare (Db, Raw, Stmt, Rc);
      Gt.Trace (Log, "Prepared: " & Raw);
      if Rc /= Gnade.Sqlite_Ok then
         raise Cannot_Prepare_Query
           with Gnade.Error_Msg (Db) & " rc: " & Rc'Image;
      end if;
   end Prepare;

   procedure Step (Db : Gnade.Database; Stmt : Gnade.Statement) is
      use Ae;
      Exception_Caught : Ae.Exception_Occurrence;
      Stmt_Var : Gnade.Statement := Stmt;
   begin
      declare
         Rc : Gnade.Result_Codes;
      begin
         Gnade.Step (Stmt_Var, Rc);
         if Rc /= Gnade.Sqlite_Done then
            raise Cannot_Execute_Query
              with Print_Statement (Stmt_Var) & ": " & 
                Gnade.Error_Msg (Db) & " rc: " & Rc'Image;
         end if;
         Gt.Trace (Log, "Step: " & Print_Statement (Stmt) & " Rc: " & Rc'Image);
      exception
         when E : others =>
            Ae.Save_Occurrence (Exception_Caught, E);
      end;
      Gnade.Finalize (Stmt);
      Ae.Reraise_Occurrence (Exception_Caught);
   end Step;

   procedure Step_One (Db : Gnade.Database; Stmt : Gnade.Statement) is
      --  The difference is this one finalizes the statement only in
      --  the case of error.
      use Ae;
      Exception_Caught : Ae.Exception_Occurrence;
      Stmt_Var : Gnade.Statement := Stmt;
   begin
      declare
         Rc : Gnade.Result_Codes;
      begin
         Gnade.Step (Stmt_Var, Rc);
         if Rc /= Gnade.Sqlite_Done then
            raise Cannot_Execute_Query
              with Print_Statement (Stmt_Var) & ": " &
                Gnade.Error_Msg (Db) & " rc: " & Rc'Image;
         end if;
         Gt.Trace (Log, "Step: " & Print_Statement (Stmt) & " Rc: " & Rc'Image);
      exception
         when E : others =>
            Ae.Save_Occurrence (Exception_Caught, E);
            Gnade.Finalize (Stmt);
      end;
      Ae.Reraise_Occurrence (Exception_Caught);
   end Step_One;

   function Fetch (Db : Gnade.Database; Stmt : Gnade.Statement) return Boolean is
      Stmt_Var : Gnade.Statement := Stmt;
      Rc : Gnade.Result_Codes;
   begin
      Gnade.Step (Stmt_Var, Rc);
      if Rc /= Gnade.Sqlite_Done and Rc /= Gnade.Sqlite_Row then
         Gnade.Finalize (Stmt);
         raise Cannot_Execute_Query
           with Gnade.Error_Msg (Db) & " rc: " & Rc'Image;
      end if;
      return Rc = Sqlite_Row;
   end Fetch;

   procedure Populate_Db (Db_File : String) is
      Exception_Caught : Ae.Exception_Occurrence;
      Db               : Gnade.Database;
   begin
      declare
         Schema         : constant Cd.Content_Type :=
           Cog_Database.Get_Content ("schema");
         Stmt : Gnade.Statement;
         -- Raw : access String;
      begin
         Open (Db, Db_File);
         for Raw of Schema.Content.all loop
            Prepare (Db, Raw.all, Stmt);
            Step (Db, Stmt);
         end loop;
      exception
         when E : others =>
            Ae.Save_Occurrence (Exception_Caught, E);
      end;
      Gnade.Close (Db);
      Ae.Reraise_Occurrence (Exception_Caught);
   end Populate_Db;

   --  TODO: The actual meaning of arguments is:
   -- int sqlite3_exec(
   --   sqlite3*,                                  /* An open database */
   --   const char *sql,                           /* SQL to be evaluated */
   --   int (*callback)(void*,int,char**,char**),  /* Callback function */
   --   void *,                                    /* 1st argument to callback */
   --   char **errmsg                              /* Error msg written here */
   -- );
   -- Also, this splits SQL statements with semicolons so, we might
   -- want to use this instead of our init loop...

   function Exec
     (Db      : Gnade.Database;
      Sql     : Interfaces.C.Strings.chars_ptr;
      X_Step  : System.Address := System.Null_Address;
      X_Final : System.Address := System.Null_Address;
      Destroy : System.Address := System.Null_Address) return Gnade.Result_Codes;
   pragma Import (C, Exec, "sqlite3_exec");

   procedure Begin_Transaction (Db : Gnade.Database) is
      Rc : Gnade.Result_Codes;
   begin
      Rc := Exec (Db, Interfaces.C.Strings.New_String ("begin"));
      if Rc /= Gnade.Sqlite_Done and Rc /= Gnade.Sqlite_Ok then
         raise Cannot_Execute_Query
           with Gnade.Error_Msg (Db) & " rc: " & Rc'Image;
      end if;
   end Begin_Transaction;

   procedure Commit_Transaction (Db : Gnade.Database) is
      Rc : Gnade.Result_Codes;
   begin
      Rc := Exec (Db, Interfaces.C.Strings.New_String ("commit"));
      if Rc /= Gnade.Sqlite_Done and Rc /= Gnade.Sqlite_Ok then
         raise Cannot_Execute_Query
           with Gnade.Error_Msg (Db) & " rc: " & Rc'Image;
      end if;
   end Commit_Transaction;

   procedure Rollback_Transaction (Db : Gnade.Database) is
      Rc : Gnade.Result_Codes;
   begin
      Rc := Exec (Db, Interfaces.C.Strings.New_String ("rollback"));
      if Rc /= Gnade.Sqlite_Done and Rc /= Gnade.Sqlite_Ok then
         raise Cannot_Execute_Query
           with Gnade.Error_Msg (Db) & " rc: " & Rc'Image;
      end if;
   end Rollback_Transaction;

end Cog.Database;
