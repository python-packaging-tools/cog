with Ada.Text_IO;
with Cog_Cli.Options;
with Ada.Strings.Unbounded;
with Cog;
with Cog.Recipes;
with Cog.Wheel;

package body Cog_Cli.Recipes is

   package Ati renames Ada.Text_IO;
   package Asu renames Ada.Strings.Unbounded;
   package Cco renames Cog_Cli.Options;
   package Cr renames Cog.Recipes;
   package Cw renames Cog.Wheel;

   procedure Parse_Add (C : in out Cco.Context) is
      Cl        : Cco.Context;
      Continue  : Boolean;
      Help      : Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-recipe-add"));
      Command   : aliased Cco.Value_Handler;
      Chosen    : Integer := -1;
   begin
      Cco.Parse_Common_Select_Wheel
        (C, Cl, Help, "recipe add", 2, 2, Chosen, Continue);

      if Continue then
         declare
            Dependency : aliased Cr.Recipe := Cr.Make_Recipe
              (Cco.Argument (Cl, Cl.Index + 1), Cco.Argument (Cl));
         begin
            Cr.Add (Asu.To_String (Cco.Db_File), Dependency, Chosen);
         end;
      end if;
   end Parse_Add;

   procedure Parse_Select (C : in out Cco.Context) is
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-recipe-select"));
   begin
      Cco.Parse_Common (C, Cl, Help, "recipe select", 1, Continue);
   end Parse_Select;

   procedure Parse_List (C : in out Cco.Context) is
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-recipe-list"));
   begin
      Cco.Parse_Common (C, Cl, Help, "recipe list", 0, Continue);
   end Parse_List;

   procedure Parse_Remove (C : in out Cco.Context) is
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-recipe-remove"));
   begin
      Cco.Parse_Common (C, Cl, Help, "recipe remove", 1, Continue);
   end Parse_Remove;

   procedure Parse_Show (C : in out Cco.Context) is
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-recipe-show"));
   begin
      Cco.Parse_Common (C, Cl, Help, "recipe show", 1, Continue);
   end Parse_Show;

   procedure Parse (C : in out Cco.Context) is
      Rc       : Cco.Context;
      Help     : aliased Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-recipe"));
   begin
      Cco.Include (Rc, "--help", "h", Help'Unchecked_Access);
      Rc.Index := C.Index;
      Cco.Parse (Rc);

      if Cco.Has_More (Rc) then
         declare
            Subcommand : constant String := Cco.Argument (Rc);
         begin
            Gt.Trace (Log, "Parsing subcommand: " & Subcommand);
            Rc.Index := Rc.Index + 1;
            if Subcommand = "add" then
               Gt.Trace (Log, "Add subcommand");
               Parse_Add (Rc);
            elsif Subcommand = "list" then
               Gt.Trace (Log, "List subcommand");
               Parse_List (Rc);
            elsif Subcommand = "remove" then
               Gt.Trace (Log, "remove subcommand");
               Parse_Remove (Rc);
            elsif Subcommand = "show" then
               Gt.Trace (Log, "show subcommand");
               Parse_Show (Rc);
            else
               raise Cco.Unknown_Subcommand
                 with Subcommand & " is not a known subcommand";
            end if;
         end;
      end if;

   end Parse;
end Cog_Cli.Recipes;
