with Ada.Exceptions;
with Ada.Streams;
with Ada.Streams.Stream_IO;
with Ada.Directories;
with Ada.Directories.Hierarchical_File_Names;
with GNAT.SHA256;
with GNAT.OS_Lib;
with GNATCOLL.Strings;
with Zip.Create;
with Cog.Wheel_Metadata;

package body Cog.Wheel_Build is

   package Ae renames Ada.Exceptions;
   package As renames Ada.Streams;
   package Asio renames Ada.Streams.Stream_IO;
   package Ad renames Ada.Directories;
   package Adh renames Ada.Directories.Hierarchical_File_Names;
   package Sh renames GNAT.SHA256;
   package Gol renames GNAT.OS_Lib;
   package Gs renames GNATCOLL.Strings;
   package Zc renames Zip.Create;
   package Cwm renames Cog.Wheel_Metadata;

   Cannot_Build : exception;

   function Checksum (File_Name : String) return Sh.Message_Digest is
      Buffer : As.Stream_Element_Array (1 .. 4096);
      Last   : As.Stream_Element_Offset;
      F      : Asio.File_Type;
      C      : Sh.Context;
      use type Ada.Streams.Stream_Element_Offset;
   begin
      Asio.Open (F, Mode => Asio.In_File, Name => File_Name);
      loop
         Asio.Read (F, Item => Buffer, Last => Last);
         Sh.Update (C, Buffer (1 .. Last));
         exit when Last < Buffer'Last;
      end loop;
      --  TODO: try-catch make sure this is closed
      Asio.Close (F);
      return Sh.Digest (C);
   end Checksum;

   procedure Zip_One_File (Info : in out Zc.Zip_Create_Info; File_Name : String) is
   begin
      --  TODO:  this will have to store file's checksum in addition to writing it.
      Zc.Add_File (Info, File_Name);
   end Zip_One_File;

   function Dist_Info_Entry (Arch : Cwt.Wheel; Name : String) return String is
      Wheel_Name : constant String := Cwt.Wheel_File_Name (Arch);
   begin
      return Wheel_Name (Wheel_Name'First .. Wheel_Name'Last - 4) &
        "/.dist-info/" & Name;
   end Dist_Info_Entry;

   procedure Write_Metadata
     (Db   : Gnade.Database;
      Info : in out Zc.Zip_Create_Info;
      Arch : Cwt.Wheel;
      Vars : Vars_Maps.Map) is
      Md         : Cwm.Wheel_Metadata := Cwm.Fetch_Wheel_Metadata (Db, Arch);
      Contents   : Gs.XString;
      Meta_Entry : constant String := Dist_Info_Entry (Arch, "METADATA");
   begin
      Contents.Set ("Metadata-Version: ");
      Contents.Append (Cwm.Mv_To_String (Md.Mv));
      Contents.Append (ASCII.Lf);
      Zc.Add_String (Info, Gs.To_String (Contents), Meta_Entry);
   end Write_Metadata;

   procedure Build
     (Db : Gnade.Database; Arch : Cwt.Wheel; Output : String; Vars : Vars_Maps.Map) is
      use Ae;
      Info : Zc.Zip_Create_Info;
      Zip_File : aliased Zc.Zip_File_Stream;
      Exception_Caught : Ae.Exception_Occurrence;
      Arch_Path : constant String := Ad.Compose (Output, Cwt.Wheel_File_Name (Arch));
   begin
      Ad.Create_Path (Output);
      Zc.Create_Archive (Info, Zip_File'Unchecked_Access, Arch_Path);
      declare
      begin
         Write_Metadata (Db, Info, Arch, Vars);
      exception
         when E : others =>
            Ae.Save_Occurrence (Exception_Caught, E);
      end;
      Zc.Finish (Info);
      Ae.Reraise_Occurrence (Exception_Caught);
   end Build;

end Cog.Wheel_Build;
