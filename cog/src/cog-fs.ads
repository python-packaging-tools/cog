package Cog.Fs is

   Cannot_Compute_Relpath : exception;

   function Relpath (Of_File : String; Relative_To : String) return String;

end Cog.Fs;
