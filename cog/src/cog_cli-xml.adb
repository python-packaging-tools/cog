with Ada.Strings.Unbounded;
with Sax.Readers;
with Input_Sources.Strings;
with Unicode.CES.Basic_8bit;
with Cog_Cli.Xml_Reader;
with Cog_Cli_Doc;

package body Cog_Cli.Xml is

   package Asu renames Ada.Strings.Unbounded;
   package Ccxr renames Cog_Cli.Xml_Reader;
   package Sr renames Sax.Readers;
   package Iss renames Input_Sources.Strings;
   package Ccd renames Cog_Cli_Doc;
   package U8bit renames Unicode.CES.Basic_8bit;

   function Cli_Help (Command : String) return String is
      Doc_Reader : Ccxr.Doc_Reader;
      Input      : Iss.String_Input;
      Cli_Help   : constant Ccd.Content_Type := Ccd.Get_Content ("cli.xml");
   begin
      Iss.Open (Cli_Help.Content.all,
                Encoding => U8bit.Basic_8bit_Encoding,
                Input => Input);
      Ccxr.Set_Command (Doc_Reader, Command);

      Ccxr.Set_Feature (Doc_Reader, Sr.Namespace_Prefixes_Feature, False);
      Ccxr.Set_Feature (Doc_Reader, Sr.Namespace_Feature, False);
      Ccxr.Set_Feature (Doc_Reader, Sr.Validation_Feature, False);

      Ccxr.Parse (Doc_Reader, Input);

      Iss.Close (Input);

      return Ccxr.Get_Help (Doc_Reader);
   end Cli_Help;

end Cog_Cli.Xml;
