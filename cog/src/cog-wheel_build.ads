with Ada.Containers.Indefinite_Hashed_Maps;
with Ada.Strings.Hash;
with GNATCOLL.SQL.Sqlite.Gnade;
with Cog.Wheel_Types;

package Cog.Wheel_Build is

   package Gnade renames GNATCOLL.SQL.Sqlite.Gnade;
   package Cwt renames Cog.Wheel_Types;

   package Vars_Maps is new
     Ada.Containers.Indefinite_Hashed_Maps
     (Key_Type        => String,
      Element_Type    => String,
      Hash            => Ada.Strings.Hash,
      Equivalent_Keys => "=");

   procedure Build
     (Db : Gnade.Database; Arch : Cwt.Wheel; Output : String; Vars : Vars_Maps.Map);

end Cog.Wheel_Build;
