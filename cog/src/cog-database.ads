with GNATCOLL.SQL.Sqlite.Gnade;

package Cog.Database is

   package Gnade renames GNATCOLL.SQL.Sqlite.Gnade;

   Cannot_Prepare_Query : exception;
   Cannot_Execute_Query : exception;
   Cannot_Open_Database : exception;
   Database_Corrupted : exception;

   function Get_Autocommit (Db: Gnade.Database) return Boolean;

   procedure Open (Db : in out Gnade.Database; Db_File : String);

   procedure Prepare
     (Db : Gnade.Database; Raw : String; Stmt: in out Gnade.Statement);

   procedure Step (Db : Gnade.Database; Stmt : Gnade.Statement);

   procedure Step_One (Db : Gnade.Database; Stmt : Gnade.Statement);

   function Fetch (Db : Gnade.Database; Stmt : Gnade.Statement) return Boolean;

   procedure Begin_Transaction (Db : Gnade.Database);

   procedure Commit_Transaction (Db : Gnade.Database);

   procedure Rollback_Transaction (Db : Gnade.Database);

   procedure Populate_Db (Db_File : String);

end Cog.Database;
