with Ada.Text_IO;
with Ada.Strings.Unbounded;
with Ada.Directories;
with GNATCOLL.SQL.Exec;
with GNATCOLL.SQL.Sqlite;
with GNATCOLL.Traces;
with Ada.Directories;
with Cog_Cli.Options;
with Cog_Cli.Xml;
with Cog.Wheel;
with Cog.File;

package body Cog_Cli.File is

   package Ati renames Ada.Text_IO;
   package Asu renames Ada.Strings.Unbounded;
   package Ad renames Ada.Directories;
   package Gse renames GNATCOLL.SQL.Exec;
   package Gss renames GNATCOLL.SQL.Sqlite;
   package Gt renames GNATCOLL.Traces;
   package Cco renames Cog_Cli.Options;
   package Ccx renames Cog_Cli.Xml;
   package Cf renames Cog.File;
   
   File_Must_Exist : exception;

   File_Purpose : Asu.Unbounded_String := Asu.To_Unbounded_String ("code");
   Choose_Wheel : Asu.Unbounded_String := Asu.Null_Unbounded_String;
   File_Recipe : Integer := -1;

   procedure Parse_Add  (C : in out Cco.Context) is
      use Asu;
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-file-add"));
      Wheel    : aliased Cco.Value_Handler;
      Purpose  : aliased Cco.Value_Handler;
      Recipe   : aliased Cco.Value_Handler;
   begin
      Cco.Include (Cl, "--wheel", "w", Wheel'Unchecked_Access);
      Cco.Include (Cl, "--purpose", "p", Purpose'Unchecked_Access);
      Cco.Include (Cl, "--recipe", "r", Recipe'Unchecked_Access);
      Cco.Parse_Common (C, Cl, Help, "file add", 1, Continue);

      if Wheel.Set then
         Choose_Wheel := Wheel.Option_Value;
      end if;
      if Purpose.Set then
         --  TODO: there are many more of these.  These should be
         --  retrieved from database.
         -- if Argument /= "code" and Argument /= "data" and Argument /= "script" then
         --    raise Cco.Invalid_Option_Argument
         --      with Option & " takes one of `code`, `data` or `script` argument";
         -- end if;
      File_Purpose := Purpose.Option_Value;
      end if;
      if Recipe.Set then
         File_Recipe := Integer'Value (Asu.To_String (Recipe.Option_Value));
      end if;

      if not Continue then
         return;
      end if;
      if Choose_Wheel /= Asu.Null_Unbounded_String then
         Cog.Wheel.Choose
           (Asu.To_String (Cco.Db_File), Asu.To_String (Choose_Wheel));
      end if;
      declare
         File : constant String := Cco.Argument (Cl);
      begin
         if File_Recipe = -1 then
            if not Ad.Exists (File) then
               raise File_Must_Exist
                 with File & " doesn't exist.  " &
                   "Either use --recipe, or check spelling";
            else
               Cf.Add_File (Asu.To_String (Cco.Db_File), File);
            end if;
         else
            Cf.Add_Derived_File (Asu.To_String (Cco.Db_File), File, File_Recipe);
         end if;
      end;
   end Parse_Add;

   procedure Parse_List  (C : in out Cco.Context) is
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-file-list"));
   begin
      Cco.Parse_Common (C, Cl, Help, "file list", 1, Continue);
   end Parse_List;

   procedure Parse_Remove  (C : in out Cco.Context) is
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-file-remove"));
   begin
      Cco.Parse_Common (C, Cl, Help, "file remove", 1, Continue);
   end Parse_Remove;

   procedure Parse (C : in out Cco.Context) is
      File_Context : Cco.Context;
      Help         : aliased Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-file"));
   begin
      Cco.Include (File_Context, "--help", "h", Help'Unchecked_Access);
      File_Context.Index := C.Index;
      Cco.Parse (File_Context);

      if Cco.Has_More (File_Context) then
         declare
            Subcommand : constant String := Cco.Argument (File_Context);
         begin
            Gt.Trace (Log, "Parsing subcommand: " & Subcommand);
            File_Context.Index := File_Context.Index + 1;
            Gt.Trace (Log, "Set index to: " & Integer'Image (File_Context.Index));
            if Subcommand = "add" then
               Gt.Trace (Log, "Add subcommand");
               Parse_Add (File_Context);
            elsif Subcommand = "list" then
               Gt.Trace (Log, "List subcommand");
               Parse_List (File_Context);
            elsif Subcommand = "remove" then
               Gt.Trace (Log, "Remove subcommand");
               Parse_Remove (File_Context);
            else
               raise Cco.Unknown_Subcommand
                 with Subcommand & " is not a known subcommand";
            end if;
         end;
      end if;

   end Parse;

end Cog_Cli.File;
