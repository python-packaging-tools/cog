package body Cog_Cli.Xml_Reader is

   procedure Start_Element
     (Handler       : in out Doc_Reader;
      Namespace_URI : Unicode.CES.Byte_Sequence := "";
      Local_Name    : Unicode.CES.Byte_Sequence := "";
      Qname         : Unicode.CES.Byte_Sequence := "";
      Atts          : Sax.Attributes.Attributes'Class) is
   begin
      if not Handler.Found and then Asu.To_String (Handler.Command) = Local_Name then
         Handler.Found := True;
      end if;
   end Start_Element;

   procedure Characters
     (Handler : in out Doc_Reader;
      Ch      : Unicode.CES.Byte_Sequence) is
      use Asu;
   begin
      if Handler.Found then
         Handler.Help := Asu.To_Unbounded_String (Ch);
         Handler.Found := False;
      end if;
   end Characters;

   procedure Set_Command (Handler : in out Doc_Reader; Command : String) is
   begin
      Handler.Command := Asu.To_Unbounded_String (Command);
   end Set_Command;

   procedure Set_Help (Handler : in out Doc_Reader; Help : String) is
   begin
      Handler.Help := Asu.To_Unbounded_String (Help);
   end Set_Help;
   
   function Get_Help (Handler : in out Doc_Reader) return String is
   begin
      return Asu.To_String (Handler.Help);
   end Get_Help;

end Cog_Cli.Xml_Reader;
