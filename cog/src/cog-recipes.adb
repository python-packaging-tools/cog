with Ada.Exceptions;
with Ada.Text_IO;
with Interfaces.C;
with GNATCOLL.Traces;
with GNATCOLL.SQL.Sqlite.Gnade;
with GNATCOLL.SQL.Exec;
with Cog.Database;
with Cog.Wheel_Types;

package body Cog.Recipes is

   package Ati renames Ada.Text_IO;
   package Ae renames Ada.Exceptions;
   package Gs renames GNATCOLL.Traces;
   package Gse renames GNATCOLL.SQL.Exec;
   package Cd renames Cog.Database;
   package Cwt renames Cog.Wheel_Types;

   Log : constant Gs.Trace_Handle := Gs.Create ("cog.recipe", Stream => "&2");

   function Make_Destination (D : String) return Destination is
   begin
      return (D => D, D_Length => D'Length);
   end Make_Destination;

   function Make_Source (S : String; Derived : Boolean := False) return Source is
   begin
      return (S => S, S_Length => S'Length, Derived => Derived);
   end Make_Source;

   function Make_Command (C : String) return Command is
   begin
      return (C => C, C_Length => C'Length);
   end Make_Command;

   function Make_Recipe
     (Command      : String;
      Label        : String;
      Destinations : Destination_Vectors.Vector := Destination_Vectors.Empty_Vector;
      Sources      : Source_Vectors.Vector := Source_Vectors.Empty_Vector;
      Conditions   : Condition_Vectors.Vector := Condition_Vectors.Empty_Vector)
     return Recipe is
   begin
      return
        (Destinations   => Destinations,
         Sources        => Sources,
         Conditions     => Conditions,
         Label          => Label,
         Label_Length   => Label'Length,
         Command_Length => Command'Length,
         Cmd            => Make_Command (Command));
   end Make_Recipe;

   function Op_From_String (Raw : String) return Condition_Op is
   begin
      if Raw = "=" then
         return Eq;
      elsif Raw = "<" then
         return Lt;
      elsif Raw = ">" then
         return Gt;
      elsif Raw = "!=" then
         return Neq;
      elsif Raw = ">=" then
         return Geq;
      elsif Raw = "<=" then
         return Leq;
      end if;
      raise Unreqcognized_Condition
        with Raw & " must be one of (=, <, >, !=, >=, <=)";
   end Op_From_String;

   function Op_To_String (Raw : Condition_Op) return String is
   begin
      case Raw is
         when Eq => return "=";
         when Lt => return "<";
         when Gt => return ">";
         when Neq => return "!=";
         when Geq => return ">=";
         when Leq => return "<=";
      end case;
   end Op_To_String;

   function Condition_To_String (C : Condition) return String is
   begin
      return C.Label & ": (" & C.Variable & " " & C.Op'Image & " " & C.Value & ")";
   end Condition_To_String;

   function Command_To_String (C : Command) return String is
      --  TODO: Eventually this will do interpolation of variables etc.
   begin
      return C.C;
   end Command_To_String;

   function Make_Condition
     (Label : String; Variable : String; Op : Condition_Op; Value : String)
     return Condition is
   begin
      return
        (Label => Label,
         L_Length => Label'Length,
         Variable => Variable,
         V_Length => Variable'Length,
         Value => Value,
         C_Length => Value'Length,
         Op => Op);
   end Make_Condition;

   procedure Add_Destination_Args
     (Db          : Gnade.Database;
      Stmt_Insert : out Gnade.Statement;
      Stmt_Rel    : out Gnade.Statement) is
      Raw_Insert  : constant String := "insert into destinations" &
        "(destination) values (?)";
      Raw_Rel     : constant String := "insert into destination_recipes" &
        "(recipe, destination) values (?, ?)";
   begin
      Cd.Prepare (Db, Raw_Insert, Stmt_Insert);
      Cd.Prepare (Db, Raw_Rel, Stmt_Rel);
   end Add_Destination_Args;

   procedure Add_Destination
     (Db_File    : String;
      Dependency : in out Recipe;
      Dst        : aliased in out Destination) is
      use Ae;
      Exception_Caught : Ae.Exception_Occurrence;
      Db               : Gnade.Database;
   begin
      declare
      begin
         Cd.Open (Db, Db_File);
         Add_Destination (Db, Dependency, Dst);
      exception
         when E : others =>
            Ae.Save_Occurrence (Exception_Caught, E);
      end;
      Gnade.Close (Db);
      Ae.Reraise_Occurrence (Exception_Caught);
   end Add_Destination;

   procedure Add_Destination
     (Db          : Gnade.Database;
      Dependency  : in out Recipe;
      Dst         : aliased in out Destination) is
      Dst_Var     : Destination_Access := Dst'Unchecked_Access;
      Stmt_Insert : Gnade.Statement;
      Stmt_Rel    : Gnade.Statement;
   begin
      Add_Destination_Args (Db, Stmt_Insert, Stmt_Rel);
      Add_Destination (Db, Dependency, Dst_Var, Stmt_Insert, Stmt_Rel);
      Gnade.Finalize (Stmt_Insert);
      Gnade.Finalize (Stmt_Rel);
   end Add_Destination;

   procedure Add_Destination
     (Db          : Gnade.Database;
      Dependency  : in out Recipe;
      Dst         : in out Destination_Access;
      Stmt_Insert : Gnade.Statement;
      Stmt_Rel    : Gnade.Statement) is
   begin
      Gnade.Bind_Text (Stmt_Insert, 1, Dst.D'Address, Dst.D'Length);
      Cd.Step_One (Db, Stmt_Insert);

      Gnade.Bind_Text (Stmt_Rel, 1, Dependency.Label'Address, Dependency.Label'Length);
      Gnade.Bind_Text (Stmt_Rel, 2, Dst.D'Address, Dst.D'Length);
      Cd.Step_One (Db, Stmt_Rel);
   end Add_Destination;

   procedure Add_Destinations (Db : Gnade.Database; Dependency : in out Recipe) is
      Stmt_Insert : Gnade.Statement := Gnade.No_Statement;
      Stmt_Rel    : Gnade.Statement := Gnade.No_Statement;
      C           : Destination_Vectors.Cursor := Dependency.Destinations.First;
   begin
      while Destination_Vectors.Has_Element (C) loop
         declare
            use Gnade;
            Dst : aliased Destination_Access := Dependency.Destinations.Reference (C).Element;
         begin
            if Stmt_Rel /= Gnade.No_Statement then
               Gnade.Finalize (Stmt_Rel);
            end if;
            if Stmt_Insert /= Gnade.No_Statement then
               Gnade.Finalize (Stmt_Insert);
            end if;
            Add_Destination_Args (Db, Stmt_Insert, Stmt_Rel);
            Add_Destination (Db, Dependency, Dst, Stmt_Insert, Stmt_Rel);
         end;
         Destination_Vectors.Next (C);
      end loop;
      Gnade.Finalize (Stmt_Insert);
      Gnade.Finalize (Stmt_Rel);
   end Add_Destinations;

   procedure Add_Source_Args
     (Db          : Gnade.Database;
      Derived     : Boolean;
      Stmt_Insert : out Gnade.Statement;
      Stmt_Rel    : out Gnade.Statement) is
      Raw_Insert  : constant String := "insert into sources" &
        (if Derived then "(destination)" else "(file)") &
        "values (?)";
      Raw_Rel     : constant String := "insert into source_recipes" &
        "(recipe, src) values (?, ?)";
   begin
      Cd.Prepare (Db, Raw_Insert, Stmt_Insert);
      Cd.Prepare (Db, Raw_Rel, Stmt_Rel);
   end Add_Source_Args;

   procedure Add_Source
     (Db_File    : String;
      Dependency : in out Recipe;
      Src        : aliased in out Source) is
      use Ae;
      Exception_Caught : Ae.Exception_Occurrence;
      Db               : Gnade.Database;
   begin
      declare
      begin
         Cd.Open (Db, Db_File);
         Add_Source (Db, Dependency, Src);
      exception
         when E : others =>
            Ae.Save_Occurrence (Exception_Caught, E);
      end;
      Gnade.Close (Db);
      Ae.Reraise_Occurrence (Exception_Caught);
   end Add_Source;

   procedure Add_Source
     (Db          : Gnade.Database;
      Dependency  : in out Recipe;
      Src         : aliased in out Source) is
      Src_Var     : Source_Access := Src'Unchecked_Access;
      Stmt_Insert : Gnade.Statement;
      Stmt_Rel    : Gnade.Statement;
   begin
      Add_Source_Args (Db, Src.Derived, Stmt_Insert, Stmt_Rel);
      Add_Source (Db, Dependency, Src_Var, Stmt_Insert, Stmt_Rel);
      Gnade.Finalize (Stmt_Insert);
      Gnade.Finalize (Stmt_Rel);
   end Add_Source;

   procedure Add_Source
     (Db          : Gnade.Database;
      Dependency  : in out Recipe;
      Src         : in out Source_Access;
      Stmt_Insert : Gnade.Statement;
      Stmt_Rel    : Gnade.Statement) is
   begin
      Gnade.Bind_Text (Stmt_Insert, 1, Src.S'Address, Src.S'Length);
      Cd.Step_One (Db, Stmt_Insert);

      Gnade.Bind_Text (Stmt_Rel, 1, Dependency.Label'Address, Dependency.Label'Length);
      Gnade.Bind_Text (Stmt_Rel, 2, Src.S'Address, Src.S'Length);
      Cd.Step_One (Db, Stmt_Rel);
   end Add_Source;

   procedure Add_Sources (Db : Gnade.Database; Dependency : in out Recipe) is
      Stmt_Insert : Gnade.Statement := Gnade.No_Statement;
      Stmt_Rel    : Gnade.Statement := Gnade.No_Statement;
      C           : Source_Vectors.Cursor := Dependency.Sources.First;
   begin
      while Source_Vectors.Has_Element (C) loop
         declare
            use Gnade;
            Src : aliased Source_Access := Dependency.Sources.Reference (C).Element;
         begin
            if Stmt_Rel /= Gnade.No_Statement then
               Gnade.Finalize (Stmt_Rel);
            end if;
            if Stmt_Insert /= Gnade.No_Statement then
               Gnade.Finalize (Stmt_Insert);
            end if;
            Add_Source_Args (Db, Src.all.Derived, Stmt_Insert, Stmt_Rel);
            Add_Source (Db, Dependency, Src, Stmt_Insert, Stmt_Rel);
         end;
         Source_Vectors.Next (C);
      end loop;
      Gnade.Finalize (Stmt_Insert);
      Gnade.Finalize (Stmt_Rel);
   end Add_Sources;

   procedure Add_Condition_Args
     (Db          : Gnade.Database;
      Stmt_Insert : out Gnade.Statement;
      Stmt_Rel    : out Gnade.Statement) is
      Raw_Insert  : constant String := "insert into conditions" &
        "(condition, condition_variable, condition_op, condition_value)" &
        "values (?, ?, ?, ?)";
      Raw_Rel     : constant String := "insert into condition_recipes" &
        "(recipe, condition) values (?, ?)";
   begin
      Cd.Prepare (Db, Raw_Insert, Stmt_Insert);
      Cd.Prepare (Db, Raw_Rel, Stmt_Rel);
   end Add_Condition_Args;

   procedure Add_Condition
     (Db_File    : String;
      Dependency : in out Recipe;
      Cond       : aliased in out Condition) is
      use Ae;
      Exception_Caught : Ae.Exception_Occurrence;
      Db               : Gnade.Database;
   begin
      declare
      begin
         Cd.Open (Db, Db_File);
         Add_Condition (Db, Dependency, Cond);
      exception
         when E : others =>
            Ae.Save_Occurrence (Exception_Caught, E);
      end;
      Gnade.Close (Db);
      Ae.Reraise_Occurrence (Exception_Caught);
   end Add_Condition;

   procedure Add_Condition
     (Db : Gnade.Database;
      Dependency  : in out Recipe;
      Cond        : aliased in out Condition) is
      Cond_Var    : Condition_Access := Cond'Unchecked_Access;
      Stmt_Insert : Gnade.Statement;
      Stmt_Rel    : Gnade.Statement;
   begin
      Add_Condition_Args (Db, Stmt_Insert, Stmt_Rel);
      Add_Condition (Db, Dependency, Cond_Var, Stmt_Insert, Stmt_Rel);
      Gnade.Finalize (Stmt_Insert);
      Gnade.Finalize (Stmt_Rel);
   end Add_Condition;

   procedure Add_Condition
     (Db          : Gnade.Database;
      Dependency  : in out Recipe;
      Cond        : in out Condition_Access;
      Stmt_Insert : Gnade.Statement;
      Stmt_Rel    : Gnade.Statement) is
      Op : aliased constant String := Op_To_String (Cond.Op);
   begin
      Gnade.Bind_Text (Stmt_Insert, 1, Cond.Label'Address, Cond.Label'Length);
      Gnade.Bind_Text (Stmt_Insert, 2, Cond.Variable'Address, Cond.Variable'Length);
      Gnade.Bind_Text (Stmt_Insert, 3, Op'Address, Op'Length);
      Gnade.Bind_Text (Stmt_Insert, 4, Cond.Value'Address, Cond.Value'Length);
      Cd.Step_One (Db, Stmt_Insert);

      Gnade.Bind_Text (Stmt_Rel, 1, Dependency.Label'Address, Dependency.Label'Length);
      Gnade.Bind_Text (Stmt_Rel, 2, Cond.Label'Address, Cond.Label'Length);
      Cd.Step_One (Db, Stmt_Rel);
   end Add_Condition;

   procedure Add_Conditions (Db : Gnade.Database; Dependency : in out Recipe) is
      Stmt_Insert : Gnade.Statement;
      Stmt_Rel    : Gnade.Statement;
      C           : Condition_Vectors.Cursor := Dependency.Conditions.First;
   begin
      Add_Condition_Args (Db, Stmt_Insert, Stmt_Rel);
      while Condition_Vectors.Has_Element (C) loop
         declare
            Cond : aliased Condition_Access := Dependency.Conditions.Reference (C).Element;
         begin
            Add_Condition (Db, Dependency, Cond, Stmt_Insert, Stmt_Rel);
         end;
         Condition_Vectors.Next (C);
      end loop;
      Gnade.Finalize (Stmt_Insert);
      Gnade.Finalize (Stmt_Rel);
   end Add_Conditions;

   procedure Add_Command (Db : Gnade.Database; Cmd : Command) is
      Raw  : constant String := "insert or replace into commands" &
        "(command) values (?)";
      Stmt : Gnade.Statement;
   begin
      Cd.Prepare (Db, Raw, Stmt);
      Gnade.Bind_Text (Stmt, 1, Cmd.C'Address, Cmd.C'Length);
      Cd.Step (Db, Stmt);
   end Add_Command;

   procedure Add (Db_File : String; Dependency : in out Recipe; To : Integer) is
      use Ae;
      Exception_Caught : Ae.Exception_Occurrence;
      Db               : Gnade.Database;
   begin
      declare
      begin
         Cd.Open (Db, Db_File);
         Add (Db, Dependency, To);
      exception
         when E : others =>
            Ae.Save_Occurrence (Exception_Caught, E);
            Cd.Rollback_Transaction (Db);
      end;
      Gnade.Close (Db);
      Ae.Reraise_Occurrence (Exception_Caught);
   end Add;

   procedure Add (Db : Gnade.Database; Dependency : in out Recipe; To : Integer) is
      Raw  : constant String := "insert into recipes" &
        "(label, wheel, command)" &
        "values (?, ?, ?)";
      Stmt : Gnade.Statement;
      Cmd : aliased constant String := Command_To_String (Dependency.Cmd);
   begin
      Cd.Prepare (Db, Raw, Stmt);
      Gnade.Bind_Text (Stmt, 1, Dependency.Label'Address, Dependency.Label'Length);
      Gnade.Bind_Int (Stmt, 2, Interfaces.C.int (To));
      Gnade.Bind_Text (Stmt, 3, Cmd'Address, Cmd'Length);
      Cd.Begin_Transaction (Db);
      Add_Command (Db, Dependency.Cmd);
      Cd.Step (Db, Stmt);
      Selected_Recipe_Label := Asu.To_Unbounded_String (Dependency.Label);
      Selected_Recipe_Rowid := Integer (Gnade.Last_Insert_Rowid (Db));
      Add_Destinations (Db, Dependency);
      Add_Sources (Db, Dependency);
      Add_Conditions (Db, Dependency);
      Cd.Commit_Transaction (Db);
   end Add;

   function Select_Conditions (Db : Gnade.Database; Dependency : String)
                              return Condition_Vectors.Vector is
      use Condition_Vectors;
      Result  : Condition_Vectors.Vector;
      Raw     : constant String         := "select" &
        "c.condition, c.condition_variable, c.condition_op, c.condition_value" &
        "from condition_recipes as cr join conditions as c on c.condition = cr.condition" &
        "where cr.recipe = ?";
      Stmt    : Gnade.Statement;
      Dep_Var : aliased constant String := Dependency;
   begin
      Cd.Prepare (Db, Raw, Stmt);
      Gnade.Bind_Text (Stmt, 1, Dep_Var'Address, Dep_Var'Length);

      while Cd.Fetch (Db, Stmt) loop
         declare
            Label : constant String := Gnade.Column_text (Stmt, 0);
            Variable : constant String := Gnade.Column_text (Stmt, 1);
            Op : constant Condition_Op :=
              Op_From_String (Gnade.Column_text (Stmt, 2));
            Value : constant String := Gnade.Column_text (Stmt, 3);
         begin
            Result := Result & Make_Condition (Label, Variable, Op, Value);
         end;
      end loop;

      return Result;
   end Select_Conditions;

   function Select_Destinations (Db : Gnade.Database; Dependency : String)
                              return Destination_Vectors.Vector is
      use Destination_Vectors;
      Result  : Destination_Vectors.Vector;
      --  TODO: This looks unnecessary at the moment, but I expect
      --  there to be more fields in destinations table at some point.
      Raw     : constant String         := "select" &
        "d.destination" &
        "from destination_recipes as dr join destinations as d on d.destination = dr.destination" &
        "where dr.recipe = ?";
      Stmt    : Gnade.Statement;
      Dep_Var : aliased constant String := Dependency;
   begin
      Cd.Prepare (Db, Raw, Stmt);
      Gnade.Bind_Text (Stmt, 1, Dep_Var'Address, Dep_Var'Length);

      while Cd.Fetch (Db, Stmt) loop
         declare
            Dst : constant String := Gnade.Column_text (Stmt, 0);
         begin
            Result := Result & Make_Destination (Dst);
         end;
      end loop;

      return Result;
   end Select_Destinations;

   function Select_Sources (Db : Gnade.Database; Dependency : String)
                              return Source_Vectors.Vector is
      use Source_Vectors;
      Result  : Source_Vectors.Vector;
      Raw     : constant String         := "select" &
        "coalesce(s.file, s.destination), s.file is null" &
        "from source_recipes as sr join sources as s on s.src = sr.src" &
        "where sr.recipe = ?";
      Stmt    : Gnade.Statement;
      Dep_Var : aliased constant String := Dependency;
   begin
      Cd.Prepare (Db, Raw, Stmt);
      Gnade.Bind_Text (Stmt, 1, Dep_Var'Address, Dep_Var'Length);

      while Cd.Fetch (Db, Stmt) loop
         declare
            Src : constant String := Gnade.Column_text (Stmt, 0);
            Derived : constant Boolean := (Gnade.Column_int (Stmt, 1) = 1);
         begin
            Result := Result & Make_Source (Src, derived);
         end;
      end loop;

      return Result;
   end Select_Sources;

   procedure Show (Db_File : String; Dependency : String) is
      use Ae;
      Exception_Caught : Ae.Exception_Occurrence;
      Db               : Gnade.Database;
   begin
      declare
      begin
         Cd.Open (Db, Db_File);
         Show (Db, Dependency);
      exception
         when E : others =>
            Ae.Save_Occurrence (Exception_Caught, E);
      end;
      Gnade.Close (Db);
      Ae.Reraise_Occurrence (Exception_Caught);
   end Show;

   procedure Show (Db : Gnade.Database; Dependency : String) is
      Raw  : constant String :=
        "select recipes.condition, recipes.wheel, recipes.destination, " &
        "recipes.command, recipes.condition" &
        "from recipes where recipes.label = ?";
      Stmt : Gnade.Statement;
      Dep_Var : aliased constant String := Dependency;
      Has_More : Boolean;
   begin
      Cd.Prepare (Db, Raw, Stmt);
      Gnade.Bind_Text (Stmt, 1, Dep_Var'Address, Dep_var'Length);
      Has_More := Cd.Fetch (Db, Stmt);
      if not Has_More then
         raise Unreqcognized_Recipe with "Recipe " & Dependency & " doesn't exist";
      end if;
      declare
         Cmd : constant String := Gnade.Column_Text (Stmt, 3);
         D : Recipe := Make_Recipe
           (Label        => Dependency,
            Command      => Cmd,
            Sources      => Select_Sources (Db, Dependency),
            Destinations => Select_Destinations (Db, Dependency),
            Conditions   => Select_Conditions (Db, Dependency));
      begin
         Show (Db, D);
      end;
   end Show;

   procedure Show (Db : Gnade.Database; Dependency : Recipe) is
      Raw       : constant String := "select wheel " &
        "from wheel_recipes where recipe = ?";
      Stmt      : Gnade.Statement;
      Dep_Var : aliased constant String := Dependency.Label;
   begin
      Cd.Prepare (Db, Raw, Stmt);
      Gnade.Bind_Text (Stmt, 1, Dep_Var'Address, Dep_var'Length);

      while Cd.Fetch (Db, Stmt) loop
         declare
            Rowid : constant Integer := Gnade.Column_int (Stmt, 0);
         begin
            Ati.Put_Line (Cwt.Wheel_File_Name (Cw.Wheel_By_Id (Db, Rowid)));
         end;
      end loop;
   end Show;

end Cog.Recipes;
