with GNATCOLL.SQL.Sqlite.Gnade;
with Ada.Strings.Unbounded;

package Cog.Variables is

   package Gnade renames GNATCOLL.SQL.Sqlite.Gnade;
   package Asu renames Ada.Strings.Unbounded;

   Missing_Variable : exception;

   type Vt is (Str, Int);
   type Variable_Type (Rt : Vt) is record
      T : Vt := Rt;
      case Rt is
         when Str => Str_Val : Asu.Unbounded_String;
         when Int => Int_Val : Integer;
      end case;
   end record;

   procedure Upsert (Db_File : String; Variable : String; Val : Variable_Type);
   procedure Upsert (Db : Gnade.Database; Variable : String; Val : Variable_Type);

   procedure List (Db_File : String; Regexp : String := ".");
   procedure List (Db : Gnade.Database; Regexp : String := ".");

   function Get (Db_File : String; Variable : String) return Variable_Type;
   function Get (Db : Gnade.Database; Variable : String) return Variable_Type;

   procedure Unset (Db_File : String; Variable : String);
   procedure Unset (Db : Gnade.Database; Variable : String);

end Cog.Variables;
