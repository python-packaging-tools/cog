with Ada.Text_IO;
with Ada.Strings.Unbounded;
with Ada.Directories;
with Ada.Directories.Hierarchical_File_Names;
with Ada.Exceptions;
with GNATCOLL.SQL.Sqlite.Gnade;
with Cog_Cli.Options;
with Cog_Cli.Xml;
with Cog.Database;

package body Cog_Cli.Init is

   package Ati renames Ada.Text_IO;
   package Asu renames Ada.Strings.Unbounded;
   package Ad renames Ada.Directories;
   package Adh renames Ada.Directories.Hierarchical_File_Names;
   package Ae renames Ada.Exceptions;
   package Gnade renames GNATCOLL.SQL.Sqlite.Gnade;
   package Cco renames Cog_Cli.Options;
   package Ccx renames Cog_Cli.Xml;
   package Cd renames Cog.Database;

   procedure Parse (C : in out Cco.Context) is
      Init_Context   : Cco.Context;
      Help           : aliased Cco.Help_Handler     :=
        (Help_Key => Asu.To_Unbounded_String ("cog-init"));
      Directory      : aliased Cco.Value_Handler;
      Init_Directory : Asu.Unbounded_String := Asu.To_Unbounded_String (".");
   begin
      --  TODO: Check if database already exists.
      Cco.Include (Init_Context, "--directory", "d", Directory'Unchecked_Access);
      Cco.Include (Init_Context, "--help", "h", Help'Unchecked_Access);
      Init_Context.Index := C.Index;
      Cco.Parse (Init_Context);

      if Directory.Set then
         Init_Directory := Directory.Option_Value;
      end if;

      if Cco.Has_More (Init_Context) then
         raise Cco.Command_Arguments_Count_Mismatch
           with "`cog init` takes no arguments";
      end if;
      
      if Init_Context.Completed then
         --  We've seen help
         return;
      end if;
      Cd.Populate_Db (Adh.Compose (Asu.To_String (Init_Directory), "wheels.db"));
   end Parse;

end Cog_Cli.Init;
