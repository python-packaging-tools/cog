with Sax.Readers;
with Unicode.CES;
with Sax.Attributes;
with Ada.Strings.Unbounded;

package Cog_Cli.Xml_Reader is
   
   package Asu renames Ada.Strings.Unbounded;

   type Doc_Reader is new Sax.Readers.Reader with private;

   procedure Start_Element
     (Handler       : in out Doc_Reader;
      Namespace_URI : Unicode.CES.Byte_Sequence := "";
      Local_Name    : Unicode.CES.Byte_Sequence := "";
      Qname         : Unicode.CES.Byte_Sequence := "";
      Atts          : Sax.Attributes.Attributes'Class);

   procedure Characters
     (Handler : in out Doc_Reader;
      Ch      : Unicode.CES.Byte_Sequence);

   procedure Set_Command (Handler : in out Doc_Reader; Command : String);

   procedure Set_Help (Handler : in out Doc_Reader; Help : String);
   
   function Get_Help (Handler : in out Doc_Reader) return String;
   
private
   type Doc_Reader is new Sax.Readers.Reader with record
      Command : Asu.Unbounded_String;
      Help    : Asu.Unbounded_String := Asu.Null_Unbounded_String;
      Found   : Boolean;
   end record;

end Cog_Cli.Xml_Reader;
