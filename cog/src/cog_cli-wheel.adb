with Ada.Text_IO;
with Ada.Exceptions;
with Ada.Strings.Unbounded;
with Ada.Strings.Fixed;
with Ada.Directories;
with Ada.Directories.Hierarchical_File_Names;
with GNATCOLL.SQL.Sqlite;
with GNATCOLL.SQL.Exec;
with GNATCOLL.Traces;
with Cog_Cli.Options;
with Cog_Cli.Xml;
with Cog.Wheel;
with Cog.Wheel_Types;
with Cog.Wheel_Build;
with Cog_Cli.Wheel_Metadata;

package body Cog_Cli.Wheel is

   package Ati renames Ada.Text_IO;
   package Asu renames Ada.Strings.Unbounded;
   package Gse renames GNATCOLL.SQL.Exec;
   package Gss renames GNATCOLL.SQL.Sqlite;
   package Gt renames GNATCOLL.Traces;
   package Ad renames Ada.Directories;
   package Ae renames Ada.Exceptions;
   package Cco renames Cog_Cli.Options;
   package Ccx renames Cog_Cli.Xml;
   package Cw renames Cog.Wheel;
   package Cwt renames Cog.Wheel_Types;
   package Cwb renames Cog.Wheel_Build;

   procedure Parse_Add (C : in out Cco.Context) is
      Cl              : Cco.Context;
      Continue        : Boolean;
      Help            : aliased Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-wheel-add"));
      Version         : aliased Cco.Value_Handler;
      Platform        : aliased Cco.Value_Handler;
      Python          : aliased Cco.Value_Handler;
      Abi             : aliased Cco.Value_Handler;
      Build           : aliased Cco.Value_Handler;
      Debugging       : aliased Cco.Boolean_Handler;
      Pymalloc        : aliased Cco.Boolean_Handler;
      Root_Is_Purelib : aliased Cco.Boolean_Handler;
   begin
      --  TODO: Add a shorthand version where all the below options
      --  are specified as part of the formatted wheel name (see
      --  Parse_Wheel).
      --
      --  TODO: Research casing for distribution part, also what
      --  characters are legal.
      Cco.Include (Cl, "--version", "v", Version'Unchecked_Access);
      Cco.Include (Cl, "--platform", "p", Platform'Unchecked_Access);
      Cco.Include (Cl, "--python", "P", Python'Unchecked_Access);
      Cco.Include (Cl, "--abi", "i", Abi'Unchecked_Access);
      Cco.Include (Cl, "--build", "b", Build'Unchecked_Access);
      Cco.Include (Cl, "--debugging", "d", Debugging'Unchecked_Access);
      Cco.Include (Cl, "--pymalloc", "m", Pymalloc'Unchecked_Access);
      Cco.Include (Cl, "--root-is-purelib", "r", Root_Is_Purelib'Unchecked_Access);
      Cco.Parse_Common (C, Cl, Help, "wheel add", 2, Continue);

      if Continue then
         declare
            use Asu;
            Version_Option : Asu.Unbounded_String :=
              (if Version.Set then Version.Option_Value
               else Asu.To_Unbounded_String ("$version"));
            Platform_Option : Asu.Unbounded_String :=
              (if Platform.Set then Platform.Option_Value
               else Asu.To_Unbounded_String ("$platform"));
            Python_Option : Asu.Unbounded_String :=
              (if Python.Set then Python.Option_Value
               else Asu.To_Unbounded_String ("$python"));
            Abi_Option : Asu.Unbounded_String :=
              (if Abi.Set then Abi.Option_Value
               else Asu.To_Unbounded_String ("$abi"));
            Build_Option : Asu.Unbounded_String :=
              (if Build.Set then Build.Option_Value
               else Asu.To_Unbounded_String ("$build"));
            Tags : Asu.Unbounded_String :=
              Python_Option & "-" & Abi_Option & "-" & Platform_Option;
            Wheel : Cwt.Wheel := Cw.Make_Wheel
              (Cco.Argument (Cl),
               Asu.To_String (Version_Option),
               Asu.To_String (Build_Option),
               Root_Is_Purelib.Set);
         begin
            Cw.Populate_Tags
              (Wheel,
               Abi_Option,
               Debugging.Set,
               Pymalloc.Set,
               Platform_Option,
               Python_Option);
            Cw.Add
              (Asu.To_String (Cco.Db_File),
               Wheel,
               Cco.Argument (Cl, Cl.Index + 1));
         end;
      end if;

   end Parse_Add;

   procedure Parse_List (C : in out Cco.Context) is
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-wheel-list"));
   begin
      Cco.Parse_Common (C, Cl, Help, "wheel list", 0, 1, Continue);
      if Continue then
         if Cco.Has_More (Cl, 1) then
            Cw.List (Asu.To_String (Cco.Db_File), Cco.Argument (Cl));
         else
            Cw.List (Asu.To_String (Cco.Db_File));
         end if;
      end if;
   end Parse_List;

   procedure Parse_Remove (C : in out Cco.Context) is
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-wheel-remove"));
   begin
      Cco.Parse_Common (C, Cl, Help, "wheel remove", 2, Continue);
   end Parse_Remove;

   procedure Parse_Show (C : in out Cco.Context) is
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-wheel-show"));
   begin
      Cco.Parse_Common (C, Cl, Help, "wheel show", 2, Continue);
   end Parse_Show;

   procedure Parse_Select  (C : in out Cco.Context) is
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-wheel-select"));
   begin
      Cco.Parse_Common (C, Cl, Help, "wheel select", 2, Continue);
   end Parse_Select;

   procedure Parse_Update  (C : in out Cco.Context) is
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-wheel-update"));
   begin
      Cco.Parse_Common (C, Cl, Help, "wheel update", 2, Continue);
   end Parse_Update;

   procedure Parse_Merge  (C : in out Cco.Context) is
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-wheel-merge"));
   begin
      Cco.Parse_Common (C, Cl, Help, "wheel merge", 2, Continue);
   end Parse_Merge;

   procedure Parse_Build  (C : in out Cco.Context) is
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-wheel-build"));
      Output   : aliased Cco.Value_Handler;
      Chosen   : Integer;
      Out_Dir  : Asu.Unbounded_String;
   begin
      Cco.Include (Cl, "--output", "o", Output'Unchecked_Access);
      Cco.Parse_Common_Select_Wheel
        (C, Cl, Help, "wheel build", 0, 0, Chosen, Continue);

      if Continue then
         if Output.Set then
            Out_Dir := Output.Option_Value;
         else
            Out_Dir := Asu.To_Unbounded_String (".");
         end if;

         declare
            Variables : Cwb.Vars_Maps.Map;
         begin
            Cw.Build
              (Asu.To_String (Cco.Db_File),
               Chosen,
               Asu.To_String (Out_Dir),
               Variables);
         end;
      end if;
   end Parse_Build;

   procedure Parse_Transform  (C : in out Cco.Context) is
      Cl       : Cco.Context;
      Continue : Boolean;
      Help     : Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-wheel-transform"));
   begin
      Cco.Parse_Common (C, Cl, Help, "wheel transform", 2, Continue);
   end Parse_Transform;

   procedure Parse (C : in out Cco.Context) is
      Wheel_Context : Cco.Context;
      Help          : aliased Cco.Help_Handler :=
        (Help_Key => Asu.To_Unbounded_String ("cog-wheel"));
   begin
      Cco.Include (Wheel_Context, "--help", "h", Help'Unchecked_Access);
      Wheel_Context.Index := C.Index;
      Cco.Parse (Wheel_Context);

      if Cco.Has_More (Wheel_Context) then
         declare
            Subcommand : constant String := Cco.Argument (Wheel_Context);
         begin
            Gt.Trace (Log, "Parsing subcommand: " & Subcommand);
            Wheel_Context.Index := Wheel_Context.Index + 1;
            Gt.Trace (Log, "Set index to: " & Integer'Image (Wheel_Context.Index));
            if Subcommand = "add" then
               Gt.Trace (Log, "Add subcommand");
               Parse_Add (Wheel_Context);
            elsif Subcommand = "list" then
               Gt.Trace (Log, "List subcommand");
               Parse_List (Wheel_Context);
            elsif Subcommand = "remove" then
               Gt.Trace (Log, "Remove subcommand");
               Parse_Remove (Wheel_Context);
            elsif Subcommand = "show" then
               Gt.Trace (Log, "Show subcommand");
               Parse_Show (Wheel_Context);
            elsif Subcommand = "select" then
               Gt.Trace (Log, "Select subcommand");
               Parse_Select (Wheel_Context);
            elsif Subcommand = "update" then
               Gt.Trace (Log, "Update subcommand");
               Parse_Update (Wheel_Context);
            elsif Subcommand = "merge" then
               Gt.Trace (Log, "Merge subcommand");
               Parse_Merge (Wheel_Context);
            elsif Subcommand = "build" then
               Gt.Trace (Log, "Build subcommand");
               Parse_Build (Wheel_Context);
            elsif Subcommand = "transform" then
               Gt.Trace (Log, "Transform subcommand");
               Parse_Transform (Wheel_Context);
            elsif Subcommand = "metadata" then
               Gt.Trace (Log, "Metadata subcommand");
               Cog_Cli.Wheel_Metadata.Parse (Wheel_Context);
            else
               raise Cco.Unknown_Subcommand
                 with Subcommand & " is not a known subcommand";
            end if;
         end;
      end if;

   end Parse;

end Cog_Cli.Wheel;
