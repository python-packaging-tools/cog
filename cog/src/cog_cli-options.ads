with Ada.Containers.Indefinite_Hashed_Maps;
with Ada.Strings.Hash;
with GNATCOLL.Traces;
with Ada.Strings.Unbounded;

package Cog_Cli.Options is

   package Asu renames Ada.Strings.Unbounded;

   Unsupported_Option : exception;

   Option_Missing_Argument : exception;

   Option_Takes_No_Arguments : exception;

   Invalid_Option_Argument : exception;

   Unknown_Subcommand : exception;

   Command_Arguments_Count_Mismatch : exception;

   Cannot_Find_Database : exception;

   Options_Mutually_Exclusive : exception;

   Log : constant GNATCOLL.Traces.Trace_Handle :=
     GNATCOLL.Traces.Create ("cog_cli", Stream => "&2");
   Db_File : Asu.Unbounded_String := Asu.Null_Unbounded_String;

   type Option_Arguments is array (Positive range <>) of Asu.Unbounded_String;

   type Option_Handler is tagged null record;

   function Nargs (H : Option_Handler) return Integer;

   function Call (H : in out Option_Handler; Args : Option_Arguments) return Boolean;

   type Oh_Access is access all Option_Handler'Class;

   type Help_Handler is new Option_Handler with record
      Help_Key : Asu.Unbounded_String;
   end record;

   type Value_Handler is new Option_Handler with record
      Option_Value : Asu.Unbounded_String;
      Set : Boolean := False;
   end record;

   type Boolean_Handler is new Option_Handler with record
      Set : Boolean := False;
   end record;

   package Option_Handler_Maps is new
     Ada.Containers.Indefinite_Hashed_Maps
     (Key_Type        => String,
      Element_Type    => Oh_Access,
      Hash            => Ada.Strings.Hash,
      Equivalent_Keys => "=");

   type Context is record
      Known_Options : Option_Handler_Maps.Map;
      Index : Integer := 1;
      Is_Argument : Boolean := False;
      Argument_Is_Short : Boolean := False;
      Completed : Boolean := False;
   end record;

   function Has_More (C : in out Context) return Boolean;

   function Has_More (C : in out Context; N : Integer) return Boolean;

   function Argument (C : in out Context) return String;

   function Argument (C : in out Context; I : Integer) return String;

   procedure Include
     (C : in out Context;
      Key : String;
      Short : String;
      Handler : Oh_Access);

   procedure Parse (C : in out Context);

   overriding function Nargs (H : Help_Handler) return Integer;
   overriding function Nargs (H : Value_Handler) return Integer;
   overriding function Nargs (H : Boolean_Handler) return Integer;

   overriding function Call (H : in out Help_Handler; Args : Option_Arguments) return Boolean;
   overriding function Call (H : in out Value_Handler; Args : Option_Arguments) return Boolean;
   overriding function Call (H : in out Boolean_Handler; Args : Option_Arguments) return Boolean;

   procedure Find_Database;

   procedure Parse_Common
     (C         : in out Context;
      Cl        : in out Context;
      Help      : in out Help_Handler;
      Cmd       : String;
      Nargs_Min : Integer;
      Nargs_Max : Integer;
      Continue  : out Boolean);

   procedure Parse_Common
     (C      : in out Context;
      Cl     : in out Context;
      Help   : in out Help_Handler;
      Cmd    : String;
      Nargs  : Integer;
      Continue : out Boolean);

   procedure Parse_Common_Select_Wheel
     (C         : in out Context;
      Cl        : in out Context;
      Help      : in out Help_Handler;
      Cmd       : String;
      Nargs_Min : Integer;
      Nargs_Max : Integer;
      Chosen    : out Integer;
      Continue  : out Boolean);

end Cog_Cli.Options;
