with Ada.Strings.Unbounded;
with Ada.Iterator_Interfaces;
with GNATCOLL.SQL.Sqlite.Gnade;
with GNATCOLL.SQL.Exec;
with Cog.Wheel_Types;
with Cog.Wheel_Build;

package Cog.Wheel is

   package Asu renames Ada.Strings.Unbounded;
   package Gse renames GNATCOLL.SQL.Exec;
   package Gnade renames GNATCOLL.SQL.Sqlite.Gnade;
   package Cwb renames Cog.Wheel_Build;
   package Cwt renames Cog.Wheel_Types;

   function Make_Wheel_Tag
     (Platform     : String  := "any";
      Python       : String  := "cp";
      Abi          : String  := "none";
      Debugging    : Boolean := False;
      Pymalloc     : Boolean := False;
      Wide_Unicode : Boolean := False) return Cwt.Wheel_Tag;

   function Make_Wheel
     (Distribution    : String;
      Version         : String  := "1.0.0";
      Build           : String  := "";
      Root_Is_Purelib : Boolean := False;
      Id              : Integer := -1;
      Tags            : Cwt.Tags_Vectors.Vector := Cwt.Tags_Vectors.Empty_Vector)
     return Cwt.Wheel;

   type Wheel_Fetcher (N_Parameters : Natural) is tagged private;

   function Make_Wheel_Fetcher
     (N_Parameters : Natural;
      Db           : Gnade.Database;
      Condition    : Asu.Unbounded_String;
      Variables    : Gse.Sql_Parameters) return Wheel_Fetcher;

   type Wheel_Fetcher_Cursor is private;
   function Has_Element (Pos : Wheel_Fetcher_Cursor) return Boolean;
   function Element (Pos : Wheel_Fetcher_Cursor) return Cwt.Wheel;

   package Wheel_Fetch_Iterators is
     new Ada.Iterator_Interfaces (Wheel_Fetcher_Cursor, Has_Element);

   function Fetch_Wheel (Fetcher : Wheel_Fetcher)
                        return Wheel_Fetch_Iterators.Forward_Iterator'Class;

   function Count_Tags (Raw : Asu.Unbounded_String) return Positive;

   procedure Populate_Tags
     (W         : in out Cwt.Wheel;
      Abi       : Asu.Unbounded_String;
      Debugging : Boolean;
      Pymalloc  : Boolean;
      Platform  : Asu.Unbounded_String;
      Python    : Asu.Unbounded_String);

   procedure Add (Db_File : String; Arch : Cwt.Wheel; Wheel_Path : String);

   procedure Choose (Db_File : String; Arch_Pattern : String);
   procedure Choose (Db : Gnade.Database; Arch : Cwt.Wheel);

   function Chosen (Db_File : String) return Integer;
   function Chosen (Db : Gnade.Database) return Integer;

   procedure List (Db_File : String; Regexp : String := ".");
   procedure List (Db : Gnade.Database; Regexp : String := ".");

   procedure Show (Db_File : String; Rowid : Integer);
   procedure Show (Db : Gnade.Database; Rowid : Integer);
   procedure Show (Db_File : String; Arch : Cwt.Wheel);
   procedure Show (Db : Gnade.Database; Arch : Cwt.Wheel);

   function Wheel_By_Id (Db : Gnade.Database; Id : Integer) return Cwt.Wheel;

   procedure Build
     (Db_File : String; Arch : Integer; Output : String; Vars : Cwb.Vars_Maps.Map);

private

   type Wheel_Fetcher (N_Parameters : Natural) is tagged record
      Db        : Gnade.Database;
      Condition : Asu.Unbounded_String;
      Variables : Gse.Sql_Parameters (1 .. N_Parameters);
   end record;

   type Wheel_Fetcher_Cursor is record
      Has_More  : Boolean;
      Db        : Gnade.Database;
      Stmt      : Gnade.Statement;
   end record;

   type Wheel_Fetch_Iterator is new Wheel_Fetch_Iterators.Forward_Iterator with record
      Db : Gnade.Database;
      Stmt : Gnade.Statement;
   end record;

   overriding function First (Self : Wheel_Fetch_Iterator) return Wheel_Fetcher_Cursor;

   overriding function Next (Self : Wheel_Fetch_Iterator; Pos : Wheel_Fetcher_Cursor)
                            return Wheel_Fetcher_Cursor;

end Cog.Wheel;
