with Ada.Text_IO;
with Ada.Command_Line;
with Ada.Strings.Fixed;
with Ada.Strings.Unbounded;
with GNATCOLL.Traces;
with Cog_Cli.Options;
with Cog_Cli.Init;
with Cog_Cli.Recipes;
with Cog_Cli.Wheel;
with Cog_Cli.Variables;
with Cog_Cli.File;
with Cog_Cli.Xml;

package body Cog_Cli is

   package Ati renames Ada.Text_IO;
   package Acl renames Ada.Command_Line;
   package Asu renames Ada.Strings.Unbounded;
   package Ccx renames Cog_Cli.Xml;
   package Cco renames Cog_Cli.Options;

   procedure Process_Verbosity (Level : String) is
      Nl : constant String := ASCII.CR & ASCII.LF;
      Trace_Config : constant String := "+" & Nl &
        "&2" & Nl &
        "DEBUG.ABSOLUTE_TIME" & Nl &
        "DEBUG.ABSOLUTE_DATE" & Nl &
        "DEBUG.LOCATION";
   begin
      Gt.Parse_Config (Trace_Config);
   end Process_Verbosity;

   procedure Parse is
      Context : Cco.Context;
      Help : aliased Cco.Help_Handler := (Help_Key => Asu.To_Unbounded_String ("cog"));
      Verbosity : aliased Cco.Value_Handler;
   begin
      Cco.Include (Context, "--help", "h", Help'Unchecked_Access);
      Cco.Include (Context, "--verbosity", "v", Verbosity'Unchecked_Access);
      Cco.Parse (Context);
      Process_Verbosity (Asu.To_String (Verbosity.Option_Value));
      Gt.Trace (Log, "Returned index: " & Integer'Image (Context.Index));
      Gt.Trace (Log, "Command: " & Acl.Command_Name);

      if Cco.Has_More (Context) then
         declare
            Subcommand : constant String := Cco.Argument (Context);
         begin
            Gt.Trace (Log, "Parsing subcommand: " & Subcommand);
            Context.Index := Context.Index + 1;
            Gt.Trace (Log, "Set index to: " & Integer'Image (Context.Index));
            if Subcommand = "init" then
               Gt.Trace (Log, "Init subcommand");
               Cog_Cli.Init.Parse (Context);
            elsif Subcommand = "file" then
               Gt.Trace (Log, "File subcommand");
               Cog_Cli.File.Parse (Context);
            elsif Subcommand = "wheel" then
               Gt.Trace (Log, "Wheel subcommand");
               Cog_Cli.Wheel.Parse (Context);
            elsif Subcommand = "query" then
               Gt.Trace (Log, "Query subcommand");
            elsif Subcommand = "variable" then
               Gt.Trace (Log, "Variable subcommand");
               Cog_Cli.Variables.Parse (Context);
            elsif Subcommand = "recipe" then
               Gt.Trace (Log, "Recipe subcommand");
               Cog_Cli.Recipes.Parse (Context);
            else
               raise Cco.Unknown_Subcommand
                 with Subcommand & " is not a known subcommand";
            end if;
         end;
      elsif not Context.Completed then
         raise Cco.Command_Arguments_Count_Mismatch
           with "`cog` needs a subcommand";
      end if;
   exception
      --  TODO: Print usage here.
      when E : Cco.Unsupported_Option =>
         Ati.Put_Line ("Invalid options");
         raise;
   end Parse;

end Cog_Cli;
